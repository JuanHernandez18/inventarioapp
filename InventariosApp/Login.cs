﻿using System;
using System.Data.SqlClient;
using Android.App;
using Android.Content;
using Android.Database;
using Android.Net;
using Android.OS;
using Android.Widget;
using InventariosApp.Clases;
using InventariosApp.Fragmentos;

namespace InventariosApp
{
    // [Activity(Label = "Login")]
    [Activity(Label = "InventariosApp", MainLauncher = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Login : Activity
    {
        Button btnLogin;
        private AlertDialog.Builder dialog;
        EditText Usuario;
        EditText Contrasena;
        ConexionLocal db;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Login);

            RevisarConexion();
            btnLogin = FindViewById<Button>(Resource.Id.btnLogin);
            Usuario = FindViewById<EditText>(Resource.Id.Usuario);
            Contrasena = FindViewById<EditText>(Resource.Id.Contrasena);

            btnLogin.Click += BtnLogin_Click;
            // Create your application here
        }

        private void RevisarConexion()
        {
            ConnectivityManager connectivityManager = (ConnectivityManager)GetSystemService(Context.ConnectivityService);
            NetworkInfo activeConnection = connectivityManager.ActiveNetworkInfo;
            bool isOnline = (activeConnection != null) && activeConnection.IsConnected;
            if (!isOnline)
            {
                dialog = new AlertDialog.Builder(this);
                AlertDialog alert = dialog.Create();
                alert.SetTitle("Error de conexón");
                alert.SetMessage("Por favor, segurese de estar conectado a internet y vuelva a intentarlo");
                alert.SetButton("OK", (c, ev) =>
                {
                    alert.Dismiss();
                });
                alert.Show();
            }
            else
            {
                validacionInicial();
            }
        }

        private void validacionInicial()
        {
            db = new ConexionLocal(Application.Context);
            ICursor cursorDatosConexion = db.ConsultaDatosConexion();
            
            string baseDatos = "";
            if (cursorDatosConexion.MoveToFirst())
            {
                do
                {
                    baseDatos = cursorDatosConexion.GetString(3);
                } while (cursorDatosConexion.MoveToNext());
            }

            if (baseDatos == "")
            {
                FragmentTransaction fragmentTransactions;
                DialogAppAjustes Dialog;

                fragmentTransactions = FragmentManager.BeginTransaction();
                Dialog = new DialogAppAjustes(Application.Context);
                Dialog.Show(fragmentTransactions, "Dialogo");
            }
        }


        private void BtnLogin_Click(object sender, EventArgs e)
        {
            //SqlDataReader LoginOk;

            db = new ConexionLocal(Application.Context);
            ConexionServidor con = new ConexionServidor();

            //LoginOk = con.Login(Usuario.Text, Contrasena.Text);
            //LoginOk = db.ConsultaLoginUsuarios(Usuario.Text, Contrasena.Text);
            ICursor LoginOk = db.ConsultaLoginUsuarios(Usuario.Text, Contrasena.Text);

            int ID = 0;
            string Nombre = "";
            if (LoginOk.MoveToFirst())
            {
                do
                {
                    var PrincipalActivity = new Intent(this, typeof(MainActivity));
                    PrincipalActivity.PutExtra("ID", Convert.ToString(LoginOk.GetInt(0)));
                    PrincipalActivity.PutExtra("Usuario", LoginOk.GetString(1));
                    VariablesGlobales.vgIDUsuario = LoginOk.GetInt(0);
                    this.StartActivity(PrincipalActivity);                    
                } while (LoginOk.MoveToNext());
            }
            else
            {
                Toast.MakeText(Application.Context, "Usuario incorrecto", ToastLength.Long).Show();                
            }
            //if (LoginOk.HasRows)
            //{
            //    while (LoginOk.Read())
            //    {
            //        ID = LoginOk.GetInt32(0);
            //        Nombre = LoginOk.GetString(3);                   
            //    }
            //}

            //if (ID != 0)
            //{
            //    var PrincipalActivity = new Intent(this, typeof(MainActivity));
            //    PrincipalActivity.PutExtra("ID", Convert.ToString(ID));
            //    PrincipalActivity.PutExtra("Usuario", Nombre);
            //    this.StartActivity(PrincipalActivity);
            //}
            //else
            //{
            //    Toast.MakeText(Application.Context, "Usuario incorrecto", ToastLength.Long).Show();
            //}            
        }
    }
}