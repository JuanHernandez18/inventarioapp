﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using InventariosApp.Clases;
using ZXing.Mobile;
using static Android.Provider.Settings;
using Android.Media;
using Android.Webkit;


namespace InventariosApp.Fragmentos
{
    class ActualizaDatos : Fragment
    {


        Button btnActualizar;
        ConexionLocal db;
        String direccionIPLocal;
        String direccionIPPublica;
        String baseDatos;
        String usuario;
        String contrasena;
        String cadenaConexionLocal;
        String cadenaConexionPublica;
        SqlConnection conexion;
        ProgressDialog progress;
        String mensaje;


        CheckBox DatosMaestros;
        CheckBox EtiquetasConsumos;
        CheckBox EtiquetasIXP;
        ArrayAdapter adapter;

        AlertDialog.Builder builder;
        int etiExist = 0;
        int FlagDisponible = 0;
        private MediaPlayer player;

        ActualizaEtiquetasDisp ActualizaEtiquetas;
        Multimedia Multimedia;
        ActualizaDatosMaestros ActualizaDatosMaes;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            Multimedia = new Multimedia();
            View view = inflater.Inflate(Resource.Layout.FgActualizaDatos, container, false);

            progress = new ProgressDialog(this.Activity);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetCancelable(false);


            //txtCodigo = view.FindViewById<EditText>(Resource.Id.txtCodigo);
            //lstLectura = view.FindViewById<ListView>(Resource.Id.lstCodigos);
            //txtValTallos = view.FindViewById<TextView>(Resource.Id.valTallos);
            //txtPresentacion = view.FindViewById<TextView>(Resource.Id.txtPresentacion);
            //txtProducto = view.FindViewById<TextView>(Resource.Id.txtProducto);
            btnActualizar = view.FindViewById<Button>(Resource.Id.btnActualizar);
            //btnManual = view.FindViewById<Button>(Resource.Id.btnManual);

            DatosMaestros = view.FindViewById<CheckBox>(Resource.Id.chkDatosMaestros);
            EtiquetasConsumos = view.FindViewById<CheckBox>(Resource.Id.chkEtiquetaConsumos);
            EtiquetasIXP = view.FindViewById<CheckBox>(Resource.Id.chkEtiquetaIXP);

            btnActualizar.Click += BtnActualizar_Click;

            return view;
        }        
        private void ActualizarOk(object sender, DialogClickEventArgs e)
        {
            ////Valido que cumpla los valores
            //if (CantidadActualizar.Text.Length == 0)
            //{
            //    Toast.MakeText(Application.Context, "Faltan valores por ingresar", ToastLength.Short).Show();
            //}
            //else
            //{
            //    if (Convert.ToInt32(CantidadActualizar.Text) > 5000)
            //    {
            //        etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);
            //        Toast.MakeText(Application.Context, "Revise la cantidad de tallos a ingresar", ToastLength.Short).Show();
            //    }

            //    else
            //    {
            //        lstlectura.Clear();
            //        db = new ConexionLocal(this.Activity);
            //        //db.ActualizarCalidad(codigoActualizar, Convert.ToInt32(CalidadActualizar.Text));
            //        //db.ActualizarCantidadTallosCP( Convert.ToInt32(CantidadActualizar.Text));

            //        CargarEtiquetas();
            //    }
            //}
        }
        private void CancelarAction(object sender, DialogClickEventArgs e)
        {
            builder.Dispose();
        }
        private void BtnActualizar_Click(object sender, EventArgs e)
        {

            if (DatosMaestros.Checked == true || EtiquetasConsumos.Checked == true || EtiquetasIXP.Checked == true)
            {
                VariablesGlobales.vgDatosMaestros = 0;
                VariablesGlobales.vgEtiquetasConsumos = 0;
                VariablesGlobales.vgEtiquetasIXP = 0;


                if (DatosMaestros.Checked)
                {
                    VariablesGlobales.vgDatosMaestros = 1;
                }
                if (EtiquetasConsumos.Checked)
                {
                    VariablesGlobales.vgEtiquetasConsumos = 1;
                }
                if (EtiquetasIXP.Checked)
                {
                    VariablesGlobales.vgEtiquetasIXP = 1;
                }     


                var builder = new AlertDialog.Builder(this.Activity);


                builder.SetIcon(Resource.Drawable.warning);
                builder.SetTitle("Advertencia");
                builder.SetMessage("¿Esta seguro de actualizar?");
                builder.SetPositiveButton("Si", OkAction);
                builder.SetNegativeButton("No", (IntentSender, args) => {
                    builder.Dispose();
                });
                builder.Show();
            }
            else
            {
                Toast.MakeText(Application.Context, "Debe seleccionar una opción para actualizar", ToastLength.Long).Show();
            }

            

        }

        private void OkAction(object sender, DialogClickEventArgs e)
        {
            MyTaskSync tarea = new MyTaskSync(progress, conexion, this.Activity);
            tarea.Execute();
        }



        public class MyTaskSync : AsyncTask
        {
            private ConexionLocal db;
            SqlDataReader CabezaDocumento;
            SqlDataReader CabezaDocumentoPrevio;
            ConexionServidor con = new ConexionServidor();
            ProgressDialog progress;
            SqlConnection conexion;
            String tablaTemporal;
            String NombreEquipo = "";
            Activity activity;
            public MyTaskSync(ProgressDialog progress, SqlConnection conexion, Activity activity)
            {
                this.progress = progress;
                this.conexion = conexion;
                this.activity = activity;
            }

            protected override void OnPreExecute()
            {
                progress.SetMessage("Actualizando Tablas ...");
                progress.Show();
            }
            protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
            {
                ActualizaEtiquetasDisp ActualizaEtiquetas;            
                
                if (VariablesGlobales.vgDatosMaestros == 1)
                {
                    ActualizaEtiquetas = new ActualizaEtiquetasDisp();
                    ActualizaEtiquetas.DatosMaestros();                    
                }
                if (VariablesGlobales.vgEtiquetasConsumos == 1)
                {
                    ActualizaEtiquetas = new ActualizaEtiquetasDisp();
                    ActualizaEtiquetas.CargarEtiquetasDisponibles(0);
                }
                if (VariablesGlobales.vgEtiquetasIXP == 1)
                {
                    ActualizaEtiquetas = new ActualizaEtiquetasDisp();
                    ActualizaEtiquetas.CargarEtiquetasDisponiblesIXP(0);
                }
                db = new ConexionLocal(Application.Context);
                //VerificaTablaTmpIngresosXProduccion();
                //IngresaEtiquetasTablaConsumoParcial();
                

                

                //ActualizaDatos();
                return "d";
            }

            private void VerificaTablaTmpIngresosXProduccion()
            {
                SqlDataReader tablaEtiquetas;

                ConexionServidor con = new ConexionServidor();

                String id = Secure.GetString(Application.Context.ContentResolver, Secure.AndroidId);
                int tam_var = id.Length;
                String IDLectora = id.Substring(tam_var - 2, 2);

                NombreEquipo = "Lec" + IDLectora;
                var NombreTabla = "AppMovilInventarios_Lec" + IDLectora;

                tablaEtiquetas = con.VerificaTablaTmpIngresosXProduccion(VariablesGlobales.vgNombreUsuario, NombreTabla);

                if (tablaEtiquetas.HasRows)
                {
                    while (tablaEtiquetas.Read())
                    {
                        tablaTemporal = tablaEtiquetas.GetString(0);
                    }
                }
            }
            protected override void OnPostExecute(Java.Lang.Object result)
            {
                Toast.MakeText(Application.Context, "Tablas Actualizadas", ToastLength.Long).Show();
                progress.Dismiss();
            }
        }
    }
}