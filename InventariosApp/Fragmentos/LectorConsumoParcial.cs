﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using InventariosApp.Clases;
using ZXing.Mobile;
using static Android.Provider.Settings;
using Android.Media;
using Android.Webkit;

namespace InventariosApp.Fragmentos
{
    class LectorConsumoParcial :Fragment
    {
        String codigoActualizar;

        String calidad;
        String cantidadTallos;
        String cosechador;

        Button btnIngresar;
        ConexionLocal db;
        String direccionIPLocal;
        String direccionIPPublica;
        String baseDatos;
        String usuario;
        String contrasena;
        String cadenaConexionLocal;
        String cadenaConexionPublica;
        SqlConnection conexion;
        ProgressDialog progress;
        String mensaje;
        Multimedia Multimedia;

        EditText txtCodigo;
        ListView lstLectura;
        TextView txtValTallos;
        TextView txtPresentacion;
        TextView txtProducto;
        Button btnManual;
        List<ListBD> lstCalidades = new List<ListBD>();
        List<LstLectura> lstlectura = new List<LstLectura>();

        EditText etTallosxRamo;        
        ArrayAdapter adapter;

        EditText CantidadActualizar;

        AlertDialog.Builder builder;
        int etiExist = 0;
        int FlagDisponible = 0;
        private MediaPlayer player;
        int IDEmpleado = 0;

        GeneraTXT GeneraTXT;
        List<ListBD> lstDestino = new List<ListBD>();
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            Multimedia = new Multimedia();
            View view = inflater.Inflate(Resource.Layout.FgLectorConsumoParcial, container, false);

            progress = new ProgressDialog(this.Activity);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetCancelable(false);


            txtCodigo = view.FindViewById<EditText>(Resource.Id.txtCodigo);
            lstLectura = view.FindViewById<ListView>(Resource.Id.lstCodigos);
            txtValTallos = view.FindViewById<TextView>(Resource.Id.valTallos);
            txtPresentacion = view.FindViewById<TextView>(Resource.Id.txtPresentacion);
            txtProducto = view.FindViewById<TextView>(Resource.Id.txtProducto);
            btnIngresar = view.FindViewById<Button>(Resource.Id.btnIngresar);
            btnManual = view.FindViewById<Button>(Resource.Id.btnManual);

            etTallosxRamo = view.FindViewById<EditText>(Resource.Id.etTallos);
            txtCodigo.RequestFocus();
            btnIngresar.Click += BtnIngresar_Click;

            MobileBarcodeScanner.Initialize(Activity.Application);

            lstlectura.Clear();
            lstLectura.SetAdapter(null);
            txtCodigo.Text = "";
            txtPresentacion.Text = "";
            txtProducto.Text = "";
            //etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);

            try
            {
                if (VariablesGlobales.FlagCamaraLector == 0)
                {
                    btnManual.Click += BtnManual_Click;
                    txtCodigo.TextChanged += (object sender, Android.Text.TextChangedEventArgs e) =>
                    {                       
                        if (txtCodigo.Text.Trim().Length == 15)
                        {
                            txtProducto.Text = "";
                            txtProducto.SetBackgroundColor(Android.Graphics.Color.White);

                            db = new ConexionLocal(Application.Context);
                            ICursor cursorEtiquetasExist = db.ConsultaEtiquetasExistCP(txtCodigo.Text);

                            if (cursorEtiquetasExist.MoveToFirst())
                            {
                                do
                                {
                                    etiExist = cursorEtiquetasExist.GetInt(0);
                                } while (cursorEtiquetasExist.MoveToNext());
                            }

                            if (etiExist == 0)
                            {
                                txtCodigo.Enabled = false;
                                ObtenerInformacionEtiqueta();
                                txtCodigo.Enabled = true;
                                btnManual.Enabled = true;                               
                                txtValTallos.RequestFocus();                                
                                txtCodigo.Enabled = true;                               
                            }
                            else
                            {
                                Multimedia.StartPlayer("E");
                                Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Long).Show();
                                LimpiarCampos();
                            }                           
                        }                        
                    };
                }
                else
                {
                    btnManual.Click += async delegate
                    {
                        var scanner = new MobileBarcodeScanner()
                        {
                            TopText = "Acerca la camara al elemento",
                            BottomText = "Toca la pantalla para enfocar",
                            CancelButtonText = "Cancelar"
                        };
                        try
                        {

                            var result = await scanner.Scan();
                            if (result != null)
                                txtCodigo.Text = result.ToString();

                            if (txtCodigo.Text.Trim().Length == 15)
                            {                                
                                ObtenerInformacionEtiqueta();
                                txtValTallos.RequestFocus();                                                             
                            }
                            else
                            {
                                Multimedia.StartPlayer("E");
                                Toast.MakeText(Application.Context, "Código inválido", ToastLength.Short).Show(); ;
                            }
                        }
                        catch
                        {
                            txtCodigo.Text = "";
                            txtPresentacion.Text = "";
                            txtProducto.Text = "";
                            Toast.MakeText(Application.Context, "Error de cámara", ToastLength.Long).Show();
                        }
                    };
                }
            }
            catch (Exception x)
            {
                // Qué ha sucedido
                var mensaje = "Error message: " + x.Message;
                // Dónde ha sucedido
                mensaje = mensaje + " Stack trace: " + x.StackTrace;
                string FechaRegistro = DateTime.Now.ToString();
                string valError = x.ToString();
                db.GuardarLog("ERROR AL CONSULTAR ETIQUETA POR PRODU. = " + mensaje + " - " + valError, FechaRegistro);
                //return AppEtiqueta;
            }

            lstLectura.ItemClick += LstLectura_ItemClick;
            CargarEtiquetas();
            return view;
        }

        private void LstLectura_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            codigoActualizar = e.View.FindViewById<TextView>(Resource.Id.txtCodigo).Text;

            //db = new ConexionLocal(this.Activity);
            //ICursor cursorActualizar = db.ConsultaListaProduccionPorCodigoEtiquetaCP(codigoActualizar);

            //if (cursorActualizar.MoveToFirst())
            //{
            //    do
            //    {
            //        calidad = cursorActualizar.GetString(0);
            //        cantidadTallos = cursorActualizar.GetString(1);
            //        cosechador = cursorActualizar.GetString(2);
            //    } while (cursorActualizar.MoveToNext());
            //}


            //LayoutInflater layoutInflater = LayoutInflater.From(this.Activity);
            //var promptView = layoutInflater.Inflate(Resource.Layout.DialogCambioCalidad, null);

            //CantidadActualizar = (EditText)promptView.FindViewById<EditText>(Resource.Id.CantidadTallos);
            //CantidadActualizar.Text = cantidadTallos;




            //builder = new AlertDialog.Builder(this.Activity);
            //builder.SetTitle("Eliminar Etiqueta " + codigoActualizar);

            //builder.SetView(promptView);

            ////builder.SetItems(;

            //builder.SetPositiveButton("Continuar", ActualizarOk);
            //builder.SetNegativeButton("Cancelar", CancelarAction);

            //builder.Show();

            var builder = new AlertDialog.Builder(this.Activity);


            builder.SetIcon(Resource.Drawable.warning);
            builder.SetTitle("Eliminar Etiqueta # " + codigoActualizar);
            builder.SetMessage("¿Esta seguro de eliminar la etiqueta?");
            builder.SetPositiveButton("Si", ActualizarOk);
            builder.SetNegativeButton("No", (IntentSender, args) => {
                builder.Dispose();
            });
            builder.Show();





        }

        private void txtcodigo_LostFocus(object sender, System.EventArgs e)
        {
            Toast.MakeText(Application.Context, "Faltan valores por ingresar", ToastLength.Short).Show();
        }

        private void ActualizarOk(object sender, DialogClickEventArgs e)
        {
            //Valido que cumpla los valores            
            lstlectura.Clear();
            db = new ConexionLocal(this.Activity);
            db.EliminarEtiquetasCP(codigoActualizar);
            //db.ActualizarCalidad(codigoActualizar, Convert.ToInt32(CalidadActualizar.Text));
            //db.ActualizarCantidadTallosCP( Convert.ToInt32(CantidadActualizar.Text));

            CargarEtiquetas();
                
        }
        private void CancelarAction(object sender, DialogClickEventArgs e)
        {
            builder.Dispose();
        }

        private void CargarEtiquetas()
        {
            lstlectura.Clear();

            db = new ConexionLocal(Application.Context);
            ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirCP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

            int SumTallos = 0;

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    SumTallos += Convert.ToInt32(cursorEtiquetas.GetString(1));
                    lstlectura.Add(new LstLectura(cursorEtiquetas.GetString(0), cursorEtiquetas.GetString(1)));
                } while (cursorEtiquetas.MoveToNext());
            }
            lstLectura.Adapter = new AdLstLectura(Activity, lstlectura);

            var CantEtiquetas = lstlectura.Count;

            btnIngresar.Text = "INGRESAR - " + CantEtiquetas + " - " + SumTallos + " TALLOS";
        }

        private void CargarCalidades()
        {
            SqlDataReader Calidades;
            lstCalidades.Clear();
            ConexionServidor con = new ConexionServidor();


            Calidades = con.ConsultaCalidades();

            if (Calidades.HasRows)
            {
                while (Calidades.Read())
                {
                    lstCalidades.Add(new ListBD() { ID = Calidades.GetInt32(0), Nombre = Calidades.GetString(1) });
                }
            }

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstCalidades);
        }

        private void BtnManual_Click(object sender, EventArgs e)
        {
            if (etiExist == 0)
            {
                if (txtCodigo.Text.Length == 0 || etTallosxRamo.Text.Length == 0 )
                {
                    Toast.MakeText(Application.Context, "Faltan valores por ingresar", ToastLength.Short).Show();
                }
                else
                {
                    if (Convert.ToInt32(etTallosxRamo.Text) >= VariablesGlobales.CantTallosDef)
                    {
                        etTallosxRamo.Text = "";
                        Multimedia.StartPlayer("E");
                        Toast.MakeText(Application.Context, "La cantidad de tallos a consumir no puede ser mayor o igual a la presentación", ToastLength.Short).Show();
                        etTallosxRamo.RequestFocus();
                    }                    
                    else
                    {                        
                        
                        if (txtCodigo.Text.Trim().Length == 15)
                        {
                            //Validación digit de verificación
                            int cont = 1;
                            int par = 0;
                            int impar = 0;

                            while (cont <= 14)
                            {
                                if (cont % 2 == 0)
                                {
                                    par += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                                }
                                else
                                {
                                    impar += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                                }
                                cont++;
                            }

                            int digVerif = ((impar * 3) + par) % 10;

                            if (digVerif == Convert.ToInt32(txtCodigo.Text.Substring(14, 1)))
                            {
                            GuardarEtiLocal(txtCodigo.Text, 1);
                            //AgregarEtiqueta(txtCodigo.Text, ValTallosEt, varIDBodega);
                            LimpiarCampos();
                            txtCodigo.RequestFocus();
                            Multimedia.StartPlayer("B");                                  
                            txtCodigo.RequestFocus();
                            CargarEtiquetas();
                            }
                            else
                            {
                                LimpiarCampos();
                                Multimedia.StartPlayer("E");
                                Toast.MakeText(Application.Context, "Etiqueta Invalida en el dígito de verificación", ToastLength.Short).Show();
                                txtCodigo.RequestFocus();
                            }
                        }
                        else
                        {
                            Multimedia.StartPlayer("E");
                            Toast.MakeText(Application.Context, "El Código de la Etiqueta no cumple con el estandar, vuelva a leer. Cod. " + txtCodigo.Text, ToastLength.Short).Show();
                            txtCodigo.RequestFocus();
                            //LimpiarCampos();
                        }                        
                    }
                }
            }
            else
            {
                txtCodigo.RequestFocus();
                LimpiarCampos();
                Multimedia.StartPlayer("E");
                Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
            }
            //txtCodigo.RequestFocus();
            InputMethodManager imm = (InputMethodManager)Application.Context.GetSystemService(Context.InputMethodService);
            //imm.ShowSoftInput(etCosechador, ShowFlags.Forced);
            imm.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);
        }

        private void LimpiarCampos()
        {
            txtCodigo.Text = "";
            txtPresentacion.Text = "";
            txtPresentacion.Text = "";
            txtProducto.Text = "";
            txtValTallos.Text = "";
            etTallosxRamo.Text = "";
        }

        private void GuardarEtiLocal(string text, int vFlagDisponible)
        {
            //int X = 0;
            if (vFlagDisponible == 0)
            {
                Multimedia.StartPlayer("E");
                txtProducto.Text = "ETIQUETA DUPLICADA";
                txtProducto.SetBackgroundColor(Android.Graphics.Color.Pink);
            }
            if (text.Trim().Length == 15)
            {
                //Validación digit de verificación
                int cont = 1;
                int par = 0;
                int impar = 0;

                while (cont <= 14)
                {
                    if (cont % 2 == 0)
                    {
                        par += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                    }
                    else
                    {
                        impar += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                    }
                    cont++;
                }

                int digVerif = ((impar * 3) + par) % 10;

                if (digVerif == Convert.ToInt32(txtCodigo.Text.Substring(14, 1)))
                {
                    db = new ConexionLocal(Application.Context);                    
                    string fecha = string.Format("{0:ddMMyyyy}", DateTime.Now);
                    db.GuardarDatosEtiquetasCP(text, Convert.ToInt32(etTallosxRamo.Text), Convert.ToInt32(VariablesGlobales.CantTallosDef), VariablesGlobales.vgIDBodega, 9, Convert.ToInt32(fecha), 1);
                }
                else
                {
                    LimpiarCampos();
                    txtCodigo.RequestFocus();
                    Multimedia.StartPlayer("E");
                    Toast.MakeText(Application.Context, "Etiqueta Invalida", ToastLength.Short).Show();
                }

            }
            else
            {
                player = new MediaPlayer();

                Multimedia.StartPlayer("E");
                Toast.MakeText(Application.Context, "El Código de la Etiqueta no cumple con el estandar, vuelva a leer. Cod. " + text, ToastLength.Short).Show();
            }
        }

        private void ObtenerInformacionEtiqueta()
        {
            String PresentacionEt = "";
            String ProductoEt = "";
            String ValTallosEt = "";
            String var = txtCodigo.Text;
            int varIDBodega = 0;

            
            if (var != "")
            {
                int tam_var = var.Length;
                String CodigoEtiqueta = var.Substring(0, tam_var - 1);

                SqlDataReader Etiquetas;

                ConexionServidor con = new ConexionServidor();
                //Etiquetas = con.ConsultaEtiquetaConsumoParcial(CodigoEtiqueta, VariablesGlobales.vgIDBodega);
                Etiquetas = con.ConsultaEtiquetaValidacionParcial(CodigoEtiqueta);
                

                if (Etiquetas.HasRows)
                {
                    while (Etiquetas.Read())
                    {
                        PresentacionEt = Etiquetas.GetString(0);
                        ProductoEt = Etiquetas.GetString(2);
                        ValTallosEt = Convert.ToString(Etiquetas.GetDouble(1));
                        VariablesGlobales.CantTallosDef = Convert.ToInt32(ValTallosEt);
                        varIDBodega = Etiquetas.GetInt32(3);
                        FlagDisponible = 1;
                        txtPresentacion.Text = PresentacionEt;
                        txtProducto.Text = ProductoEt;
                        etTallosxRamo.RequestFocus();  
                        //        txtValTallos.Text = ValTallosEt;
                    }
                    //player = new MediaPlayer();
                    //String FilePath = "/sdcard/Music/TonoListo.mp3";
                    //player.SetDataSource(FilePath);
                    //player.Prepare();
                    //player.Start();
                    //txtValTallos.RequestFocus();                    
                }
                else
                {
                    FlagDisponible = 0;
                    txtValTallos.RequestFocus();                    
                    Toast.MakeText(Application.Context, "No esta disponible ó Etiqueta leida en otro documento", ToastLength.Short).Show();
                }

                //if (ValTallosEt != "")
                //{
                //    if (Convert.ToInt32(ValTallosEt) == 0)
                //    {
                //        txtCodigo.Text = "";
                //        Toast.MakeText(Application.Context, "No tiene tallos disponibles para consumo", ToastLength.Short).Show();
                //    }
                //    else
                //    {
                //        txtPresentacion.Text = PresentacionEt;
                //        txtProducto.Text = ProductoEt;
                //        txtValTallos.Text = ValTallosEt;
                //    }
                //}
                //else
                //{
                //    txtCodigo.Text = "";
                //}
            }
            else
            {
                LimpiarCampos();
                txtCodigo.RequestFocus();
                Multimedia.StartPlayer("E");
                Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
            }
        }
        //public void ObtenerInformacionEmpleados(int ID)
        //{
        //    db = new ConexionLocal(Application.Context);
        //    ICursor cursorEmpleados = db.ConsultaEmpleados(ID);

        //    int Ide = 0;

        //    if (cursorEmpleados.MoveToFirst())
        //    {
        //        do
        //        {
        //            Ide = 1;
        //        } while (cursorEmpleados.MoveToNext());
        //    }
        //    else
        //    {
        //        Ide = 0;
        //    }            
        //}


        private void BtnIngresar_Click(object sender, EventArgs e)
        {

            var builder = new AlertDialog.Builder(this.Activity);
           

            builder.SetIcon(Resource.Drawable.warning);
            builder.SetTitle("Advertencia");
            builder.SetMessage("¿Esta seguro de registar el documento?");
            builder.SetPositiveButton("Si", OkAction);
            builder.SetNegativeButton("No", (IntentSender, args) => {
                builder.Dispose();
            });
            builder.Show();

        }

        private void OkAction(object sender, DialogClickEventArgs e)
        {
            GenerarArchivo();

            db = new ConexionLocal(Application.Context);
            ICursor cursorDatosConexion = db.ConsultaDatosConexion();

            if (cursorDatosConexion.MoveToFirst())
            {
                do
                {
                    direccionIPLocal = cursorDatosConexion.GetString(1);
                    direccionIPPublica = cursorDatosConexion.GetString(2);
                    baseDatos = cursorDatosConexion.GetString(3);
                    usuario = cursorDatosConexion.GetString(4);
                    contrasena = cursorDatosConexion.GetString(5);
                    VariablesGlobales.vgFlagExterno = cursorDatosConexion.GetInt(6);

                } while (cursorDatosConexion.MoveToNext());
            }

            cadenaConexionLocal = @"data source= " + direccionIPLocal + ";initial catalog=" + baseDatos + ";user id=" + usuario + ";password= " + contrasena + ";Connect Timeout=0";

            conexion = new SqlConnection(cadenaConexionLocal);

            int valConex = 0;
            try
            {
                conexion.Open();
                valConex = 1;
            }
            catch (SqlException Exp)
            {
                Toast.MakeText(Application.Context, "No estas conectado a internet", ToastLength.Long).Show();
                throw;
            }

            if (valConex == 1)
            {
                MyTaskSync tarea = new MyTaskSync(progress, conexion, this.Activity);
                tarea.Execute();
            }
        }

        private void GenerarArchivo()
        {
            ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirNoDispCP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            string NombreArchivo = VariablesGlobales.vgIDBodega + "_conpar_" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    GeneraTXT = new GeneraTXT();
                    GeneraTXT.GenerarArchivoTxt(cursorEtiquetas.GetString(0), NombreArchivo);
                } while (cursorEtiquetas.MoveToNext());
            }
        }

        
        private void GenerarArchivoTxt(string codigo, string NombreArchivo, int tipo)
        {
            // string NombreArchivo = "Prod_" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");


            string pathTxt = "/sdcard/Download/" + NombreArchivo;

            if (tipo == 1) //Crear archivo al leer sin validar
            {

                StringBuilder constructor = new StringBuilder();
                String hora = DateTime.Now.ToString("hh:mm:ss");


                String cadena = txtCodigo.Text + "," + etTallosxRamo.Text + "," + hora + "," + 0 + "," + 0 + "\r\n";

                if (!File.Exists(pathTxt))
                {
                    StreamWriter streamWriter = File.CreateText(pathTxt);
                    constructor.AppendLine(cadena);
                    streamWriter.Dispose();
                }
                else
                {
                    constructor.AppendLine(cadena);
                }
                //WriteAllText
                File.AppendAllText(pathTxt, constructor.ToString());

            }
            else //crea archivo al subir produccion
            {
                ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirIP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

                StringBuilder constructor = new StringBuilder();

                if (!File.Exists(pathTxt))
                {
                    StreamWriter streamWriter = File.CreateText(pathTxt);
                }

                if (File.Exists(pathTxt))
                {
                    if (cursorEtiquetas.MoveToFirst())
                    {
                        do
                        {
                            String a = cursorEtiquetas.GetString(0) + "," + cursorEtiquetas.GetInt(1) + "," + cursorEtiquetas.GetInt(3) + "," + cursorEtiquetas.GetInt(2) + "," + cursorEtiquetas.GetString(4) + "," + 0 + "," + 0 + "\r\n";

                            constructor.AppendLine(a);
                            File.AppendAllText(pathTxt, constructor.ToString());
                        } while (cursorEtiquetas.MoveToNext());
                    }
                }
            }
        }

        public class MyTaskSync : AsyncTask
        {
            private ConexionLocal db;
            SqlDataReader CabezaDocumento;
            SqlDataReader CabezaDocumentoPrevio;
            ConexionServidor con = new ConexionServidor();
            ProgressDialog progress;
            SqlConnection conexion;
            String tablaTemporal;
            String NombreEquipo = "";
            Activity activity;
            public MyTaskSync(ProgressDialog progress, SqlConnection conexion, Activity activity)
            {
                this.progress = progress;
                this.conexion = conexion;
                this.activity = activity;
            }

            protected override void OnPreExecute()
            {
                progress.SetMessage("Generando documento ...");
                progress.Show();
            }
            protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
            {
                db = new ConexionLocal(Application.Context);
                //VerificaTablaTmpIngresosXProduccion();
                IngresaEtiquetasTablaConsumoParcial();
                return "d";
            }

            private void VerificaTablaTmpIngresosXProduccion()
            {
                SqlDataReader tablaEtiquetas;

                ConexionServidor con = new ConexionServidor();

                String id = Secure.GetString(Application.Context.ContentResolver, Secure.AndroidId);
                int tam_var = id.Length;
                String IDLectora = id.Substring(tam_var - 2, 2);

                NombreEquipo = "Lec" + IDLectora;
                var NombreTabla = "AppMovilInventarios_Lec" + IDLectora;

                tablaEtiquetas = con.VerificaTablaTmpIngresosXProduccion(VariablesGlobales.vgNombreUsuario, NombreTabla);

                if (tablaEtiquetas.HasRows)
                {
                    while (tablaEtiquetas.Read())
                    {
                        tablaTemporal = tablaEtiquetas.GetString(0);
                    }
                }
            }

            private void IngresaEtiquetasTablaConsumoParcial()
            {
                ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirCP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

                SqlDataReader Etiquetas;

                ConexionServidor con = new ConexionServidor();
                //string sql = "INSERT INTO " + tablaTemporal + "(CodBarras,TallosXRamo,Cosechador,Calidad,Hora,Long,PresCos) VALUES (@Etiqueta,@TallosXRamo,@Cosechador,@Calidad,@Hora,@Long,@PresCos)";

                if (cursorEtiquetas.MoveToFirst())
                {
                    do
                    {
                        
                        Etiquetas = con.InsertaEtiquetaConsumoParcial(cursorEtiquetas.GetString(0), cursorEtiquetas.GetInt(1),cursorEtiquetas.GetInt(2), VariablesGlobales.vgIDBodega);

                        if (Etiquetas.HasRows)
                        {
                            while (Etiquetas.Read())
                            {
                                //Toast.MakeText(Application.Context, "Documentos Generados", ToastLength.Short).Show();
                            }                            
                        }
                        else
                        {
                            //Colocar aqui que guarde las malas ya sea en la tabla temporal u otra
                            //FlagDisponible = 0;
                            //txtValTallos.RequestFocus();
                            //Toast.MakeText(Application.Context, "Fallo Ingreso de documento", ToastLength.Short).Show();
                        }                                                                     
                    } while (cursorEtiquetas.MoveToNext());
                }
                //insertaDocumento();
            }

            private void insertaDocumento()
            {
                //Crear cabeza documento previo 
                DateTime fecha = DateTime.Now;
                String fecha1 = fecha.ToShortDateString();

                String Fecha2 = string.Format("{0:MM/dd/yyyy}", fecha);

                String hora = fecha.ToString("hh:mm:ss");

                int idPrevio = 0;

                CabezaDocumentoPrevio = con.GeneraDetallesEtiquetasDocFlorPrevioIP(Fecha2, hora, VariablesGlobales.vgIDCompanias, VariablesGlobales.vgIDUnidades, 1, VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDUsuario, "", 1);

                if (CabezaDocumentoPrevio.HasRows)
                {
                    while (CabezaDocumentoPrevio.Read())
                    {
                        idPrevio = CabezaDocumentoPrevio.GetInt32(0);
                    }
                }

                con.EliminaEtiquetasTemporalAfan(NombreEquipo, VariablesGlobales.vgNombreUsuario);

                con.LLenaAfanXUsuarioMaquina(VariablesGlobales.vgNombreUsuario, NombreEquipo, tablaTemporal);


                con.VerificaEtiquetasDocIngresoXProduccionFlorPrevio(1, "", NombreEquipo, VariablesGlobales.vgNombreUsuario, idPrevio);

                //VariablesGlobales.vgIDConsumo = 0;
                ////CABEZA DOCUMENTO
                CabezaDocumento = con.GeneraCabezaDocFlor(6, VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDUnidades, VariablesGlobales.vgIDCompanias, 0, 0, 0, 0, 0, 0, 0, VariablesGlobales.vgIDUsuario, "");


                if (CabezaDocumento.HasRows)
                {
                    while (CabezaDocumento.Read())
                    {
                        VariablesGlobales.vgIDConsumo = CabezaDocumento.GetInt32(0);
                    }
                }

                ////DETTALLES ETIQUETAS DOCUMENTO
                con.GeneraDetallesEtiquetasDocFlorIP(idPrevio, VariablesGlobales.vgIDConsumo);

                //COMPACTA DETTALLES ETIQUETAS DOCUMENTO             
                String FechaRotacion = string.Format("{0:MM/dd/yyyy}", fecha);
                con.CompactaDocIngProdFlorDetalles(VariablesGlobales.vgIDConsumo, VariablesGlobales.vgIDUnidades, FechaRotacion);

                con.EliminaEtiquetasTemporalAfan(NombreEquipo, VariablesGlobales.vgNombreUsuario);
            }

            private void LimpiarTablaEtiquetas()
            {
                db = new ConexionLocal(Application.Context);
                db.EliminarDatosEtiquetasCP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            }
            protected override void OnPostExecute(Java.Lang.Object result)
            {
                Toast.MakeText(Application.Context, "Documento Registrado", ToastLength.Long).Show();
                progress.Dismiss();
                LimpiarTablaEtiquetas();
            }
        }
    }
}