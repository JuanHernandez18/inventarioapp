﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Database;
using InventariosApp.Clases;
using Android.Views.InputMethods;

namespace InventariosApp.Fragmentos
{
    public class Ajustes : Fragment
    {

        EditText direccionIPLocal;
        EditText direccionIPPublica;
        EditText baseDatos;
        EditText usuario;
        EditText contrasena;
        Button btnTest;
        Button btnGuardar;
        CheckBox Externo;
        int FlagExterno;

        ConexionLocal db;
        int FlagDatosConexion = 0;
        Switch swChange;

        int flagShow = 0;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.FgAjustes, container, false);
            direccionIPLocal = view.FindViewById<EditText>(Resource.Id.DireccionIPLocal);
            direccionIPPublica = view.FindViewById<EditText>(Resource.Id.DireccionIPPublica);
            baseDatos = view.FindViewById<EditText>(Resource.Id.BaseDatos);
            usuario = view.FindViewById<EditText>(Resource.Id.Usuario);
            contrasena = view.FindViewById<EditText>(Resource.Id.Contrasena);
            btnTest = view.FindViewById<Button>(Resource.Id.btnTest);
            btnGuardar = view.FindViewById<Button>(Resource.Id.btnGuardar);
            swChange = view.FindViewById<Switch>(Resource.Id.swChange);
            Externo = view.FindViewById<CheckBox>(Resource.Id.chkExterno);

            btnTest.Click += btnTestClick;
            btnGuardar.Click += btnGuardarClick;
            VariablesGlobales.FlagCamaraLector = 0;
            

            LimpiarFormulario();
            CargarInfoDatosConexion();
            
            swChange.Click += SwChange_Click;
            return view;
        }



        private void SwChange_Click(object sender, EventArgs e)
        {
            VariablesGlobales.FlagCamaraLector = 1;
        }
        private void LimpiarFormulario()
        {
            direccionIPLocal.Text = "";
            direccionIPPublica.Text = "";
            baseDatos.Text = "";
            usuario.Text = "";
            contrasena.Text = "";
            Externo.Checked = false;
        }

        private void CargarInfoDatosConexion()
        {
            db = new ConexionLocal(Application.Context);
            ICursor cursorDatosConexion = db.ConsultaDatosConexion();


            if (cursorDatosConexion.MoveToFirst())
            {
                FlagDatosConexion = 1;
                do
                {
                    direccionIPLocal.Text = cursorDatosConexion.GetString(1);
                    direccionIPPublica.Text = cursorDatosConexion.GetString(2);
                    baseDatos.Text = cursorDatosConexion.GetString(3);
                    usuario.Text = cursorDatosConexion.GetString(4);
                    contrasena.Text = cursorDatosConexion.GetString(5);
                    VariablesGlobales.vgFlagExterno = cursorDatosConexion.GetInt(6);
                    Externo.Checked = (VariablesGlobales.vgFlagExterno == 1 ? true : false);
                } while (cursorDatosConexion.MoveToNext());
            }
        }

        private void btnTestClick(object sender, EventArgs e)
        {
            String CadenaConexionLocal;
            String CadenaConexionPublica;
            SqlConnection conexion;

            CadenaConexionLocal = @"data source=" + direccionIPLocal.Text + ";initial catalog=" + baseDatos.Text + ";user id=" + usuario.Text + ";password=" + contrasena.Text + ";Connect Timeout=10";
            CadenaConexionPublica = @"data source=" + direccionIPPublica.Text + ";initial catalog=" + baseDatos.Text + ";user id=" + usuario.Text + ";password=" + contrasena.Text + ";Connect Timeout=10";
            
            conexion = new SqlConnection(CadenaConexionLocal);
            try
            {
                conexion.Open();
                Toast.MakeText(Application.Context, "Conexion válida", ToastLength.Long).Show();
                btnGuardar.Visibility = ViewStates.Visible;
            }
            catch (SqlException Exp)
            {
                conexion = new SqlConnection(CadenaConexionPublica);
                try
                {
                    conexion.Open();
                    Toast.MakeText(Application.Context, "Conexion válida", ToastLength.Long).Show();
                    btnGuardar.Visibility = ViewStates.Visible;
                }
                catch (SqlException Exp1)
                {
                    Toast.MakeText(Application.Context, "Error de conexion", ToastLength.Long).Show();
                    btnGuardar.Visibility = ViewStates.Invisible;
                }
            }
        }
        private void btnGuardarClick(object sender, EventArgs e)
        {
            db = new ConexionLocal(Application.Context);
            if (Externo.Checked)
            { VariablesGlobales.vgFlagExterno = 1; }
            else
            { VariablesGlobales.vgFlagExterno = 0; }



            if (FlagDatosConexion == 0)            {
               
                db.GuardarDatosConexion(direccionIPLocal.Text, direccionIPPublica.Text, baseDatos.Text, usuario.Text, contrasena.Text, VariablesGlobales.vgFlagExterno);
                btnGuardar.Visibility = ViewStates.Invisible;
                FlagDatosConexion = 1;
            }
            else
            {
                db.ActualizarDatosConexion(direccionIPLocal.Text, direccionIPPublica.Text, baseDatos.Text, usuario.Text, contrasena.Text, VariablesGlobales.vgFlagExterno);
                btnGuardar.Visibility = ViewStates.Invisible;
                // db.GuardarDatosConexion(direccionIPLocal.Text, direccionIPPublica.Text, baseDatos.Text, usuario.Text, contrasena.Text);
                // Toast.MakeText(Application.Context, "Registro Realizado", ToastLength.Long).Show();
            }
            Toast.MakeText(Application.Context, "Registro Realizado", ToastLength.Long).Show();
        }
    }
}