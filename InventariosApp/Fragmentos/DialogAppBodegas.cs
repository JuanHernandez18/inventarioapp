﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using InventariosApp.Clases;

namespace InventariosApp.Fragmentos
{
    public class DialogAppBodegas : DialogFragment
    {
        private Context context;
        private List<ListBD> lstBodegas;
        private ArrayAdapter adapter;

        public DialogAppBodegas(Context context, List<ListBD> lstBodegas)
        {
            this.context = context;
            this.lstBodegas = lstBodegas;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View v = inflater.Inflate(Resource.Layout.LstBodegas, container, false);

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleExpandableListItem1, lstBodegas);
            var spinnerBodega = v.FindViewById<Spinner>(Resource.Id.IDBodega);
            spinnerBodega.Adapter = adapter;

            spinnerBodega.ItemSelected += ItemBodegaSelected;

            return v;
        }

        private void ItemBodegaSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstBodegas[e.Position];
            VariablesGlobales.vgIDBodega = l.ID;
        }
    }
}