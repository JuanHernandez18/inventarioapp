﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.Media;
using Android.Net.Wifi;
using Android.OS;
using Android.Views;
using Android.Widget;
using InventariosApp.Clases;
using ZXing.Mobile;
using static Android.Provider.Settings;
using Environment = Android.OS.Environment;

namespace InventariosApp.Fragmentos
{
    class Pruebas : Fragment
    {
        AlertDialog.Builder builder;
        private View alertLayout;
        Button btnCapturar;
        EditText txtCodigo;
        TextView txtPresentacion;
        TextView txtProducto;
        Switch btnState;
        Button btnOk;
        Button btnIngresar;
        Button btnManual;
        EditText cantTallos;
        ListView lstLectura;
        TextView txtValTallos;
        ConexionServidor con = new ConexionServidor();
        List<LstLectura> lstlectura = new List<LstLectura>();
        List<ListBD> lstBodegas = new List<ListBD>();

        bool isConnected = false;
        private ArrayAdapter adapter;

        private ConexionLocal db;
        public ProgressDialog progress;

        String direccionIPLocal;
        String direccionIPPublica;
        String baseDatos;
        String usuario;
        String contrasena;
        String cadenaConexionLocal;
        String cadenaConexionPublica;
        SqlConnection conexion;
        private MediaPlayer player;
        private Fragment mCurrentFragment;
        private Principal PrincipalFragment;
        private GeneraTXT GeneraTXT;
        TextView txtMensaje;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.FgLectorTrasladosConfirmacion, container, false);
            alertLayout = inflater.Inflate(Resource.Layout.LstBodegas, container, false);
            txtMensaje = view.FindViewById<TextView>(Resource.Id.txtMensaje);
            //btnCapturar = view.FindViewById<Button>(Resource.Id.btnCaptura);
            txtCodigo = view.FindViewById<EditText>(Resource.Id.txtCodigo);
            lstLectura = view.FindViewById<ListView>(Resource.Id.lstCodigos);
            //txtValTallos = view.FindViewById<TextView>(Resource.Id.valTallos);
            //txtPresentacion = view.FindViewById<TextView>(Resource.Id.txtPresentacion);
            //txtProducto = view.FindViewById<TextView>(Resource.Id.txtProducto);
            btnIngresar = view.FindViewById<Button>(Resource.Id.btnIngresar);
            btnManual = view.FindViewById<Button>(Resource.Id.btnManual);


            MobileBarcodeScanner.Initialize(Activity.Application);

            txtCodigo.RequestFocus();
            btnIngresar.Click += btnIngresarInfo;
            btnManual.Click += bthManualLectura;
            //Conexion();
            // MobileBarcodeScanner.Initialize(Activity.Application);

            lstlectura.Clear();
            lstLectura.SetAdapter(null);
            txtCodigo.Text = "";
            //txtPresentacion.Text = "";
            //txtProducto.Text = "";

            progress = new ProgressDialog(this.Context);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetCancelable(false);

            PrincipalFragment = new Principal();


            //CargarEtiquetas();

            string NombreArchivo = "Con_" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");

            if (VariablesGlobales.FlagCamaraLector == 0)
            {
                txtCodigo.TextChanged += (object sender, Android.Text.TextChangedEventArgs e) =>
                {
                    if (txtCodigo.Text.Length == 15)
                    {
                        // GenerarArchivoTxt(txtCodigo.Text, NombreArchivo);
                        //ObtenerInformacionEtiqueta();
                    }
                    else
                    {
                        if (txtCodigo.Text.Length > 15)
                        {
                            string resultado = txtCodigo.Text.Substring(0, 15);
                            txtCodigo.Text = "";
                            txtCodigo.Text = resultado;
                        }
                    }
                };
            }
            else
            {
                btnManual.Click += async delegate
                {
                    var scanner = new MobileBarcodeScanner()
                    {
                        TopText = "Acerca la camara al elemento",
                        BottomText = "Toca la pantalla para enfocar",
                        CancelButtonText = "Cancelar"
                    };
                    try
                    {
                        var result = await scanner.Scan();
                        if (result != null)
                            txtCodigo.Text = result.ToString();

                        if (txtCodigo.Text.Length == 15)
                        {
                            //  GenerarArchivoTxt(txtCodigo.Text, NombreArchivo);
                            //ObtenerInformacionEtiqueta();
                            txtValTallos.RequestFocus();
                            txtCodigo.RequestFocus();
                        }
                        else
                        {
                            txtCodigo.Text = "";
                            txtPresentacion.Text = "";
                            txtProducto.Text = "";
                            player = new MediaPlayer();

                            String FilePath = "/sdcard/Music/TonoError.mp3";
                            player.SetDataSource(FilePath);
                            player.Prepare();
                            player.Start();
                            Toast.MakeText(Application.Context, "Código inválido", ToastLength.Short).Show(); ;
                        }
                    }
                    catch
                    {
                        txtCodigo.Text = "";
                        txtPresentacion.Text = "";
                        txtProducto.Text = "";
                        Toast.MakeText(Application.Context, "Error de cámara", ToastLength.Long).Show();
                    }
                };
            }
            return view;
        }

        //private void CargarEtiquetas()
        //{
        //    lstlectura.Clear();

        //    db = new ConexionLocal(Application.Context);
        //    ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubir(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
        //    int SumTallos = 0;

        //    if (cursorEtiquetas.MoveToFirst())
        //    {
        //        do
        //        {
        //            SumTallos += Convert.ToInt32(cursorEtiquetas.GetString(1));
        //            lstlectura.Add(new LstLectura(cursorEtiquetas.GetString(0), cursorEtiquetas.GetString(1)));
        //        } while (cursorEtiquetas.MoveToNext());
        //    }
        //    lstLectura.Adapter = new AdLstLectura(Activity, lstlectura);

        //    var CantEtiquetas = lstlectura.Count;

        //    btnIngresar.Text = "INGRESAR - " + CantEtiquetas + " - " + SumTallos + " TALLOS";
        //}

        private void bthManualLectura(object sender, EventArgs e)
        {
            String var = txtCodigo.Text;
            int tam_var = var.Length;

            if (tam_var == 15)
            {
                //ObtenerInformacionEtiqueta();
            }
            else
            {
                if (txtCodigo.Text.Length != 0)
                {
                    //txtCodigo.Text = "";
                    //txtPresentacion.Text = "";
                    //txtProducto.Text = "";
                    Toast.MakeText(Application.Context, "Codigo Inválido", ToastLength.Long).Show();
                }
            }
        }

        private void btnIngresarInfo(object sender, EventArgs e)
        {
            var builder = new AlertDialog.Builder(this.Activity);
            builder.SetIcon(Resource.Drawable.warning);
            builder.SetTitle("Advertencia");
            builder.SetMessage("¿Esta seguro de registar el documento?");
            builder.SetPositiveButton("Si", OkAction);
            builder.SetNegativeButton("No", (IntentSender, args) => {
                builder.Dispose();
            });
            builder.Show();
            //ReplaceFragment(PrincipalFragment);

        }

        private void GenerarArchivoTxt(string codigo, string NombreArchivo)
        {

            string pathTxt = "/sdcard/Download/" + NombreArchivo;

            StringBuilder constructor = new StringBuilder();
            String CodigoTxt = codigo + "\r\n";

            if (!File.Exists(pathTxt))
            {
                StreamWriter streamWriter = File.CreateText(pathTxt);
                constructor.AppendLine(CodigoTxt);
                streamWriter.Dispose();
            }
            else
            {
                constructor.AppendLine(CodigoTxt);
            }
            //WriteAllText
            File.AppendAllText(pathTxt, constructor.ToString());
        }

        //private void GenerarArchivoTxtNoDisponible(string codigo)
        //{


        //    string pathTxt = "/sdcard/Download/" + NombreArchivo;

        //    StringBuilder constructor = new StringBuilder();

        //    String CodigoTxt = codigo + "\r\n";

        //    if (!File.Exists(pathTxt))
        //    {
        //        StreamWriter streamWriter = File.CreateText(pathTxt);
        //        constructor.AppendLine(CodigoTxt);
        //        streamWriter.Dispose();
        //    }
        //    else
        //    {
        //        constructor.AppendLine(CodigoTxt);
        //    }
        //    //WriteAllText
        //    File.AppendAllText(pathTxt, constructor.ToString());
        //}

        private void GenerarArchivo()
        {
            ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirNoDisp(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            string NombreArchivo = VariablesGlobales.vgIDBodega + "_conf_" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    GeneraTXT = new GeneraTXT();
                    GeneraTXT.GenerarArchivoTxt(cursorEtiquetas.GetString(0), NombreArchivo);
                } while (cursorEtiquetas.MoveToNext());
            }
        }

        private void OkAction(object sender, DialogClickEventArgs e)
        {
            GenerarArchivo();

            if (VariablesGlobales.vgIDDocumento == 7)
            {
                if (lstlectura.Count == 0)
                {
                    Toast.MakeText(Application.Context, "No hay datos para generar confirmación de traslados", ToastLength.Long).Show();
                }
                else
                {
                    db = new ConexionLocal(Application.Context);
                    ICursor cursorDatosConexion = db.ConsultaDatosConexion();

                    if (cursorDatosConexion.MoveToFirst())
                    {
                        do
                        {
                            direccionIPLocal = cursorDatosConexion.GetString(1);
                            direccionIPPublica = cursorDatosConexion.GetString(2);
                            baseDatos = cursorDatosConexion.GetString(3);
                            usuario = cursorDatosConexion.GetString(4);
                            contrasena = cursorDatosConexion.GetString(5);

                        } while (cursorDatosConexion.MoveToNext());
                    }

                    cadenaConexionLocal = @"data source= " + direccionIPLocal + ";initial catalog=" + baseDatos + ";user id=" + usuario + ";password= " + contrasena + ";Connect Timeout=0";

                    conexion = new SqlConnection(cadenaConexionLocal);

                    int valConex = 0;
                    try
                    {
                        conexion.Open();
                        valConex = 1;
                    }
                    catch (SqlException Exp)
                    {
                        Toast.MakeText(Application.Context, "No estas conectado a internet", ToastLength.Long).Show();
                        throw;
                    }

                    if (valConex == 1)
                    {
                        MyTaskSync tarea = new MyTaskSync(progress, VariablesGlobales.vgIDDocumento, conexion, this.Activity);
                        tarea.Execute();
                    }
                }

                //CargarEtiquetas();
            }
        }

        private void LimpiarTablaEtiquetas()
        {
            db = new ConexionLocal(Application.Context);
            db.EliminarDatosEtiquetas(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
        }

        private void ItemBodegaSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstBodegas[e.Position];
            VariablesGlobales.vgIDBodega = l.ID;
        }

        private void btnOKClick(object sender, EventArgs e)
        {
            if (txtCodigo.Text == "" || cantTallos.Text == "")
            {
                Toast.MakeText(Application.Context, "Falta valores por ingresar", ToastLength.Long).Show();
            }
            else
            {
                int boolVal = 0;
                foreach (LstLectura item in this.lstlectura)
                {
                    if (item.codigo == txtCodigo.Text)
                    {
                        boolVal = 1;
                    }
                }

                if (boolVal == 0)
                {
                    if (Convert.ToInt32(cantTallos.Text) > Convert.ToInt32(txtValTallos.Text))
                    {
                        Toast.MakeText(Application.Context, "No puede ingresar esa cantidad de tallos", ToastLength.Long).Show();
                    }
                    else
                    {
                        lstlectura.Add(new LstLectura(txtCodigo.Text, txtPresentacion.Text));
                        adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstlectura);

                        lstLectura.Adapter = new AdLstLectura(Activity, lstlectura);

                        txtCodigo.Text = "";
                    }
                }
                else
                {
                    txtCodigo.Text = "";
                    cantTallos.Text = "";
                    txtPresentacion.Text = "";
                    txtProducto.Text = "";
                    player = new MediaPlayer();

                    String FilePath = "/sdcard/Music/TonoError.mp3";
                    player.SetDataSource(FilePath);
                    player.Prepare();
                    player.Start();
                    Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Long).Show();
                }
            }
        }

        private void LimpiaCamposForm()
        {
            lstlectura.Clear();
            lstLectura.SetAdapter(null);
            //txtPresentacion.Text = "";
            //txtProducto.Text = "";
        }
        //private void ObtenerInformacionEtiqueta()
        //{
        //    String PresentacionEt = "";
        //    String ProductoEt = "";
        //    String ValTallosEt = "";
        //    String var = txtCodigo.Text;


        //    int boolVal = 0;
        //    foreach (LstLectura item in this.lstlectura)
        //    {
        //        if (item.codigo == txtCodigo.Text)
        //        {
        //            boolVal = 1;
        //        }
        //    }

        //    if (var != "")
        //    {
        //        if (boolVal == 0)
        //        {

        //            int tam_var = var.Length;
        //            String CodigoEtiqueta = var.Substring(0, tam_var - 1);

        //            SqlDataReader EtiquetasC;

        //            ConexionServidor con = new ConexionServidor();

        //            db = new ConexionLocal(Application.Context);
        //            ICursor cursorEtiquetasExist = db.ConsultaEtiquetasExist(txtCodigo.Text);

        //            int eti = 0;
        //            if (cursorEtiquetasExist.MoveToFirst())
        //            {
        //                do
        //                {
        //                    eti = cursorEtiquetasExist.GetInt(0);
        //                } while (cursorEtiquetasExist.MoveToNext());
        //            }

        //            if (eti == 0)
        //            {
        //                EtiquetasC = con.ConsultaEtiquetaTrasladoConfirmacion(VariablesGlobales.vgIDRemision, CodigoEtiqueta);

        //                int CantExist = 0;

        //                if (EtiquetasC != null && EtiquetasC.HasRows)
        //                {
        //                    while (EtiquetasC.Read())
        //                    {
        //                        CantExist = EtiquetasC.GetInt32(0);
        //                    }
        //                }

        //                if (CantExist == 0)
        //                {
        //                    txtMensaje.Text = "ETIQUETA NO EXISTE EN DOCUMENTO";
        //                    txtMensaje.SetBackgroundColor(Android.Graphics.Color.Pink);
        //                    GuardarEtiLocal(txtCodigo.Text, "0", VariablesGlobales.vgIDBodega, 0);
        //                    txtCodigo.Text = "";
        //                    player = new MediaPlayer();

        //                    String FilePath = "/sdcard/Music/TonoError.mp3";
        //                    player.SetDataSource(FilePath);
        //                    player.Prepare();
        //                    player.Start();
        //                }
        //                else
        //                {
        //                    txtMensaje.Text = "";
        //                    txtMensaje.SetBackgroundColor(Android.Graphics.Color.White);
        //                    GuardarEtiLocal(txtCodigo.Text, "0", VariablesGlobales.vgIDBodega, 1);
        //                    AgregarEtiqueta(txtCodigo.Text, "0", VariablesGlobales.vgIDBodega);
        //                }
        //            }
        //            else
        //            {
        //                Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
        //            }
        //        }
        //        else
        //        {
        //            txtCodigo.Text = "";
        //            Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
        //        }
        //    }
        //    //CargarEtiquetas();
        //}

        public class MyTaskSync : AsyncTask
        {
            ConexionLocal db;
            ConexionServidor con = new ConexionServidor();
            ProgressDialog progress;
            int IDTipoDocumento;
            SqlConnection conexion;
            String tablaTemporal;
            String NombreEquipo = "";
            Activity context;
            public MyTaskSync(ProgressDialog progress, int IDDocumento, SqlConnection conexion, Activity activity)
            {
                this.progress = progress;
                this.IDTipoDocumento = IDDocumento;
                this.conexion = conexion;
                this.context = activity;
            }

            protected override void OnPreExecute()
            {
                progress.SetMessage("Generando documento ...");
                progress.Show();
            }
            protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
            {
                VerificaTablaTemporalEtiquetas();
                CargarEtiquetasTemporalTraslados();
                insertaDocumentoTraslado();

                return "d";
            }


            private void VerificaTablaTemporalEtiquetas()
            {
                SqlDataReader tablaEtiquetas;

                ConexionServidor con = new ConexionServidor();
                String id = Secure.GetString(Application.Context.ContentResolver, Secure.AndroidId);
                int tam_var = id.Length;
                String IDLectora = id.Substring(tam_var - 2, 2);

                NombreEquipo = "Lec" + IDLectora;
                var NombreTabla = "AppMovilInventarios_Lec" + IDLectora;

                tablaEtiquetas = con.VerificaTablaTemporal(VariablesGlobales.vgIDUsuario, NombreTabla);

                if (tablaEtiquetas.HasRows)
                {
                    while (tablaEtiquetas.Read())
                    {
                        tablaTemporal = tablaEtiquetas.GetString(0);
                    }
                }
            }

            private void CargarEtiquetasTemporalTraslados()
            {
                db = new ConexionLocal(Application.Context);
                ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubir(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

                string sql = "INSERT INTO " + tablaTemporal + "(Etiqueta) VALUES (@Etiqueta)";

                if (cursorEtiquetas.MoveToFirst())
                {
                    do
                    {
                        using (SqlCommand comando = new SqlCommand(sql, conexion))
                        {
                            comando.Parameters.Add("@Etiqueta", SqlDbType.VarChar).Value = cursorEtiquetas.GetString(0);
                            comando.CommandType = CommandType.Text;
                            comando.ExecuteNonQuery();
                        }
                    } while (cursorEtiquetas.MoveToNext());
                }
            }

            private void insertaDocumentoTraslado()
            {
                //CABEZA DOCUMENTO
                //CabezaDocumento = con.GeneraCabezaDocFlor(7, VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDUnidades, VariablesGlobales.vgIDCompanias, VariablesGlobales.vgIDCentroC, VariablesGlobales.vgIDBodegaDest, VariablesGlobales.vgIDCamion, VariablesGlobales.vgIDConductor, 0, 0, 0, VariablesGlobales.vgIDUsuario, VariablesGlobales.vgPrecinto);

                //if (CabezaDocumento.HasRows)
                //{
                //    while (CabezaDocumento.Read())
                //    {
                //        VariablesGlobales.vgIDConsumo = CabezaDocumento.GetInt32(0);
                //    }
                //}

                //DETTALLES ETIQUETAS DOCUMENTO
                con.GeneraDetallesEtiquetasDocFlorConsumos(7, tablaTemporal, VariablesGlobales.vgIDRemision, NombreEquipo, VariablesGlobales.vgNombreUsuario);


                ////COMPACTA DETALLES ETIQUETAS DOCUMENTO
                //con.CompactaDocTrasladoFlorDetalles(VariablesGlobales.vgIDConsumo);
                ////CONFIRMA BASADO EN ETIQUETAS
                con.GeneraConfirmacionBasadoEtiquetasValidacion(VariablesGlobales.vgIDRemision);
                ////CONFIRMA BASADO EN ETIQUETAS
                con.GeneraConfirmarRemisionEntreBodegas(VariablesGlobales.vgIDRemision);
            }

            private void LimpiarTablaEtiquetas()
            {
                db = new ConexionLocal(Application.Context);
                db.EliminarDatosEtiquetas(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            }
            protected override void OnPostExecute(Java.Lang.Object result)
            {
                //SqlDataReader BuscaCodigoTraslado;
                ////BUSCAR CÓDIGO TRASLADO
                //BuscaCodigoTraslado = con.BuscaCodigoTraslado(VariablesGlobales.vgIDRemision);

                //if (BuscaCodigoTraslado.HasRows)
                //{
                //    while (BuscaCodigoTraslado.Read())
                //    {
                //        VariablesGlobales.vgCodigoConsumo = BuscaCodigoTraslado.GetInt32(0);
                //    }
                //}

                //progress.Dismiss();                
                //var builder = new AlertDialog.Builder(context);
                //builder.SetIcon(Resource.Drawable.warning);
                //builder.SetTitle("Código documento");
                //builder.SetMessage(VariablesGlobales.vgCodigoConsumo);
                //builder.SetPositiveButton("Ok", (IntentSender, args) => {
                //    builder.Dispose();
                //});
                //builder.Show();
                //LimpiarTablaEtiquetas();
                Toast.MakeText(Application.Context, "Documento Registrado", ToastLength.Long).Show();
                progress.Dismiss();
                LimpiarTablaEtiquetas();
            }
        }

        private void GuardarEtiLocal(string codigoEtiqueta, string valTallosEt, int vgIDBodega, int FlagDisponible)
        {
            db = new ConexionLocal(Application.Context);
            string fecha = string.Format("{0:ddMMyyyy}", DateTime.Now);
            db.GuardarDatosEtiquetas(codigoEtiqueta, Convert.ToInt32(valTallosEt), vgIDBodega, VariablesGlobales.vgIDDocumento, VariablesGlobales.vgIDCodLote, Convert.ToInt32(fecha), FlagDisponible);
        }

        private void AgregarEtiqueta(string codigoEtiqueta, string valTallosEt, int IDBodega)
        {
            if (VariablesGlobales.vgIDBodega == IDBodega)
            {
                lstlectura.Add(new LstLectura(codigoEtiqueta, valTallosEt));
                adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstlectura);

                lstLectura.Adapter = new AdLstLectura(Activity, lstlectura);
            }

            var CantEtiquetas = lstlectura.Count;

            btnIngresar.Text = "INGRESAR - " + CantEtiquetas;

            Toast.MakeText(Application.Context, "OK", ToastLength.Short).Show();
            txtCodigo.Text = "";
            //txtPresentacion.Text = "";
            //txtProducto.Text = "";
        }

        private void btnConexion(object sender, EventArgs e)
        {
            WifiManager wifi = (WifiManager)Application.Context.GetSystemService(Context.WifiService);
            if (isConnected == false)
            {
                wifi.SetWifiEnabled(true);
            }
            else
            {
                wifi.SetWifiEnabled(false);
            }
        }
    }
}
