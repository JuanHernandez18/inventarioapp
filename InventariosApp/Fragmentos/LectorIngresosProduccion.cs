﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using InventariosApp.Clases;
using ZXing.Mobile;
using static Android.Provider.Settings;
using Android.Media;
using Android.Webkit;

namespace InventariosApp.Fragmentos
{
    public class LectorIngresosProduccion : Fragment
    {
        String codigoActualizar;

        String calidad;
        String cantidadTallos;
        String cosechador;

        Button btnIngresar;
        ConexionLocal db;
        String direccionIPLocal;
        String direccionIPPublica;
        String baseDatos;
        String usuario;
        String contrasena;
        String cadenaConexionLocal;
        String cadenaConexionPublica;
        SqlConnection conexion;
        ProgressDialog progress;
        String mensaje;
        Multimedia Multimedia;

        EditText txtCodigo;
        ListView lstLectura;
        TextView txtValTallos;
        TextView txtPresentacion;
        TextView txtProducto;
        Button btnManual;
        List<ListBD> lstCalidades = new List<ListBD>();
        List<LstLecturaIP> lstlectura = new List<LstLecturaIP>();

        EditText etTallosxRamo;
        EditText etCosechador;
        EditText etCalidad;
       // Spinner spCalidades;
        ArrayAdapter adapter;

        EditText CalidadActualizar;
        EditText CosechadorActualizar;
        EditText CantidadActualizar;

        AlertDialog.Builder builder;
        int etiExist = 0;
        int FlagDisponible = 0;
        private MediaPlayer player;
        int IDEmpleado = 0;

        GeneraTXT GeneraTXT;
        List<ListBD> lstDestino = new List<ListBD>();
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.FgLectorIngresosProduccion, container, false);

            progress = new ProgressDialog(this.Activity);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetCancelable(false);


            txtCodigo = view.FindViewById<EditText>(Resource.Id.txtCodigo);
            lstLectura = view.FindViewById<ListView>(Resource.Id.lstCodigos);
            txtValTallos = view.FindViewById<TextView>(Resource.Id.valTallos);
            txtPresentacion = view.FindViewById<TextView>(Resource.Id.txtPresentacion);
            txtProducto = view.FindViewById<TextView>(Resource.Id.txtProducto);
            btnIngresar = view.FindViewById<Button>(Resource.Id.btnIngresar);
            btnManual = view.FindViewById<Button>(Resource.Id.btnManual);

            etTallosxRamo = view.FindViewById<EditText>(Resource.Id.etTallos);
            etCosechador = view.FindViewById<EditText>(Resource.Id.etCosechador);
            etCalidad = view.FindViewById<EditText>(Resource.Id.etCalidad);
            txtCodigo.RequestFocus();
            btnIngresar.Click += BtnIngresar_Click;

             MobileBarcodeScanner.Initialize(Activity.Application);
            
            lstlectura.Clear();
            lstLectura.SetAdapter(null);
            txtCodigo.Text = "";
            txtPresentacion.Text = "";
            txtProducto.Text = "";
            etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);

            try
            {
                if (VariablesGlobales.FlagCamaraLector == 0)
                {
                    btnManual.Click += BtnManual_Click;
                    txtCodigo.TextChanged += (object sender, Android.Text.TextChangedEventArgs e) =>
                    {
                        //progress = new ProgressDialog(this.Activity);
                        //progress.Indeterminate = true;
                        //progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                        //progress.SetCancelable(false);


                        //progress.SetMessage("Verificando Etiqueta ...");
                        //progress.Show();
                        if (txtCodigo.Text.Trim().Length == 15)
                        {


                            //ProgressDialog progress;
                            

                            //progress = new ProgressDialog(this.Activity);
                            //progress.Indeterminate = true;
                            //progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                            //progress.SetCancelable(false);


                            //progress.SetMessage("Verificando Etiqueta ...");
                            //progress.Show();

                            txtProducto.Text = "";
                            txtProducto.SetBackgroundColor(Android.Graphics.Color.White);

                            db = new ConexionLocal(Application.Context);
                            ICursor cursorEtiquetasExist = db.ConsultaEtiquetasExistIP(txtCodigo.Text);

                            if (cursorEtiquetasExist.MoveToFirst())
                            {
                                do
                                {
                                    etiExist = cursorEtiquetasExist.GetInt(0);
                                } while (cursorEtiquetasExist.MoveToNext());
                            }

                            if (etiExist == 0)
                            {
                                //ProgressDialog progress;

                                //progress = new ProgressDialog(this.Activity);
                                //progress.Indeterminate = true;
                                //progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                                //progress.SetCancelable(false);

                                //progress.SetMessage("Verificando Etiqueta ...");
                                //progress.Show();
                                txtCodigo.Enabled = false;
                                ObtenerInformacionEtiqueta();
                                txtCodigo.Enabled = true;
                                btnManual.Enabled = true;
                                //txtValTallos;                                
                                //player = new MediaPlayer();                                
                                //String FilePath = "/sdcard/Music/TonoListo.mp3";
                                //player.SetDataSource(FilePath);
                                //player.Prepare();
                                //player.Start();
                                txtValTallos.RequestFocus();
                                etCalidad.RequestFocus();
                                txtCodigo.Enabled = true;                              

                                //progress.Dismiss();
                            }
                            else
                            {
                               // player = new MediaPlayer();
                                String FilePath = "/sdcard/Music/TonoError.mp3";
                                Multimedia = new Multimedia();
                                Multimedia.StartPlayer(FilePath);
                                //player.SetDataSource(FilePath);
                                //player.Prepare();
                                //player.Start();
                                Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Long).Show();
                                LimpiarCampos();
                            }
                            //progress.Dismiss();
                        }
                        //progress.Dismiss();
                        //else
                        //{
                        //    if (txtCodigo.Text.Length != 0)
                        //    {
                        //        Toast.MakeText(Application.Context, "Codigo Inválido", ToastLength.Long).Show();
                        //        btnManual.Enabled = false;
                        //    }
                        //}
                    };
                }
                else
                {
                    btnManual.Click += async delegate
                    {
                        var scanner = new MobileBarcodeScanner()
                        {
                            TopText = "Acerca la camara al elemento",
                            BottomText = "Toca la pantalla para enfocar",
                            CancelButtonText = "Cancelar"
                        };
                        try
                        {
                            
                            var result = await scanner.Scan();
                            if (result != null)
                                txtCodigo.Text = result.ToString();

                            if (txtCodigo.Text.Trim().Length == 15)
                            {
                                ProgressDialog progress;

                                //progress = new ProgressDialog(this.Activity);
                                //progress.Indeterminate = true;
                                //progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                                //progress.SetCancelable(false);


                                //progress.SetMessage("Verificando Etiqueta ...");
                                //progress.Show();
                                ObtenerInformacionEtiqueta();                                
                                //player = new MediaPlayer();
                                //String FilePath = "/sdcard/Music/TonoListo.mp3";
                                //player.SetDataSource(FilePath);
                                //player.Prepare();
                                //player.Start();
                                txtValTallos.RequestFocus();
                                etCalidad.RequestFocus();
                                //progress.Dismiss();
                            }
                            else
                            {
                                //player = new MediaPlayer();
                                String FilePath = "/sdcard/Music/TonoError.mp3";
                                Multimedia = new Multimedia();
                                Multimedia.StartPlayer(FilePath);
                                //player.SetDataSource(FilePath);
                                //player.Prepare();
                                //player.Start();
                                Toast.MakeText(Application.Context, "Código inválido", ToastLength.Short).Show(); ;
                            }
                        }
                        catch
                        {
                            txtCodigo.Text = "";
                            txtPresentacion.Text = "";
                            txtProducto.Text = "";
                            Toast.MakeText(Application.Context, "Error de cámara", ToastLength.Long).Show();
                        }
                    };
                }
            }
            catch (Exception x)
            {
                // Qué ha sucedido
                var mensaje = "Error message: " + x.Message;
                // Dónde ha sucedido
                mensaje = mensaje + " Stack trace: " + x.StackTrace;
                string FechaRegistro = DateTime.Now.ToString();
                string valError = x.ToString();
                db.GuardarLog("ERROR AL CONSULTAR ETIQUETA POR PRODU. = " + mensaje + " - " + valError, FechaRegistro);
                //return AppEtiqueta;
            }            
            
            lstLectura.ItemClick += LstLectura_ItemClick;
            CargarEtiquetas();
            return view;
        }

        private void LstLectura_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            codigoActualizar = e.View.FindViewById<TextView>(Resource.Id.txtCodigo).Text;

            db = new ConexionLocal(this.Activity);
            ICursor cursorActualizar = db.ConsultaListaProduccionPorCodigoEtiqueta(codigoActualizar);

            if (cursorActualizar.MoveToFirst())
            {
                do
                {
                    calidad = cursorActualizar.GetString(0);
                    cantidadTallos = cursorActualizar.GetString(1);
                    cosechador = cursorActualizar.GetString(2);
                } while (cursorActualizar.MoveToNext());
            }


            LayoutInflater layoutInflater = LayoutInflater.From(this.Activity);
            var promptView = layoutInflater.Inflate(Resource.Layout.DialogCambioCalidad, null);

            CalidadActualizar = (EditText)promptView.FindViewById<EditText>(Resource.Id.Calidad);
            CantidadActualizar = (EditText)promptView.FindViewById<EditText>(Resource.Id.CantidadTallos);
            CosechadorActualizar = (EditText)promptView.FindViewById<EditText>(Resource.Id.Cosechador);

            CalidadActualizar.Text = calidad;
            CantidadActualizar.Text = cantidadTallos;
            CosechadorActualizar.Text = cosechador;

            builder = new AlertDialog.Builder(this.Activity);
            builder.SetTitle("Ingrese nueva calidad ");

            builder.SetView(promptView);
            
            //builder.SetItems(;

            builder.SetPositiveButton("Continuar", ActualizarOk);
            builder.SetNegativeButton("Cancelar", CancelarAction);

            builder.Show();




        }

        private void txtcodigo_LostFocus(object sender, System.EventArgs e)
        {
            Toast.MakeText(Application.Context, "Faltan valores por ingresar", ToastLength.Short).Show();
        }

        private void ActualizarOk(object sender, DialogClickEventArgs e)
        {
            //Valido que cumpla los valores
            if (CalidadActualizar.Text.Length == 0 || CantidadActualizar.Text.Length == 0 || CosechadorActualizar.Text.Length == 0)
            {
                Toast.MakeText(Application.Context, "Faltan valores por ingresar", ToastLength.Short).Show();
            }
            else
            {
                if (Convert.ToInt32(CantidadActualizar.Text) > 5000)
                {
                    etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);
                    Toast.MakeText(Application.Context, "Revise la cantidad de tallos a ingresar", ToastLength.Short).Show();
                }
                else if (CosechadorActualizar.Text.Trim().Length > 5)
                {
                    //etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);
                    Toast.MakeText(Application.Context, "Revise el código del cosechador", ToastLength.Short).Show();
                }
                else if (CalidadActualizar.Text.Trim().Length > 5)
                {
                    //etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);
                    Toast.MakeText(Application.Context, "Revise la calidad", ToastLength.Short).Show();
                }
                else
                {
                    lstlectura.Clear();
                    db = new ConexionLocal(this.Activity);
                    //db.ActualizarCalidad(codigoActualizar, Convert.ToInt32(CalidadActualizar.Text));
                    db.ActualizarCalidad(codigoActualizar, Convert.ToInt32(CalidadActualizar.Text), Convert.ToInt32(CantidadActualizar.Text), Convert.ToInt32(CosechadorActualizar.Text));

                    CargarEtiquetas();
                }
            }
        }
        private void CancelarAction(object sender, DialogClickEventArgs e)
        {
            builder.Dispose();
        }
        
        private void CargarEtiquetas()
        {
            lstlectura.Clear();

            db = new ConexionLocal(Application.Context);
            ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirIP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

            int SumTallos = 0;

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    SumTallos += Convert.ToInt32(cursorEtiquetas.GetString(1));
                    lstlectura.Add(new LstLecturaIP(cursorEtiquetas.GetString(0), cursorEtiquetas.GetString(1), cursorEtiquetas.GetString(2), cursorEtiquetas.GetString(3)));
                } while (cursorEtiquetas.MoveToNext());
            }
            lstLectura.Adapter = new AdLstLecturaIP(Activity, lstlectura);

            var CantEtiquetas = lstlectura.Count;

            btnIngresar.Text = "INGRESAR - " + CantEtiquetas + " - " + SumTallos + " TALLOS";
        }

        private void CargarCalidades()
        {
            SqlDataReader Calidades;
            lstCalidades.Clear();
            ConexionServidor con = new ConexionServidor();

           
            Calidades = con.ConsultaCalidades();

            if (Calidades.HasRows)
            {
                while (Calidades.Read())
                {
                    lstCalidades.Add(new ListBD() { ID = Calidades.GetInt32(0), Nombre = Calidades.GetString(1) });
                }
            }

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstCalidades);
        }

        private void BtnManual_Click(object sender, EventArgs e)
        {
            if (etiExist == 0)
            {
                if (txtCodigo.Text.Length == 0|| etTallosxRamo.Text.Length == 0 || etCalidad.Text.Length == 0 || etCosechador.Text.Length == 0)
                {
                    Toast.MakeText(Application.Context, "Faltan valores por ingresar", ToastLength.Short).Show();
                }                
                else
                {
                    if (Convert.ToInt32(etTallosxRamo.Text) > 5000)
                    {
                        etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);
                        Toast.MakeText(Application.Context, "Revise la cantidad de tallos a ingresar", ToastLength.Short).Show();
                    }
                    else if (etCosechador.Text.Trim().Length > 5)
                    {
                        //etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);
                        Toast.MakeText(Application.Context, "Revise el código del cosechador", ToastLength.Short).Show();
                    }
                    else if (etCalidad.Text.Trim().Length > 5)
                    {
                        //etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);
                        Toast.MakeText(Application.Context, "Revise la calidad", ToastLength.Short).Show();
                    }

                    else
                    {
                        IDEmpleado = Convert.ToInt32(etCosechador.Text);

                        db = new ConexionLocal(Application.Context);
                        ICursor cursorEmpleados = db.ConsultaEmpleados(IDEmpleado);

                        int Ide = 0;

                        if (cursorEmpleados.MoveToFirst())
                        {
                            do
                            {
                                Ide = 1;
                            } while (cursorEmpleados.MoveToNext());
                        }
                        else
                        {
                            Ide = 0;
                        }
                        Ide = 1;

                        //IDEmpleado = ObtenerInformacionEmpleados(IDEmpleado);
                        if (Ide == 0)
                        {

                            //etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);
                            Toast.MakeText(Application.Context, "El Código del cosechador no existe, verifique por favor", ToastLength.Short).Show();
                        }
                        else
                        {
                            if (txtCodigo.Text.Substring(0, 1) == "9" & (txtCodigo.Text.Trim().Length == 15))
                            {
                                //Validación digit de verificación
                                int cont = 1;
                                int par = 0;
                                int impar = 0;

                                while (cont <= 14)
                                {
                                    if (cont % 2 == 0)
                                    {
                                        par += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                                    }
                                    else
                                    {
                                        impar += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                                    }
                                    cont++;
                                }

                                int digVerif = ((impar * 3) + par) % 10;

                                if (digVerif == Convert.ToInt32(txtCodigo.Text.Substring(14, 1)))
                                {
                                    GuardarEtiLocal(txtCodigo.Text, FlagDisponible);
                                    CargarEtiquetas();
                                    LimpiarCampos();
                                    txtCodigo.RequestFocus();
                                }
                                else
                                {
                                    txtCodigo.Text = "";
                                    txtPresentacion.Text = "";
                                    txtProducto.Text = "";
                                    player = new MediaPlayer();
                                    String FilePath = "/sdcard/Music/TonoError.mp3";
                                    player.SetDataSource(FilePath);
                                    player.Prepare();
                                    player.Start();
                                    Toast.MakeText(Application.Context, "Etiqueta Invalida", ToastLength.Short).Show();
                                }

                            }

                            else
                            {
                                player = new MediaPlayer();
                                String FilePath = "/sdcard/Music/TonoError.mp3";
                                player.SetDataSource(FilePath);
                                player.Prepare();
                                player.Start();
                                Toast.MakeText(Application.Context, "El Código de la Etiqueta no cumple con el estandar, vuelva a leer. Cod. " + txtCodigo.Text, ToastLength.Short).Show();
                                txtCodigo.RequestFocus();
                                //LimpiarCampos();
                            }
                        }
                    }
                }
            }
            else
            {
                txtCodigo.RequestFocus();
                txtCodigo.Text = "";
                txtPresentacion.Text = "";
                txtProducto.Text = "";
                etTallosxRamo.Text = "";
                etCosechador.Text = "";
                player = new MediaPlayer();
                String FilePath = "/sdcard/Music/TonoError.mp3";
                player.SetDataSource(FilePath);
                player.Prepare();
                player.Start();
                Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
            }
            txtCodigo.RequestFocus();
            InputMethodManager imm = (InputMethodManager)Application.Context.GetSystemService(Context.InputMethodService);
            imm.ShowSoftInput(etCosechador,ShowFlags.Forced);
            imm.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);
        }

        private void LimpiarCampos()
        {
            txtCodigo.Text = "";
            txtPresentacion.Text = "";
            etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);
            etCosechador.Text = "";
        }

        private void GuardarEtiLocal(string text, int vFlagDisponible)
        {
            //int X = 0;
            if (vFlagDisponible == 0)
            {
                player = new MediaPlayer();
                String FilePath = "/sdcard/Music/TonoError.mp3";
                player.SetDataSource(FilePath);
                player.Prepare();
                player.Start();
                txtProducto.Text = "ETIQUETA DUPLICADA";
                txtProducto.SetBackgroundColor(Android.Graphics.Color.Pink);
            }
            if (text.Substring(0, 1) == "9" & (text.Trim().Length == 15))
            {
                //Validación digit de verificación
                int cont = 1;
                int par = 0;
                int impar = 0;

                while (cont <= 14)
                {
                    if (cont % 2 == 0)
                    {
                        par += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                    }
                    else
                    {
                        impar += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                    }
                    cont++;
                }

                int digVerif = ((impar * 3) + par) % 10;

                if (digVerif == Convert.ToInt32(txtCodigo.Text.Substring(14, 1)))
                {
                    db = new ConexionLocal(Application.Context);
                    String hora = DateTime.Now.ToString("hh:mm:ss");
                    db.GuardarDatosEtiquetasINP(text, Convert.ToInt32(etTallosxRamo.Text), VariablesGlobales.vgIDDocumento, VariablesGlobales.vgIDBodega, Convert.ToInt32(etCosechador.Text), Convert.ToInt32(etCalidad.Text), hora, vFlagDisponible);
                }
                else
                {
                    txtCodigo.Text = "";
                    txtPresentacion.Text = "";
                    txtProducto.Text = "";
                    player = new MediaPlayer();
                    String FilePath = "/sdcard/Music/TonoError.mp3";
                    player.SetDataSource(FilePath);
                    player.Prepare();
                    player.Start();
                    Toast.MakeText(Application.Context, "Etiqueta Invalida", ToastLength.Short).Show();
                }
                
            }
            else
            {
                player = new MediaPlayer();

                String FilePath = "/sdcard/Music/TonoError.mp3";
                player.SetDataSource(FilePath);
                player.Prepare();
                player.Start();
                Toast.MakeText(Application.Context, "El Código de la Etiqueta no cumple con el estandar, vuelva a leer. Cod. " + text, ToastLength.Short).Show();
            }
        }
        //private void AgregarEtiqueta(string codigoEtiqueta, string valTallosEt, int IDBodega)
        //{
        //    if (VariablesGlobales.vgIDBodega == IDBodega)
        //    {
        //        lstlectura.Add(new LstLectura(codigoEtiqueta, valTallosEt));
        //        adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstlectura);

        //        lstLectura.Adapter = new AdLstLectura(Activity, lstlectura);
        //    }

        //    var CantEtiquetas = lstlectura.Count;

        //    btnIngresar.Text = "INGRESAR - " + CantEtiquetas;

        //    //Toast.MakeText(Application.Context, "OK", ToastLength.Short).Show();
        //    txtCodigo.Text = "";
        //    txtPresentacion.Text = "";
        //    txtProducto.Text = "";
        //}

        private void ObtenerInformacionEtiqueta()
        {
            String PresentacionEt = "";
            String ProductoEt = "";
            String ValTallosEt = "";
            String var = txtCodigo.Text;
            int varIDBodega = 0;

            //int boolVal = 0;
            //foreach (LstLecturaIP item in this.lstlectura)
            //{
            //    if (item.codigo == txtCodigo.Text)
            //    {
            //        boolVal = 1;
            //    }
            //}

            if (var != "")
            {           
                    int tam_var = var.Length;
                    String CodigoEtiqueta = var.Substring(0, tam_var - 1);

                    //SqlDataReader Etiquetas;

                    //ConexionServidor con = new ConexionServidor();
                    //Etiquetas = con.ConsultaEtiquetaINP(CodigoEtiqueta, VariablesGlobales.vgIDBodega);

                db = new ConexionLocal(Application.Context);
                ICursor EtiquetasIXP = db.ConsultaEtiquetasDisponiblesIXP(CodigoEtiqueta, VariablesGlobales.vgIDBodega);


                //if (Etiquetas.HasRows)
                //    }     
                if (EtiquetasIXP.MoveToFirst())
                {

                    do
                    {
                        PresentacionEt = EtiquetasIXP.GetString(0);
                        ProductoEt = EtiquetasIXP.GetString(2);
                        ValTallosEt = Convert.ToString(EtiquetasIXP.GetDouble(1));
                        varIDBodega = EtiquetasIXP.GetInt(3);
                        FlagDisponible = 1;
                        txtPresentacion.Text = PresentacionEt;
                        txtProducto.Text = ProductoEt;
                        txtValTallos.RequestFocus();
                        etCalidad.RequestFocus();

                    } while (EtiquetasIXP.MoveToNext());
                    txtValTallos.RequestFocus();
                    etCosechador.RequestFocus();
                }
                else
                    {
                        FlagDisponible = 0;
                        txtValTallos.RequestFocus();
                        etCalidad.RequestFocus();
                    //Toast.MakeText(Application.Context, "No esta disponible ó Etiqueta leida en otro documento", ToastLength.Short).Show();
                }

                    //if (ValTallosEt != "")
                    //{
                    //    if (Convert.ToInt32(ValTallosEt) == 0)
                    //    {
                    //        txtCodigo.Text = "";
                    //        Toast.MakeText(Application.Context, "No tiene tallos disponibles para consumo", ToastLength.Short).Show();
                    //    }
                    //    else
                    //    {
                    //        txtPresentacion.Text = PresentacionEt;
                    //        txtProducto.Text = ProductoEt;
                    //        txtValTallos.Text = ValTallosEt;
                    //    }
                    //}
                    //else
                    //{
                    //    txtCodigo.Text = "";
                    //}
                }
                else
                {
                    txtCodigo.Text = "";
                    txtPresentacion.Text = "";
                    txtProducto.Text = "";
                    player = new MediaPlayer();
                    String FilePath = "/sdcard/Music/TonoError.mp3";
                    player.SetDataSource(FilePath);
                    player.Prepare();
                    player.Start();
                    Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
                }
        }
        //public void ObtenerInformacionEmpleados(int ID)
        //{
        //    db = new ConexionLocal(Application.Context);
        //    ICursor cursorEmpleados = db.ConsultaEmpleados(ID);

        //    int Ide = 0;

        //    if (cursorEmpleados.MoveToFirst())
        //    {
        //        do
        //        {
        //            Ide = 1;
        //        } while (cursorEmpleados.MoveToNext());
        //    }
        //    else
        //    {
        //        Ide = 0;
        //    }            
        //}


        private void BtnIngresar_Click(object sender, EventArgs e){
           
            var builder = new AlertDialog.Builder(this.Activity);
            //Para saber que mensaje mostrar
            if (VariablesGlobales.vgFlagExterno == 1)
            {
                mensaje = "¿Desea generar el traslado?";
            }
            else
            {
                mensaje = "¿Desea generar el traslado y la confirmación?";
                //JH para crear el traslado y la confirmación directa
                //var builder = new AlertDialog.Builder(activity);
                builder.SetIcon(Resource.Drawable.warning);
                builder.SetTitle("Advertencia");
                builder.SetMessage(mensaje);
                builder.SetPositiveButton("Si", OkTrasladoConfirmacion);
                builder.SetNegativeButton("No", (IntentSender, args) =>
                {
                    builder.Dispose();
                });
                builder.Show();
            }
            
            builder.SetIcon(Resource.Drawable.warning);
            builder.SetTitle("Advertencia");
            builder.SetMessage("¿Esta seguro de registar el documento?");
            builder.SetPositiveButton("Si", OkAction);
            builder.SetNegativeButton("No", (IntentSender, args) => {
                builder.Dispose();
            });
            builder.Show();            

        }
        
        private void OkAction(object sender, DialogClickEventArgs e)
        {           
            GenerarArchivo();

            db = new ConexionLocal(Application.Context);
            ICursor cursorDatosConexion = db.ConsultaDatosConexion();

            if (cursorDatosConexion.MoveToFirst())
            {
                do
                {
                    direccionIPLocal = cursorDatosConexion.GetString(1);
                    direccionIPPublica = cursorDatosConexion.GetString(2);
                    baseDatos = cursorDatosConexion.GetString(3);
                    usuario = cursorDatosConexion.GetString(4);
                    contrasena = cursorDatosConexion.GetString(5);
                    VariablesGlobales.vgFlagExterno  = cursorDatosConexion.GetInt(6);

                } while (cursorDatosConexion.MoveToNext());
            }

            cadenaConexionLocal = @"data source= " + direccionIPLocal + ";initial catalog=" + baseDatos + ";user id=" + usuario + ";password= " + contrasena + ";Connect Timeout=0";
           
            conexion = new SqlConnection(cadenaConexionLocal);

            int valConex = 0;
            try
            {
                conexion.Open();
                valConex = 1;
            }
            catch (SqlException Exp)
            {
                Toast.MakeText(Application.Context, "No estas conectado a internet", ToastLength.Long).Show();
                throw;
            }

            if (valConex == 1)
            {
                MyTaskSync tarea = new MyTaskSync(progress, conexion, this.Activity);
                tarea.Execute();
            }
        }

        private void GenerarArchivo()
        {
            //Etiquetas Duplicadas    
            ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirIPNoDisp(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            string NombreArchivo = VariablesGlobales.vgIDBodega + "Ing" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    String Ing = cursorEtiquetas.GetString(0) + "," + cursorEtiquetas.GetInt(1) + "," + cursorEtiquetas.GetInt(3) + "," + cursorEtiquetas.GetInt(2) + "," + cursorEtiquetas.GetString(4) + "," + 0 + "," + 0 + "\r\n";
                    
                    GeneraTXT = new GeneraTXT();
                    GeneraTXT.GenerarArchivoTxt(Ing, NombreArchivo);
                } while (cursorEtiquetas.MoveToNext());
            }

            //Etiquetas Ingresadas
            ICursor cursorEtiquetasSinSubir = db.ConsultaEtiquetasSinSubirIP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            string NombreArchivoSinSubir = VariablesGlobales.vgIDBodega + "IngSubidas" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");

            if (cursorEtiquetasSinSubir.MoveToFirst())
            {
                do
                {
                    String Ing = cursorEtiquetasSinSubir.GetString(0) + "," + cursorEtiquetasSinSubir.GetInt(1) + "," + cursorEtiquetasSinSubir.GetInt(3) + "," + cursorEtiquetasSinSubir.GetInt(2) + "," + cursorEtiquetasSinSubir.GetString(4) + "," + 0 + "," + 0 + "\r\n";

                    GeneraTXT = new GeneraTXT();
                    GeneraTXT.GenerarArchivoTxt(Ing, NombreArchivoSinSubir);
                } while (cursorEtiquetasSinSubir.MoveToNext());
            }
        }

        private void OkTrasladoConfirmacion(object sender, DialogClickEventArgs e)
        {
            try
            {
                //progress.SetMessage("Generando el traslado y la confirmación ...." );
                //progress.Show();
                ConexionServidor con = new ConexionServidor();
                con.GeneraTrspasoConfirmacion(VariablesGlobales.vgIDConsumo);
                progress.Dismiss();
                Toast.MakeText(Application.Context, "Se genero los documentos", ToastLength.Short).Show();
            }
            catch (Exception )
            {
                //foreach (SqlError error in e.Errors)
                //{
                //    var FechaRegistro = DateTime.Now.ToString();
                //    var errors = FechaRegistro + error.Number.ToString() + error.Message.ToString();

                //    GenerarArchivoTxt(errors, "errores.txt");

                player = new MediaPlayer();

                String FilePath = "/sdcard/Music/TonoError.mp3";
                player.SetDataSource(FilePath);
                player.Prepare();
                player.Start();

                //}
                throw;
            }            
        }

        private void GenerarArchivoTxt(string codigo, string NombreArchivo, int tipo)
        {
           // string NombreArchivo = "Prod_" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");
           
            
            string pathTxt = "/sdcard/Download/" + NombreArchivo;

            if (tipo == 1) //Crear archivo al leer sin validar
            {
               
                StringBuilder constructor = new StringBuilder();
                String hora = DateTime.Now.ToString("hh:mm:ss");


                String cadena = txtCodigo.Text + "," + etTallosxRamo.Text + "," + etCosechador.Text + "," + etCalidad.Text + "," + hora + "," + 0 + "," + 0 + "\r\n";

                if (!File.Exists(pathTxt))
                {
                    StreamWriter streamWriter = File.CreateText(pathTxt);                   
                    constructor.AppendLine(cadena);
                    streamWriter.Dispose();
                }
                else
                {
                    constructor.AppendLine(cadena);
                }
                //WriteAllText
                File.AppendAllText(pathTxt, constructor.ToString());

            }
            else //crea archivo al subir produccion
            {
                ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirIP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

                StringBuilder constructor = new StringBuilder();

                if (!File.Exists(pathTxt))
                {
                    StreamWriter streamWriter = File.CreateText(pathTxt);
                }

                if (File.Exists(pathTxt))
                {
                    if (cursorEtiquetas.MoveToFirst())
                    {
                        do
                        {
                            String a = cursorEtiquetas.GetString(0) + "," + cursorEtiquetas.GetInt(1) + "," + cursorEtiquetas.GetInt(3) + "," + cursorEtiquetas.GetInt(2) + "," + cursorEtiquetas.GetString(4) + "," + 0 + "," + 0 + "\r\n";

                            constructor.AppendLine(a);
                            File.AppendAllText(pathTxt, constructor.ToString());
                        } while (cursorEtiquetas.MoveToNext());
                    }
                }
            }       
        }

        public class MyTaskSync : AsyncTask
        {
            private ConexionLocal db;
            SqlDataReader CabezaDocumento;
            SqlDataReader CabezaDocumentoPrevio;
            ConexionServidor con = new ConexionServidor();
            ProgressDialog progress;
            SqlConnection conexion;
            String tablaTemporal;
            String NombreEquipo = "";
            Activity activity;
            public MyTaskSync(ProgressDialog progress,  SqlConnection conexion, Activity activity)
            {
                this.progress = progress;
                this.conexion = conexion;
                this.activity = activity;
            }

            protected override void OnPreExecute()
            {
                progress.SetMessage("Generando documento ...");
                progress.Show();
            }
            protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
            {
                db = new ConexionLocal(Application.Context);
                VerificaTablaTmpIngresosXProduccion();
                IngresaEtiquetasTablaTemporal();
                return "d";
            }

            private void VerificaTablaTmpIngresosXProduccion()
            {
                SqlDataReader tablaEtiquetas;

                ConexionServidor con = new ConexionServidor();

                String id = Secure.GetString(Application.Context.ContentResolver, Secure.AndroidId);
                int tam_var = id.Length;
                String IDLectora = id.Substring(tam_var - 2, 2);

                NombreEquipo = "Lec" + IDLectora;
                var NombreTabla = "AppMovilInventarios_Lec" +  IDLectora;

                tablaEtiquetas = con.VerificaTablaTmpIngresosXProduccion(VariablesGlobales.vgNombreUsuario, NombreTabla);

                if (tablaEtiquetas.HasRows)
                {
                    while (tablaEtiquetas.Read())
                    {
                        tablaTemporal = tablaEtiquetas.GetString(0);
                    }
                }
            }
            
            private void IngresaEtiquetasTablaTemporal()
            {
                ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirIP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

                string sql = "INSERT INTO " + tablaTemporal + "(CodBarras,TallosXRamo,Cosechador,Calidad,Hora,Long,PresCos) VALUES (@Etiqueta,@TallosXRamo,@Cosechador,@Calidad,@Hora,@Long,@PresCos)";

                if (cursorEtiquetas.MoveToFirst())
                {
                    do
                    {
                        using (SqlCommand comando = new SqlCommand(sql, conexion))
                        {
                            if (cursorEtiquetas.GetString(0).Length == 15)
                            {
                                comando.Parameters.Add("@Etiqueta", SqlDbType.VarChar).Value = cursorEtiquetas.GetString(0);
                                comando.Parameters.Add("@TallosXRamo", SqlDbType.Int).Value = cursorEtiquetas.GetInt(1);
                                comando.Parameters.Add("@Cosechador", SqlDbType.Int).Value = cursorEtiquetas.GetInt(3);
                                comando.Parameters.Add("@Calidad", SqlDbType.Int).Value = cursorEtiquetas.GetInt(2);
                                comando.Parameters.Add("@Hora", SqlDbType.VarChar).Value = cursorEtiquetas.GetString(4);
                                comando.Parameters.Add("@Long", SqlDbType.Int).Value = 0;
                                comando.Parameters.Add("@PresCos", SqlDbType.Int).Value = 0;

                                comando.CommandType = CommandType.Text;
                                comando.ExecuteNonQuery();
                            }                           
                        }
                    } while (cursorEtiquetas.MoveToNext());
                }
                insertaDocumento();
            }

            private void insertaDocumento()
            {
                //Crear cabeza documento previo 
                DateTime fecha = DateTime.Now;
                String fecha1 = fecha.ToShortDateString();

                String Fecha2 = string.Format("{0:MM/dd/yyyy}", fecha);

                String hora = fecha.ToString("hh:mm:ss");

                int idPrevio = 0;
                
                CabezaDocumentoPrevio = con.GeneraDetallesEtiquetasDocFlorPrevioIP(Fecha2, hora,VariablesGlobales.vgIDCompanias, VariablesGlobales.vgIDUnidades,1,VariablesGlobales.vgIDBodega,VariablesGlobales.vgIDUsuario,"",1);
                
                if (CabezaDocumentoPrevio.HasRows)
                {
                    while (CabezaDocumentoPrevio.Read())
                    {
                        idPrevio = CabezaDocumentoPrevio.GetInt32(0);
                    }
                }

                con.EliminaEtiquetasTemporalAfan(NombreEquipo, VariablesGlobales.vgNombreUsuario);

                con.LLenaAfanXUsuarioMaquina(VariablesGlobales.vgNombreUsuario, NombreEquipo, tablaTemporal);


                con.VerificaEtiquetasDocIngresoXProduccionFlorPrevio(1,"",NombreEquipo,VariablesGlobales.vgNombreUsuario,idPrevio);

                //VariablesGlobales.vgIDConsumo = 0;
                ////CABEZA DOCUMENTO
                CabezaDocumento = con.GeneraCabezaDocFlor(6, VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDUnidades, VariablesGlobales.vgIDCompanias, 0, 0, 0, 0, 0, 0, 0, VariablesGlobales.vgIDUsuario,"");
                

                if (CabezaDocumento.HasRows)
                {
                    while (CabezaDocumento.Read())
                    {
                        VariablesGlobales.vgIDConsumo = CabezaDocumento.GetInt32(0);
                    }
                }
                
                ////DETTALLES ETIQUETAS DOCUMENTO
                con.GeneraDetallesEtiquetasDocFlorIP(idPrevio, VariablesGlobales.vgIDConsumo);

                //COMPACTA DETTALLES ETIQUETAS DOCUMENTO             
                String FechaRotacion = string.Format("{0:MM/dd/yyyy}", fecha);
                con.CompactaDocIngProdFlorDetalles(VariablesGlobales.vgIDConsumo, VariablesGlobales.vgIDUnidades, FechaRotacion);

                con.EliminaEtiquetasTemporalAfan(NombreEquipo, VariablesGlobales.vgNombreUsuario);
            }

            private void LimpiarTablaEtiquetas()
            {
                db = new ConexionLocal(Application.Context);
                db.EliminarDatosEtiquetasIP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            }
            protected override void OnPostExecute(Java.Lang.Object result)
            {
                Toast.MakeText(Application.Context, "Documento Registrado", ToastLength.Long).Show();                
                progress.Dismiss();     
                LimpiarTablaEtiquetas();
            }
        }        
    }
}