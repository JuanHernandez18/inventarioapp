﻿using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Views;
using Android.Widget;
using InventariosApp.Clases;
using System;
using System.Data.SqlClient;

namespace InventariosApp.Fragmentos
{
    internal class DialogAppAjustes: DialogFragment
    {
        private Context context;
        EditText direccionIPLocal;
        EditText direccionIPPublica;
        EditText baseDatos;
        EditText usuario;
        EditText contrasena;
        Button btnTest;
        Button btnGuardar;
        CheckBox Externo;
        int FlagExterno;

        ConexionLocal db;
        int FlagDatosConexion = 0;
        public DialogAppAjustes(Context context)
        {
            this.context = context;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.FgAjustes, container, false);

            direccionIPLocal = view.FindViewById<EditText>(Resource.Id.DireccionIPLocal);
            direccionIPPublica = view.FindViewById<EditText>(Resource.Id.DireccionIPPublica);
            baseDatos = view.FindViewById<EditText>(Resource.Id.BaseDatos);
            usuario = view.FindViewById<EditText>(Resource.Id.Usuario);
            contrasena = view.FindViewById<EditText>(Resource.Id.Contrasena);
            btnTest = view.FindViewById<Button>(Resource.Id.btnTest);
            btnGuardar = view.FindViewById<Button>(Resource.Id.btnGuardar);
            Externo = view.FindViewById<CheckBox>(Resource.Id.chkExterno);

            btnTest.Click += btnTestClick;
            btnGuardar.Click += btnGuardarClick;

            LimpiarFormulario();
            CargarInfoDatosConexion();

            return view;
        }
        private void LimpiarFormulario()
        {
            direccionIPLocal.Text = "";
            direccionIPPublica.Text = "";
            baseDatos.Text = "";
            usuario.Text = "";
            contrasena.Text = "";
        }

        private void CargarInfoDatosConexion()
        {
            db = new ConexionLocal(Application.Context);
            ICursor cursorDatosConexion = db.ConsultaDatosConexion();


            if (cursorDatosConexion.MoveToFirst())
            {
                FlagDatosConexion = 1;
                do
                {
                    direccionIPLocal.Text = cursorDatosConexion.GetString(1);
                    direccionIPPublica.Text = cursorDatosConexion.GetString(2);
                    baseDatos.Text = cursorDatosConexion.GetString(3);
                    usuario.Text = cursorDatosConexion.GetString(4);
                    contrasena.Text = cursorDatosConexion.GetString(5);

                } while (cursorDatosConexion.MoveToNext());
            }

        }

        private void btnTestClick(object sender, EventArgs e)
        {
            String CadenaConexionLocal;
            String CadenaConexionPublica;
            SqlConnection conexion;

            CadenaConexionLocal = @"data source=" + direccionIPLocal.Text + ";initial catalog=" + baseDatos.Text + ";user id=" + usuario.Text + ";password=" + contrasena.Text + ";Connect Timeout=10";
            CadenaConexionPublica = @"data source=" + direccionIPPublica.Text + ";initial catalog=" + baseDatos.Text + ";user id=" + usuario.Text + ";password=" + contrasena.Text + ";Connect Timeout=10";



            conexion = new SqlConnection(CadenaConexionLocal);
            try
            {
                conexion.Open();
                Toast.MakeText(Application.Context, "Conexion válida", ToastLength.Long).Show();
                btnGuardar.Visibility = ViewStates.Visible;
            }
            catch (SqlException Exp)
            {
                conexion = new SqlConnection(CadenaConexionPublica);
                try
                {
                    conexion.Open();
                    Toast.MakeText(Application.Context, "Conexion válida", ToastLength.Long).Show();
                    btnGuardar.Visibility = ViewStates.Visible;

                }
                catch (SqlException Exp1)
                {
                    Toast.MakeText(Application.Context, "Error de conexion", ToastLength.Long).Show();
                    btnGuardar.Visibility = ViewStates.Invisible;
                }
            }
        }

        private void btnGuardarClick(object sender, EventArgs e)
        {
            db = new ConexionLocal(Application.Context);
            if (Externo.Checked)
            { VariablesGlobales.vgFlagExterno = 1; }
            else
            { VariablesGlobales.vgFlagExterno = 0; }

            if (FlagDatosConexion == 0)
            {
                db.GuardarDatosConexion(direccionIPLocal.Text, direccionIPPublica.Text, baseDatos.Text, usuario.Text, contrasena.Text,VariablesGlobales.vgFlagExterno);
                btnGuardar.Visibility = ViewStates.Invisible;
                FlagDatosConexion = 1;
            }
            else
            {
                db.ActualizarDatosConexion(direccionIPLocal.Text, direccionIPPublica.Text, baseDatos.Text, usuario.Text, contrasena.Text, VariablesGlobales.vgFlagExterno);
                btnGuardar.Visibility = ViewStates.Invisible;
                // db.GuardarDatosConexion(direccionIPLocal.Text, direccionIPPublica.Text, baseDatos.Text, usuario.Text, contrasena.Text);
                // Toast.MakeText(Application.Context, "Registro Realizado", ToastLength.Long).Show();
            }            
            db = new ConexionLocal(Application.Context);
            ConexionServidor con = new ConexionServidor();
            SqlDataReader DatosMaestros;          


            //Usuarios            
            DatosMaestros = con.AppUsrs();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarUsuarios(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetString(4), DatosMaestros.GetInt32(5), DatosMaestros.GetString(6));
                }
            }
            else
            {
                //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                Toast.MakeText(Application.Context, "Tabla de Usuarios No disponible", ToastLength.Short).Show();
            }
            Toast.MakeText(Application.Context, "Registro Realizado", ToastLength.Long).Show();
        }

        
    }
}