﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.Media;
using Android.Net.Wifi;
using Android.OS;
using Android.Views;
using Android.Widget;
using InventariosApp.Clases;
using ZXing.Mobile;
using static Android.Provider.Settings;
using Environment = Android.OS.Environment;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using RestSharp;
using System.Net;


using RestSharp.Deserializers;

namespace InventariosApp.Fragmentos
{
    public class ActTablasMaestras : Fragment
    {


        //private MediaPlayer player;
        private ConexionLocal db;
        public ProgressDialog progress;

        Button btnActualizar;
        //EditText tallos;
        private Fragment mCurrentFragment;


        //ConexionLocal db;
        public override void OnCreate(Bundle savedInstanceState)
        {


            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.FgActTablasMaestras, container, false);

            btnActualizar = view.FindViewById<Button>(Resource.Id.btnActualizar);

            btnActualizar.Click += btnActualizarInfo;

            validacionInicial2();

            return view;
        }

        private void btnActualizarInfo(object sender, EventArgs e)
        {
            //var builder = new AlertDialog.Builder(this.Activity);
            //builder.SetIcon(Resource.Drawable.warning);
            //builder.SetTitle("Advertencia");
            //builder.SetMessage("¿Esta seguro de registar el documento?");
            //builder.SetPositiveButton("Si", OkAction);
            //builder.SetNegativeButton("No", (IntentSender, args) => {
            //    builder.Dispose();
            //});
            //builder.Show();
        }

        private void validacionInicial2()
        {
            var client = new RestClient("http://10.99.0.41:8084");
            // var client = new RestClient("https://samples.openweathermap.org/data/2.5/forecast?q=M%C3%BCnchen,DE&appid=b6907d289e10d714a6e88b30761fae22");

            // client.Authenticator = new HttpBasicAuthenticator(username, password);
            String query = "/api/Company?id=2";
            //var request = new RestRequest("resource/{id}", Method.GET);
            var request = new RestRequest(query, Method.GET);
            IRestResponse response = client.Execute(request);
            var context = response.Content;
            //var a = context.Replace;
            String reemplazar = @"\";
            int n = reemplazar.Length;
            //String reemplazar2 = reemplazar.r (n);
            //string s = context.Replace(reemplazar, string .Empty);
            var s = context.Replace("\\\"", "'");
            //s = context.Replace("\"", string.Empty);
            s = s.Replace("\"", string.Empty);
            //var definicion = new { ID = 0, Nombre = "" };
            //var listaDefinicion = new[] { definicion };
            Compania m = JsonConvert.DeserializeObject<Compania>(context);

            string name = m.Nombre;


            //var listProductos = JsonConvert.DeserializeAnonymousType(context, listaDefinicion);

            //foreach (var prod in listProductos)
            //{
            //    Console.WriteLine("Código: " + prod.ID + " - Cantidad: " + prod.Nombre);
            //}


            //var listCompania = JsonConvert.DeserializeObject<List<Compania>>(context);

            //foreach (dynamic com in listCompania)
            //{
            //    Console.WriteLine("Código: " + com.ID + " - Cantidad: " + com.Nombre);
            //}


            //Compania compania;
            ////HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@"http://10.99.0.41:8084/api/Company?id=1");
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@"https://samples.openweathermap.org/data/2.5/forecast?q=M%C3%BCnchen,DE&appid=b6907d289e10d714a6e88b30761fae22");
            //using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            //using (System.IO.Stream stream = response.GetResponseStream())
            //using (StreamReader reader = new StreamReader(stream))
            //{
            //    var json = reader.ReadToEnd();                
            //    compania = JsonConvert.DeserializeObject<Compania>(json);

            //}
            //Console.WriteLine("La temperatura en Madrid es: " + compania.Nombre);



            Compania serializer1 = new Compania();
            dynamic obje = JsonConvert.DeserializeObject(context);
            dynamic obje2 = JsonConvert.DeserializeObject(s);



            //List<Compania> compania = JsonConvert.DeserializeObject<List<Compania>>(context, new JsonSerializerSettings
            //{
            //    NullValueHandling = NullValueHandling.Ignore
            //});
            //List<Compania> compania = obje;
            List<Compania> compania = obje2;

            foreach (var i in compania)
            {
                var id = @i.ID;
                var a = @i.Nombre;
            }


            // Two ways to get the result:
            //string rawResponse = response.Content;
            //    Compania myClass = new JsonDeserializer().Deserialize<Compania>(context);



            //var content = response.Content; // raw content as string          
            //if (response.ErrorException != null)
            //{
            //    const string message = "Error retrieving response. Check inner details for more info.";
            //    var myException = new ApplicationException(message, response.ErrorException);
            //    throw myException;
            //}

            //// Important and simple line. response.rawbytes was what I was missing. 
            //string strReportId;
            //strReportId =response.RawBytes, response.ContentType, "sample.doc";

            // or automatically deserialize result
            // return content type is sniffed but can be explicitly set via RestClient.AddHandler();
            //RestResponse<Person> response2 = client.Execute<Person>(request);
            //var name = response2.Data.Name;

            //RestResponse<Compania> response2 = client.Execute<Compania>(request);
            //var name = response2.Data.Nombre;

            //// easy async support
            //client.ExecuteAsync(request, response => {
            //    Console.WriteLine(response.Content);
            //});
            //string name;
            //// async with deserialization
            //var asyncHandle = client.ExecuteAsync<Compania>(request, response =>
            //{
            //    name =response.Data.Nombre;
            //});

            //// abort the request on demand
            //asyncHandle.Abort();
        }


        private void validacionInicial()
        {
            ///////////////////////////////////pruebas de llenado de la tabla de usuario
            ///
            db = new ConexionLocal(Application.Context);
            ICursor cLoginOk = db.ConsultaLoginUsuarios(VariablesGlobales.vgNombreUsuario, VariablesGlobales.vgNombreUsuario);
            int eti = 0;
            if (cLoginOk.MoveToFirst())
            {
                db = new ConexionLocal(Application.Context);
                db.EliminarUsuario();
            }
            int ID = 0;
            String LOGIN = "";
            String PASSWORD = "";
            String NOMBRE = "";
            String APELLIDOS = "";
            int IDAPPPERFILESUSRS = 0;
            String EMAIL = "";

            SqlDataReader UsuariosC;

            ConexionServidor con = new ConexionServidor();

            UsuariosC = con.ConsultaUsuarioApp();


            if (UsuariosC != null && UsuariosC.HasRows)
            {
                while (UsuariosC.Read())
                {
                    //ID = Convert.ToString(UsuariosC.GetDouble(0));
                    ID = UsuariosC.GetInt32(0);
                    LOGIN = UsuariosC.GetString(1);
                    PASSWORD = UsuariosC.GetString(2);
                    NOMBRE = UsuariosC.GetString(3);
                    APELLIDOS = UsuariosC.GetString(4);
                    IDAPPPERFILESUSRS = UsuariosC.GetInt32(5);
                    EMAIL = "";//UsuariosC.GetString(6);

                    GuardarUsuarios(ID, LOGIN, PASSWORD, NOMBRE, APELLIDOS, IDAPPPERFILESUSRS, EMAIL);
                }
            }



            db = new ConexionLocal(Application.Context);
            ICursor cCompania = db.ConsultaCompania();
            int Compania = 0;
            if (cCompania.MoveToFirst())
            {
                do
                {
                    Compania = cCompania.GetInt(0);
                } while (cCompania.MoveToNext());
            }





            String DIRECCION = "";
            String TELEFONO = "";
            int FLAGACTIVO = 0;

            SqlDataReader CompaniaC;

            //ConexionServidor con = new ConexionServidor();

            CompaniaC = con.ConsultaCompania();


            if (CompaniaC != null && CompaniaC.HasRows)
            {
                while (CompaniaC.Read())
                {
                    //ID = Convert.ToString(UsuariosC.GetDouble(0));
                    ID = CompaniaC.GetInt32(0);
                    NOMBRE = CompaniaC.GetString(1);
                    DIRECCION = CompaniaC.GetString(2);
                    TELEFONO = CompaniaC.GetString(3);
                    FLAGACTIVO = CompaniaC.GetInt32(4);
                    //EMAIL = "";//UsuariosC.GetString(6);

                    GuardarCompanias(ID, NOMBRE, DIRECCION, TELEFONO, FLAGACTIVO);
                }
            }


            SqlDataReader UnidadNegocioC;

            //ConexionServidor con = new ConexionServidor();

            UnidadNegocioC = con.ConsultaUnidadNegocio();


            if (UnidadNegocioC != null && UnidadNegocioC.HasRows)
            {
                while (UnidadNegocioC.Read())
                {
                    //ID = Convert.ToString(UsuariosC.GetDouble(0));
                    ID = UnidadNegocioC.GetInt32(0);
                    NOMBRE = UnidadNegocioC.GetString(1);
                    DIRECCION = UnidadNegocioC.GetString(2);
                    TELEFONO = UnidadNegocioC.GetString(3);
                    FLAGACTIVO = UnidadNegocioC.GetInt32(4);
                    //EMAIL = "";//UsuariosC.GetString(6);

                    //GuardarUnidadNegocios(ID, NOMBRE, DIRECCION, TELEFONO, FLAGACTIVO);
                }
            }




            ///////////////////////////////////FIN pruebas de llenado de la tabla de usuario FIN
            //db = new ConexionLocal(Application.Context);
            //ICursor cursorDatosConexion = db.ConsultaDatosConexion();


            //string baseDatos = "";
            //if (cursorDatosConexion.MoveToFirst())
            //{
            //    do
            //    {
            //        baseDatos = cursorDatosConexion.GetString(3);
            //    } while (cursorDatosConexion.MoveToNext());
            //}

            //if (baseDatos != "")
            //{
            //    CargarSpinnerCompanias();
            //    CargarSpinnerUnidades();
            //    CargarSpinnerBodegas();
            //    CargarSpinnerDocumentos();
            //    CargarSpinnerCentroC();
            //    CargarSpinnerCamiones();
            //    CargarSpinnerConductor();
            //    CargarSpinnerMotivo();
            //    CargarSpinnerDestino();
            //    CargarSpinnerBodegaDest();
            //    CargarSpinnerCodLote();
            //}
        }

        //*********************************************************************************************************************//
        //                                               CARGA DE SPINNER
        //**********************************************************************************************************************//


        //*********************************************************************************************************************//
        //                                               EVENTO BOTON
        //**********************************************************************************************************************/

        //private void btnActualizar(object sender, EventArgs e)
        //{

        //}
        public void ReplaceFragment(Fragment fragment)
        {
            if (fragment.IsVisible)
            {
                return;
            }
            var trans = FragmentManager.BeginTransaction();
            trans.Replace(Resource.Id.fragmentContainer, fragment);
            trans.AddToBackStack(null);
            trans.Commit();

            mCurrentFragment = fragment;
        }

        private void GuardarUsuarios(int Id, String Login, String Password, String Nombre, String Apellidos, int IdApperfilesUsrs, String Email)
        {
            db = new ConexionLocal(Application.Context);
            db.GuardarUsuarios(Id, Login, Password, Nombre, Apellidos, IdApperfilesUsrs, Email);
        }

        private void GuardarCompanias(int Id, String Nombre, String Direccion, String Telefono, int FlagActivo)
        {
            db = new ConexionLocal(Application.Context);
            db.GuardarCompanias(Id, Nombre, Direccion, Telefono, FlagActivo);
        }


        //static async Task RunAsync()


        //{

        //    // Update port # in the following line.
        //    client.BaseAddress = new Uri("http://localhost:64195/");
        //    client.DefaultRequestHeaders.Accept.Clear();
        //    client.DefaultRequestHeaders.Accept.Add(
        //        new MediaTypeWithQualityHeaderValue("application/json"));

        //}

        //private void GuardarCompanias(int Id, String Nombre, String Direccion, String Telefono, int FlagActivo)
        //{
        //    HttpClient client = new HttpClient();
        //    db.GuardarCompanias(Id, Nombre, Direccion, Telefono, FlagActivo);
        //}




        //Public Async CargaActivosFijos()
        //{
        //    HttpClient client = new HttpClient();            
        //    String Json = Await HttpClient.GetStringAsync("http://localhost:64482/api/ModeloAF?prmOptional=1");

        //    GVActivosFijos.DataSource = ConvertJsonStringToDataSet(Json);
        //    GVActivosFijos.DataBind();

        //}

        //Private Function ConvertJsonStringToDataSet(ByVal jsonString As String) 
        //{
        //    Try
        //        Var xd As XmlDocument = New XmlDocument();
        //        jsonString = "{ ""rootNode"": {" & jsonString.Trim().TrimStart("{"c).TrimEnd("}"c) & "} }";
        //        xd = CType(JsonConvert.DeserializeXmlNode(jsonString), XmlDocument);
        //Var ds As DataSet = New DataSet();
        //ds.ReadXml(New XmlNodeReader(xd));
        //        Return ds;
        //Catch ex As Exception
        //        Throw New ArgumentException(ex.Message);
        //End Try
        //    }


    }
}