﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using InventariosApp.Clases;
using ZXing.Mobile;
using static Android.Provider.Settings;

namespace InventariosApp.Fragmentos
{
    //class LectorTrasladoEnv
    public class LectorTrasladoEnv : Fragment

    {
        String codigoActualizar;
        Button btnIngresar;
        ConexionLocal db;
        String direccionIPLocal;
        String direccionIPPublica;
        String baseDatos;
        String usuario;
        String contrasena;
        String cadenaConexionLocal;
        String cadenaConexionPublica;
        SqlConnection conexion;
        ProgressDialog progress;

        EditText txtCodigo;
        ListView lstLectura;
        TextView txtValTallos;
        TextView txtPresentacion;
        TextView txtProducto;
        Button btnManual;
        List<ListBD> lstCalidades = new List<ListBD>();
        List<LstLecturaIP> lstlectura = new List<LstLecturaIP>();

        EditText etTallosxRamo;
        EditText etCosechador;
        EditText etCalidad;
        // Spinner spCalidades;
        ArrayAdapter adapter;

        EditText CalidadActualizar;
        int etiExist = 0;

        //  EditText IDCalidad;
        private AlertDialog.Builder dialog;
        AlertDialog.Builder builder;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View view = inflater.Inflate(Resource.Layout.FgLectorTrasladoEnv, container, false);

            progress = new ProgressDialog(this.Activity);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetCancelable(false);


            txtCodigo = view.FindViewById<EditText>(Resource.Id.txtCodigo);
            lstLectura = view.FindViewById<ListView>(Resource.Id.lstCodigos);
            txtValTallos = view.FindViewById<TextView>(Resource.Id.valTallos);
            txtPresentacion = view.FindViewById<TextView>(Resource.Id.txtPresentacion);
            txtProducto = view.FindViewById<TextView>(Resource.Id.txtProducto);
            btnIngresar = view.FindViewById<Button>(Resource.Id.btnIngresar);
            btnManual = view.FindViewById<Button>(Resource.Id.btnManual);

            etTallosxRamo = view.FindViewById<EditText>(Resource.Id.etTallos);
            etCosechador = view.FindViewById<EditText>(Resource.Id.etCosechador);
            etCalidad = view.FindViewById<EditText>(Resource.Id.etCalidad);
            // spCalidades = view.FindViewById<Spinner>(Resource.Id.etCalidad);

            // IDCalidad = view.FindViewById<EditText>(Resource.Id.idCalidad);

            txtCodigo.RequestFocus();
            btnIngresar.Click += BtnIngresar_Click;
            //   btnManual.Click += BtnManual_Click; ;
            //Conexion();
            MobileBarcodeScanner.Initialize(Activity.Application);

            lstlectura.Clear();
            lstLectura.SetAdapter(null);
            txtCodigo.Text = "";
            txtPresentacion.Text = "";
            txtProducto.Text = "";
            etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);

            if (VariablesGlobales.FlagCamaraLector == 0)
            {
                btnManual.Click += BtnManual_Click;
                txtCodigo.TextChanged += (object sender, Android.Text.TextChangedEventArgs e) =>
                {

                    if (txtCodigo.Text.Length == 15)
                    {
                        string NombreArchivo = "Prod_" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");
                        ObtenerInformacionEtiqueta();
                        btnManual.Enabled = true;
                        txtValTallos.RequestFocus();
                    }
                    else
                    {
                        if (txtCodigo.Text.Length != 0)
                        {
                            Toast.MakeText(Application.Context, "Codigo Inválido", ToastLength.Long).Show();
                            btnManual.Enabled = false;
                        }
                    }
                };
            }
            else
            {
                btnManual.Click += async delegate
                {
                    var scanner = new MobileBarcodeScanner()
                    {
                        TopText = "Acerca la camara al elemento",
                        BottomText = "Toca la pantalla para enfocar",
                        CancelButtonText = "Cancelar"
                    };
                    try
                    {
                        var result = await scanner.Scan();
                        if (result != null)
                            txtCodigo.Text = result.ToString();

                        if (txtCodigo.Text.Length == 15)
                        {
                            string NombreArchivo = "Prod_" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");
                            GenerarArchivoTxt(txtCodigo.Text, NombreArchivo, 1);
                            ObtenerInformacionEtiqueta();
                            txtValTallos.RequestFocus();
                        }
                        else
                        {
                            Toast.MakeText(Application.Context, "Código inválido", ToastLength.Short).Show(); ;
                        }
                    }
                    catch
                    {
                        txtCodigo.Text = "";
                        txtPresentacion.Text = "";
                        txtProducto.Text = "";
                        Toast.MakeText(Application.Context, "Error de cámara", ToastLength.Long).Show();
                    }
                };
            }

            lstLectura.ItemClick += LstLectura_ItemClick;
            CargarEtiquetas();
            return view;
        }

        private void LstLectura_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            codigoActualizar = e.View.FindViewById<TextView>(Resource.Id.txtCodigo).Text;
            LayoutInflater layoutInflater = LayoutInflater.From(this.Activity);
            var promptView = layoutInflater.Inflate(Resource.Layout.DialogCambioCalidad, null);

            CalidadActualizar = (EditText)promptView.FindViewById<EditText>(Resource.Id.Calidad);

            var builder = new AlertDialog.Builder(this.Activity);
            builder.SetTitle("Ingrese nueva calidad ");
            builder.SetView(promptView);

            builder.SetPositiveButton("Continuar", ActualizarOk);
            builder.SetNegativeButton("Cancelar", CancelarAction);

            builder.Show();
        }
        private void ActualizarOk(object sender, DialogClickEventArgs e)
        {
            lstlectura.Clear();
            db = new ConexionLocal(this.Activity);
            db.ActualizarCalidad(codigoActualizar, Convert.ToInt32(CalidadActualizar.Text));
            CargarEtiquetas();
        }
        private void CancelarAction(object sender, DialogClickEventArgs e)
        {
            builder.Dispose();
        }

        private void CargarEtiquetas()
        {
            lstlectura.Clear();

            db = new ConexionLocal(Application.Context);
            ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirIP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

            int SumTallos = 0;

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    SumTallos += Convert.ToInt32(cursorEtiquetas.GetString(1));
                    lstlectura.Add(new LstLecturaIP(cursorEtiquetas.GetString(0), cursorEtiquetas.GetString(1), cursorEtiquetas.GetString(2), cursorEtiquetas.GetString(3)));
                } while (cursorEtiquetas.MoveToNext());
            }
            lstLectura.Adapter = new AdLstLecturaIP(Activity, lstlectura);

            var CantEtiquetas = lstlectura.Count;

            btnIngresar.Text = "INGRESAR - " + CantEtiquetas + " - " + SumTallos + " TALLOS";
        }

        private void CargarCalidades()
        {
            SqlDataReader Calidades;
            lstCalidades.Clear();
            ConexionServidor con = new ConexionServidor();


            Calidades = con.ConsultaCalidades();

            if (Calidades.HasRows)
            {
                while (Calidades.Read())
                {
                    lstCalidades.Add(new ListBD() { ID = Calidades.GetInt32(0), Nombre = Calidades.GetString(1) });
                }
            }

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstCalidades);
        }

        private void BtnManual_Click(object sender, EventArgs e)
        {
            if (etiExist == 0)
            {
                if (txtCodigo.Text.Length == 0 || etTallosxRamo.Text.Length == 0 || etCalidad.Text.Length == 0 || etCosechador.Text.Length == 0)
                {
                    Toast.MakeText(Application.Context, "Faltan valores por ingresar", ToastLength.Short).Show();
                }
                else
                {
                    if (Convert.ToInt32(etTallosxRamo.Text) > 250)
                    {
                        etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);
                        Toast.MakeText(Application.Context, "Revise la cantidad de tallos a ingresar", ToastLength.Short).Show();
                    }
                    else
                    {
                        GuardarEtiLocal(txtCodigo.Text);
                        CargarEtiquetas();
                        LimpiarCampos();
                        txtCodigo.RequestFocus();
                    }
                }
            }
            else
            {
                txtCodigo.RequestFocus();
                txtCodigo.Text = "";
                txtPresentacion.Text = "";
                txtProducto.Text = "";
                etTallosxRamo.Text = "";
                etCosechador.Text = "";
                Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
            }
            txtCodigo.RequestFocus();
            InputMethodManager imm = (InputMethodManager)Application.Context.GetSystemService(Context.InputMethodService);
            imm.ShowSoftInput(etCosechador, ShowFlags.Forced);
            imm.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);
        }

        private void LimpiarCampos()
        {
            txtCodigo.Text = "";
            txtPresentacion.Text = "";
            txtProducto.Text = "";
            etTallosxRamo.Text = Convert.ToString(VariablesGlobales.CantTallosDef);
            etCosechador.Text = "";
        }

        private void GuardarEtiLocal(string text)
        {
            db = new ConexionLocal(Application.Context);
            String hora = DateTime.Now.ToString("hh:mm:ss");
            db.GuardarDatosEtiquetasINP(text, Convert.ToInt32(etTallosxRamo.Text), VariablesGlobales.vgIDDocumento, VariablesGlobales.vgIDBodega, Convert.ToInt32(etCosechador.Text), Convert.ToInt32(etCalidad.Text), hora);
        }

        private void ObtenerInformacionEtiqueta()
        {
            String PresentacionEt = "";
            String ProductoEt = "";
            String ValTallosEt = "";
            String var = txtCodigo.Text;
            int varIDBodega = 0;

            int boolVal = 0;
            foreach (LstLecturaIP item in this.lstlectura)
            {
                if (item.codigo == txtCodigo.Text)
                {
                    boolVal = 1;
                }
            }

            if (var != "")
            {
                if (boolVal == 0)
                {
                    int tam_var = var.Length;
                    String CodigoEtiqueta = var.Substring(0, tam_var - 1);

                    SqlDataReader Etiquetas;

                    ConexionServidor con = new ConexionServidor();
                    Etiquetas = con.ConsultaEtiquetaINP(CodigoEtiqueta, VariablesGlobales.vgIDBodega);

                    if (Etiquetas.HasRows)
                    {
                        while (Etiquetas.Read())
                        {
                            PresentacionEt = Etiquetas.GetString(0);
                            ProductoEt = Etiquetas.GetString(2);
                            ValTallosEt = Convert.ToString(Etiquetas.GetDouble(1));
                            varIDBodega = Etiquetas.GetInt32(3);
                        }

                        //validacion que no exista etiqueta

                        db = new ConexionLocal(Application.Context);
                        ICursor cursorEtiquetasExist = db.ConsultaEtiquetasExistIP(txtCodigo.Text);

                        if (cursorEtiquetasExist.MoveToFirst())
                        {
                            do
                            {
                                etiExist = cursorEtiquetasExist.GetInt(0);
                            } while (cursorEtiquetasExist.MoveToNext());
                        }
                    }
                    else
                    {
                        Toast.MakeText(Application.Context, "No esta disponible ó Etiqueta leida en otro documento", ToastLength.Short).Show();
                    }

                    if (ValTallosEt != "")
                    {
                        if (Convert.ToInt32(ValTallosEt) == 0)
                        {
                            txtCodigo.Text = "";
                            Toast.MakeText(Application.Context, "No tiene tallos disponibles para consumo", ToastLength.Short).Show();
                        }
                        else
                        {
                            txtPresentacion.Text = PresentacionEt;
                            txtProducto.Text = ProductoEt;
                            txtValTallos.Text = ValTallosEt;
                        }
                    }
                    else
                    {
                        txtCodigo.Text = "";
                    }
                }
                else
                {
                    txtCodigo.Text = "";
                    txtPresentacion.Text = "";
                    txtProducto.Text = "";
                    Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
                }
            }
        }

        private void BtnIngresar_Click(object sender, EventArgs e)
        {

            var builder = new AlertDialog.Builder(this.Activity);
            builder.SetIcon(Resource.Drawable.warning);
            builder.SetTitle("Advertencia");
            builder.SetMessage("¿Esta seguro de registar el documento?");
            builder.SetPositiveButton("Si", OkAction);
            builder.SetNegativeButton("No", (IntentSender, args) => {
                builder.Dispose();
            });
            builder.Show();
        }

        private void OkAction(object sender, DialogClickEventArgs e)
        {
            db = new ConexionLocal(Application.Context);
            ICursor cursorDatosConexion = db.ConsultaDatosConexion();
            string NombreArchivo = "ProdGen" + (string.Format("{0:MMddyyyy}", DateTime.Now) + ".txt");



            GenerarArchivoTxt("", NombreArchivo, 2);


            if (cursorDatosConexion.MoveToFirst())
            {
                do
                {
                    direccionIPLocal = cursorDatosConexion.GetString(1);
                    direccionIPPublica = cursorDatosConexion.GetString(2);
                    baseDatos = cursorDatosConexion.GetString(3);
                    usuario = cursorDatosConexion.GetString(4);
                    contrasena = cursorDatosConexion.GetString(5);

                } while (cursorDatosConexion.MoveToNext());
            }

            cadenaConexionLocal = @"data source= " + direccionIPLocal + ";initial catalog=" + baseDatos + ";user id=" + usuario + ";password= " + contrasena + ";Connect Timeout=0";
            cadenaConexionPublica = @"data source= " + direccionIPPublica + ";initial catalog=" + baseDatos + ";user id=" + usuario + ";password= " + contrasena + ";Connect Timeout=0";

            conexion = new SqlConnection(cadenaConexionLocal);

            int valConex = 0;
            try
            {
                conexion.Open();
                valConex = 1;
            }
            catch (SqlException Exp)
            {
                conexion = new SqlConnection(cadenaConexionPublica);
                try
                {
                    conexion.Open();
                    valConex = 1;
                }
                catch (SqlException ex)
                {
                    Toast.MakeText(Application.Context, "No estas conectado a internet", ToastLength.Long).Show();
                }
            }

            if (valConex == 1)
            {
                MyTaskSync tarea = new MyTaskSync(progress, conexion);
                tarea.Execute();
            }
        }

        private void GenerarArchivoTxt(string codigo, string NombreArchivo, int tipo)
        {
            // string NombreArchivo = "Prod_" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");


            string pathTxt = "/sdcard/Download/" + NombreArchivo;

            if (tipo == 1) //Crear archivo al leer sin validar
            {

                StringBuilder constructor = new StringBuilder();
                String hora = DateTime.Now.ToString("hh:mm:ss");


                String cadena = txtCodigo.Text + "," + etTallosxRamo.Text + "," + etCosechador.Text + "," + etCalidad.Text + "," + hora + "," + 0 + "," + 0 + "\r\n";

                if (!File.Exists(pathTxt))
                {
                    StreamWriter streamWriter = File.CreateText(pathTxt);
                    constructor.AppendLine(cadena);
                    streamWriter.Dispose();
                }
                else
                {
                    constructor.AppendLine(cadena);
                }
                //WriteAllText
                File.AppendAllText(pathTxt, constructor.ToString());

            }
            else //crea archivo al subir produccion
            {
                ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirIP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

                StringBuilder constructor = new StringBuilder();

                if (!File.Exists(pathTxt))
                {
                    StreamWriter streamWriter = File.CreateText(pathTxt);
                }

                if (File.Exists(pathTxt))
                {
                    if (cursorEtiquetas.MoveToFirst())
                    {
                        do
                        {
                            String a = cursorEtiquetas.GetString(0) + "," + cursorEtiquetas.GetInt(1) + "," + cursorEtiquetas.GetInt(3) + "," + cursorEtiquetas.GetInt(2) + "," + cursorEtiquetas.GetString(4) + "," + 0 + "," + 0 + "\r\n";

                            constructor.AppendLine(a);
                            File.AppendAllText(pathTxt, constructor.ToString());
                        } while (cursorEtiquetas.MoveToNext());
                    }
                }
            }
        }

        public class MyTaskSync : AsyncTask
        {
            private ConexionLocal db;
            SqlDataReader CabezaDocumento;
            SqlDataReader CabezaDocumentoPrevio;
            ConexionServidor con = new ConexionServidor();
            ProgressDialog progress;
            SqlConnection conexion;
            String tablaTemporal;
            String NombreEquipo = "";
            public MyTaskSync(ProgressDialog progress, SqlConnection conexion)
            {
                this.progress = progress;
                this.conexion = conexion;
            }

            protected override void OnPreExecute()
            {
                progress.SetMessage("Generando documento ...");
                progress.Show();
            }
            protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
            {
                db = new ConexionLocal(Application.Context);
                VerificaTablaTmpIngresosXProduccion();
                IngresaEtiquetasTablaTemporal();
                return "d";
            }

            private void VerificaTablaTmpIngresosXProduccion()
            {
                SqlDataReader tablaEtiquetas;

                ConexionServidor con = new ConexionServidor();

                String id = Secure.GetString(Application.Context.ContentResolver, Secure.AndroidId);
                int tam_var = id.Length;
                String IDLectora = id.Substring(tam_var - 2, 2);

                NombreEquipo = "Lec" + IDLectora;
                var NombreTabla = "AppMovilInventarios_Lec" + IDLectora;

                tablaEtiquetas = con.VerificaTablaTmpIngresosXProduccion(VariablesGlobales.vgNombreUsuario, NombreTabla);

                if (tablaEtiquetas.HasRows)
                {
                    while (tablaEtiquetas.Read())
                    {
                        tablaTemporal = tablaEtiquetas.GetString(0);
                    }
                }
            }

            private void IngresaEtiquetasTablaTemporal()
            {
                ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirIP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

                string sql = "INSERT INTO " + tablaTemporal + "(CodBarras,TallosXRamo,Cosechador,Calidad,Hora,Long,PresCos) VALUES (@Etiqueta,@TallosXRamo,@Cosechador,@Calidad,@Hora,@Long,@PresCos)";

                if (cursorEtiquetas.MoveToFirst())
                {
                    do
                    {
                        using (SqlCommand comando = new SqlCommand(sql, conexion))
                        {
                            if (cursorEtiquetas.GetString(0).Length == 15)
                            {
                                comando.Parameters.Add("@TallosXRamo", SqlDbType.Int).Value = cursorEtiquetas.GetInt(1);
                                comando.Parameters.Add("@Cosechador", SqlDbType.Int).Value = cursorEtiquetas.GetInt(3);
                                comando.Parameters.Add("@Calidad", SqlDbType.Int).Value = cursorEtiquetas.GetInt(2);
                                comando.Parameters.Add("@Hora", SqlDbType.VarChar).Value = cursorEtiquetas.GetString(4);
                                comando.Parameters.Add("@Long", SqlDbType.Int).Value = 0;
                                comando.Parameters.Add("@PresCos", SqlDbType.Int).Value = 0;

                                comando.CommandType = CommandType.Text;
                                comando.ExecuteNonQuery();
                                comando.Parameters.Add("@Etiqueta", SqlDbType.VarChar).Value = cursorEtiquetas.GetString(0);
                            }
                        }
                        //comando.Parameters.Add("@Etiqueta", SqlDbType.VarChar).Value = cursorEtiquetas.GetString(0);

                    } while (cursorEtiquetas.MoveToNext());
                }
                insertaDocumento();
            }

            private void insertaDocumento()
            {
                //Crear cabeza documento previo 
                DateTime fecha = DateTime.Now;
                String fecha1 = fecha.ToShortDateString();

                String Fecha2 = string.Format("{0:MM/dd/yyyy}", fecha);

                String hora = fecha.ToString("hh:mm:ss");

                int idPrevio = 0;

                CabezaDocumentoPrevio = con.GeneraDetallesEtiquetasDocFlorPrevioIP(Fecha2, hora, VariablesGlobales.vgIDCompanias, VariablesGlobales.vgIDUnidades, 1, VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDUsuario, "", 1);

                if (CabezaDocumentoPrevio.HasRows)
                {
                    while (CabezaDocumentoPrevio.Read())
                    {
                        idPrevio = CabezaDocumentoPrevio.GetInt32(0);
                    }
                }

                con.EliminaEtiquetasTemporalAfan(NombreEquipo, VariablesGlobales.vgNombreUsuario);

                con.LLenaAfanXUsuarioMaquina(VariablesGlobales.vgNombreUsuario, NombreEquipo, tablaTemporal);


                con.VerificaEtiquetasDocIngresoXProduccionFlorPrevio(1, "", NombreEquipo, VariablesGlobales.vgNombreUsuario, idPrevio);


                ////CABEZA DOCUMENTO
                CabezaDocumento = con.GeneraCabezaDocFlor(6, VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDUnidades, VariablesGlobales.vgIDCompanias, 0, 0, 0, 0, 0, 0, 0, VariablesGlobales.vgIDUsuario);


                if (CabezaDocumento.HasRows)
                {
                    while (CabezaDocumento.Read())
                    {
                        VariablesGlobales.vgIDConsumo = CabezaDocumento.GetInt32(0);
                    }
                }

                ////DETTALLES ETIQUETAS DOCUMENTO
                con.GeneraDetallesEtiquetasDocFlorIP(idPrevio, VariablesGlobales.vgIDConsumo);

                //COMPACTA DETTALLES ETIQUETAS DOCUMENTO             
                String FechaRotacion = string.Format("{0:MM/dd/yyyy}", fecha);
                con.CompactaDocIngProdFlorDetalles(VariablesGlobales.vgIDConsumo, VariablesGlobales.vgIDUnidades, FechaRotacion);

                con.EliminaEtiquetasTemporalAfan(NombreEquipo, VariablesGlobales.vgNombreUsuario);
            }

            private void LimpiarTablaEtiquetas()
            {
                db = new ConexionLocal(Application.Context);
                db.EliminarDatosEtiquetasIP(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            }
            protected override void OnPostExecute(Java.Lang.Object result)
            {
                Toast.MakeText(Application.Context, "Documento Registrado", ToastLength.Long).Show();
                progress.Dismiss();
                LimpiarTablaEtiquetas();
            }
        }
    }
}