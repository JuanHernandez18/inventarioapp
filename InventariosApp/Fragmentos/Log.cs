﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using InventariosApp.Adapter;
using InventariosApp.Clases;

namespace InventariosApp.Fragmentos
{
    public class Log : Fragment
    {
        ListView lvLog;
        List<LstLog> lstLog = new List<LstLog>();
        private ConexionLocal db;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
           View view = inflater.Inflate(Resource.Layout.FgLog, container, false);
           lvLog = view.FindViewById<ListView>(Resource.Id.lstLog);

            CargaLog();
            return view;
        }

        private void CargaLog()
        {
            lstLog.Clear();
            db = new ConexionLocal(Application.Context);
            ICursor cursorSincronizar = db.ConsultaLog();

            if (cursorSincronizar.MoveToFirst())
            {
                do
                {
                    lstLog.Add(new LstLog(cursorSincronizar.GetString(2), cursorSincronizar.GetString(1)));
                } while (cursorSincronizar.MoveToNext());
            }

            lvLog.Adapter = new AdLstLog(Activity, lstLog);
        }
    }
}