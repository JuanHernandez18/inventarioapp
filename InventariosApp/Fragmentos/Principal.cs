﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using InventariosApp.Clases;

namespace InventariosApp.Fragmentos
{
    public class Principal : Fragment
    {
        List<ListBD> lstCompania = new List<ListBD>();
        List<ListBD> lstUnidades = new List<ListBD>();
        List<ListBD> lstBodega = new List<ListBD>();
        List<ListBD> lstCentroC = new List<ListBD>();
        List<ListBD> lstDocumentos = new List<ListBD>();
        List<ListBD> lstCamiones = new List<ListBD>();
        List<ListBD> lstConductores = new List<ListBD>();
        List<ListBD> lstMotivo = new List<ListBD>();
        List<ListBD> lstDestino = new List<ListBD>();
        List<ListBD> lstBodegaDest = new List<ListBD>();
        List<ListBD> lstCodLote = new List<ListBD>();
        List<ListBD> lstRemision = new List<ListBD>();

        LinearLayout lyCentros;
        LinearLayout lyCamion;
        LinearLayout lyConductor;
        LinearLayout lyMotivo;
        LinearLayout lyDestino;
        LinearLayout lyBodegaDest;
        LinearLayout lyCentros1;
        LinearLayout lyCamion1;
        LinearLayout lyConductor1;
        LinearLayout lyMotivo1;
        LinearLayout lyDestino1;
        LinearLayout lyBodegaDest1;
        LinearLayout lyCodLote;
        LinearLayout lyCodLote1;
        LinearLayout lyPrecinto;
        LinearLayout lyPrecinto1;
        LinearLayout lyRemision;
        LinearLayout lyRemision1;


        ArrayAdapter adapter;
        Spinner spinnerCompania;
        Spinner spinnerUnidades;
        Spinner spinnerCentroC;
        Spinner spinnerBodega;
        Spinner spinnerDocumento;
        Spinner spinnerCamion;
        Spinner spinnerConductor;
        Spinner spinnerMotivo;
        Spinner spinnerDestino;
        Spinner spinnerBodegaDest;
        Spinner spinnerCodLote;
        EditText EdPrecinto;
        Spinner spinnerRemision;

        private MediaPlayer player;

        Button btnIngresarConsumo;
        EditText tallos;

        private Fragment mCurrentFragment;
        private Lector lectorFragment;
        private LectorIngresosProduccion lectorIngresosProduccionFragment;
        private LectorConteoTotal LectorConteoTotalFragment;
        private Pruebas PruebasFragment;
        private LectorTraslados lectorTrasladosFragment;
        private LectorTrasladosConfirmacion lectorTrasladosConfirmacionFragment;
        private LectorConsumoParcial lectorConsumoParcialFragment;

        ConexionLocal db;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.FgPrincipal, container, false);

            lyCentros = view.FindViewById<LinearLayout>(Resource.Id.lyCentros);
            lyCamion = view.FindViewById<LinearLayout>(Resource.Id.lyCamion);
            lyConductor = view.FindViewById<LinearLayout>(Resource.Id.lyConductor);
            lyMotivo = view.FindViewById<LinearLayout>(Resource.Id.lyMotivo);
            lyDestino = view.FindViewById<LinearLayout>(Resource.Id.lyDestino);
            lyBodegaDest = view.FindViewById<LinearLayout>(Resource.Id.lyBodegaDest);
            lyRemision = view.FindViewById<LinearLayout>(Resource.Id.lyRemision);

            lyCentros1 = view.FindViewById<LinearLayout>(Resource.Id.lyCentros1);
            lyCamion1 = view.FindViewById<LinearLayout>(Resource.Id.lyCamion1);
            lyConductor1 = view.FindViewById<LinearLayout>(Resource.Id.lyConductor1);
            lyMotivo1 = view.FindViewById<LinearLayout>(Resource.Id.lyMotivo1);
            lyDestino1 = view.FindViewById<LinearLayout>(Resource.Id.lyDestino1);
            lyBodegaDest1 = view.FindViewById<LinearLayout>(Resource.Id.lyBodegaDest1);
            lyCodLote = view.FindViewById<LinearLayout>(Resource.Id.lyCodLote);
            lyCodLote1 = view.FindViewById<LinearLayout>(Resource.Id.lyCodLote1);
            lyPrecinto = view.FindViewById<LinearLayout>(Resource.Id.lyPrecinto);
            lyPrecinto1 = view.FindViewById<LinearLayout>(Resource.Id.lyPrecinto1);
            lyRemision = view.FindViewById<LinearLayout>(Resource.Id.lyRemision);
            lyRemision1 = view.FindViewById<LinearLayout>(Resource.Id.lyRemision1);


            spinnerCompania = view.FindViewById<Spinner>(Resource.Id.idCompania);
            spinnerUnidades = view.FindViewById<Spinner>(Resource.Id.idUnidad);
            spinnerCentroC = view.FindViewById<Spinner>(Resource.Id.IDCentroC);
            spinnerBodega = view.FindViewById<Spinner>(Resource.Id.IDBodega);
            spinnerDocumento = view.FindViewById<Spinner>(Resource.Id.IDDocumento);
            spinnerCamion = view.FindViewById<Spinner>(Resource.Id.IDCamion);
            spinnerConductor = view.FindViewById<Spinner>(Resource.Id.IDConductor);
            spinnerMotivo = view.FindViewById<Spinner>(Resource.Id.IDMotivo);
            spinnerDestino = view.FindViewById<Spinner>(Resource.Id.IDDestino);
            spinnerBodegaDest = view.FindViewById<Spinner>(Resource.Id.IDBodegaDest);
            spinnerCodLote = view.FindViewById<Spinner>(Resource.Id.IDCodLote);
            EdPrecinto = view.FindViewById<EditText>(Resource.Id.EdPrecinto);
            spinnerRemision = view.FindViewById<Spinner>(Resource.Id.IDRemision);

            btnIngresarConsumo = view.FindViewById<Button>(Resource.Id.btnIngresarConsumo);

            lectorFragment = new Lector();
            lectorConsumoParcialFragment = new LectorConsumoParcial();
            lectorIngresosProduccionFragment = new LectorIngresosProduccion();
            LectorConteoTotalFragment = new LectorConteoTotal();
            //PruebasFragment = new Pruebas();


            lectorTrasladosFragment = new LectorTraslados();
            lectorTrasladosConfirmacionFragment = new LectorTrasladosConfirmacion();

            btnIngresarConsumo.Click += btnIngresarConsumoClick;

            validacionInicial();

            return view;
        }

        private void validacionInicial()
        {
            db = new ConexionLocal(Application.Context);
            ICursor cursorDatosConexion = db.ConsultaDatosConexion();


            string baseDatos = "";
            if (cursorDatosConexion.MoveToFirst())
            {
                do
                {
                    baseDatos = cursorDatosConexion.GetString(3);
                    VariablesGlobales.vgFlagExterno = cursorDatosConexion.GetInt(6);
                } while (cursorDatosConexion.MoveToNext());
            }

            if (baseDatos != "")
            {
                CargarSpinnerCompanias();
                CargarSpinnerUnidades();
                CargarSpinnerBodegas();
                CargarSpinnerDocumentos();
                CargarSpinnerCentroC();
                CargarSpinnerCamiones();
                CargarSpinnerConductor();
                CargarSpinnerMotivo();
                CargarSpinnerDestino();
                CargarSpinnerBodegaDest();
                CargarSpinnerCodLote();
            }
        }

        //*********************************************************************************************************************//
        //                                               CARGA DE SPINNER
        //**********************************************************************************************************************//
        private void CargarSpinnerDocumentos()
        {
            lstDocumentos.Clear();

            lstDocumentos.Add(new ListBD() { ID = 1, Nombre = "BAJAS" });
            lstDocumentos.Add(new ListBD() { ID = 2, Nombre = "CONSUMOS" });
            lstDocumentos.Add(new ListBD() { ID = 3, Nombre = "CONTEO TOTAL" });
            lstDocumentos.Add(new ListBD() { ID = 4, Nombre = "RE-INGRESOS" });
            lstDocumentos.Add(new ListBD() { ID = 5, Nombre = "TRASLADOS" });
            lstDocumentos.Add(new ListBD() { ID = 6, Nombre = "INGRESOS PRODUCCIÓN" }); //6 INGRESOS PRODUCCION
            lstDocumentos.Add(new ListBD() { ID = 7, Nombre = "CONFIRMACIÓN REMISIÓN" });
            lstDocumentos.Add(new ListBD() { ID = 8, Nombre = "CONTEO PARCIAL" });
            lstDocumentos.Add(new ListBD() { ID = 9, Nombre = "CONSUMO PARCIAL" });
            // lstDocumentos.Add(new ListBD() { ID = 6, Nombre = "TRANSFORMACIONES" });

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstDocumentos);
            spinnerDocumento.Adapter = adapter;
            spinnerDocumento.SetSelection(1);
            spinnerDocumento.ItemSelected += ItemDocumentpsSelected;
        }

        private void CargarSpinnerCentroC()
        {
            lstCentroC.Clear();
            //SqlDataReader CentroC;            
            //ConexionServidor con = new ConexionServidor();

            //db = new ConexionLocal(Application.Context);
            //ICursor cursorDatosConexion = db.ConsultaDatosConexion();

            //CentroC = con.ConsultaCentrosC(VariablesGlobales.vgIDCompanias);

            //if (CentroC.HasRows)
            //{
            //    while (CentroC.Read())
            //    {
            //        lstCentroC.Add(new ListBD() { ID = CentroC.GetInt32(0), Nombre = CentroC.GetString(1) });
            //    }
            //}

            db = new ConexionLocal(Application.Context);
            ICursor CentroC = db.ConsultaCentroC(VariablesGlobales.vgIDCompanias);
            if (CentroC.MoveToFirst())
            {
                do
                {
                    lstCentroC.Add(new ListBD() { ID = CentroC.GetInt(0), Nombre = CentroC.GetString(1) });
                } while (CentroC.MoveToNext());
            }
            //else
            //{
            //    Toast.MakeText(Application.Context, "No se pudo cargar los Centros de Costo", ToastLength.Long).Show();
            //}

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstCentroC);

            spinnerCentroC.Adapter = adapter;
            spinnerCentroC.SetSelection(11);
            spinnerCentroC.ItemSelected += ItemCentroCSelected;
        }

        private void CargarSpinnerBodegas()
        {
           
            lstBodega.Clear();

            // SqlDataReader Bodegas;
            //ConexionServidor con = new ConexionServidor();
            //Bodegas = con.consultaBodegas(VariablesGlobales.vgIDCompanias, VariablesGlobales.vgIDUnidades);

            //if (Bodegas.HasRows)
            //{
            //    while (Bodegas.Read())
            //    {
            //        lstBodega.Add(new ListBD() { ID = Bodegas.GetInt32(0), Nombre = Bodegas.GetString(1) });
            //    }
            //}
            db = new ConexionLocal(Application.Context);
            ICursor Bodegas = db.ConsultaBodegas(VariablesGlobales.vgIDCompanias, VariablesGlobales.vgIDUnidades);
            if (Bodegas.MoveToFirst())
            {
                do
                {
                    lstBodega.Add(new ListBD() { ID = Bodegas.GetInt(0), Nombre = Bodegas.GetString(1) });
                } while (Bodegas.MoveToNext());
            }
            //else
            //{
            //    Toast.MakeText(Application.Context, "No se pudo cargar las Bodegas", ToastLength.Long).Show();
            //}

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstBodega);

            spinnerBodega.Adapter = adapter;
            spinnerBodega.ItemSelected += ItemBodegasSelected;
        }

        private void CargarSpinnerCompanias()
        {
            //SqlDataReader Companias;
            lstCompania.Clear();
            ConexionServidor con = new ConexionServidor();

            db = new ConexionLocal(Application.Context);

            ICursor cursorDatosConexion = db.ConsultaDatosConexion();
            if (cursorDatosConexion.Count == 0)
            {
                Toast.MakeText(Application.Context, "Ingresar parámetros generales de la app", ToastLength.Long).Show();
            }
            else
            {
                //Companias = con.ConsultaCompanias(VariablesGlobales.vgIDUsuario);
                ICursor Companias = db.ConsultaCompania(VariablesGlobales.vgIDUsuario);
                if (Companias.MoveToFirst())
                {
                    do
                    {
                        lstCompania.Add(new ListBD() { ID = Companias.GetInt(0), Nombre = Companias.GetString(1) });
                    } while (Companias.MoveToNext());
                }
                //else
                //{
                //    Toast.MakeText(Application.Context, "No se pudo cargar compañias", ToastLength.Long).Show();
                //}
                //if (Companias.HasRows)
                //{
                //    while (Companias.Read())
                //    {
                //        lstCompania.Add(new ListBD() { ID = Companias.GetInt32(0), Nombre = Companias.GetString(1) });
                //    }
                //}

                adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstCompania);

                spinnerCompania.Adapter = adapter;
                //     spinnerCompania.SetSelection(1);

                CargarSpinnerUnidades();
                CargarSpinnerCentroC();
                spinnerCompania.ItemSelected += ItemCompaniaSelected;
            }
        }

        private void CargarSpinnerUnidades()
        {
            lstUnidades.Clear();
            //SqlDataReader Unidades;
            //ConexionServidor con = new ConexionServidor();

            //Unidades = con.ConsultaUnidades(VariablesGlobales.vgIDCompanias, VariablesGlobales.vgIDUsuario);

            //if (Unidades.HasRows)
            //{
            //    while (Unidades.Read())
            //    {
            //        lstUnidades.Add(new ListBD() { ID = Unidades.GetInt32(0), Nombre = Unidades.GetString(1) });
            //    }
            //}


            db = new ConexionLocal(Application.Context);            
            ICursor UnidadesNegocio = db.ConsultaUnidadesNegocio(VariablesGlobales.vgIDCompanias, VariablesGlobales.vgIDUsuario);
            if (UnidadesNegocio.MoveToFirst())
            {
                do
                {
                    lstUnidades.Add(new ListBD() { ID = UnidadesNegocio.GetInt(0), Nombre = UnidadesNegocio.GetString(1) });
                } while (UnidadesNegocio.MoveToNext());
            }
            //else
            //{
            //    Toast.MakeText(Application.Context, "No se pudo cargar UnidadesNegocio", ToastLength.Long).Show();
            //}

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstUnidades);

            spinnerUnidades.Adapter = adapter;

            //  spinnerUnidades.SetSelection(10);
            CargarSpinnerBodegas();
            spinnerUnidades.ItemSelected += ItemUnidadesSelected;
        }

        private void CargarSpinnerCamiones()
        {
            lstCamiones.Clear();

            //SqlDataReader Camiones;         
            //ConexionServidor con = new ConexionServidor();
            //Camiones = con.consultaCamiones();

            //if (Camiones.HasRows)
            //{
            //    while (Camiones.Read())
            //    {
            //        lstCamiones.Add(new ListBD() { ID = Camiones.GetInt32(0), Nombre = Camiones.GetString(1) });
            //    }
            //}
            db = new ConexionLocal(Application.Context);
            ICursor Camiones = db.ConsultaCamiones();
            if (Camiones.MoveToFirst())
            {
                do
                {
                    lstCamiones.Add(new ListBD() { ID = Camiones.GetInt(0), Nombre = Camiones.GetString(1) });
                } while (Camiones.MoveToNext());
            }
            //else
            //{
            //    Toast.MakeText(Application.Context, "No se pudo cargar los camiones", ToastLength.Long).Show();
            //}

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstCamiones);

            spinnerCamion.Adapter = adapter;
            spinnerCamion.ItemSelected += ItemCamionSelected;
        }

        private void CargarSpinnerConductor()
        {
            lstConductores.Clear();
            //SqlDataReader Conductores;            

            //ConexionServidor con = new ConexionServidor();
            //Conductores = con.consultaConductor();

            //if (Conductores.HasRows)
            //{
            //    while (Conductores.Read())
            //    {
            //        lstConductores.Add(new ListBD() { ID = Conductores.GetInt32(0), Nombre = Conductores.GetString(1) });
            //    }
            //}
            db = new ConexionLocal(Application.Context);
            ICursor Conductores = db.ConsultaConductores();
            if (Conductores.MoveToFirst())
            {
                do
                {
                    lstConductores.Add(new ListBD() { ID = Conductores.GetInt(0), Nombre = Conductores.GetString(1) });
                } while (Conductores.MoveToNext());
            }
            //else
            //{
            //    Toast.MakeText(Application.Context, "No se pudo cargar los Conductores", ToastLength.Long).Show();
            //}

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstConductores);

            spinnerConductor.Adapter = adapter;
            spinnerConductor.ItemSelected += ItemConductorSelected;
        }

        private void CargarSpinnerMotivo()
        {
            lstMotivo.Clear();

            //SqlDataReader Motivos;           

            //ConexionServidor con = new ConexionServidor();
            //Motivos = con.consultaMotivo();

            //if (Motivos.HasRows)
            //{
            //    while (Motivos.Read())
            //    {
            //        lstMotivo.Add(new ListBD() { ID = Motivos.GetInt32(0), Nombre = Motivos.GetString(1) });
            //    }
            //}

            db = new ConexionLocal(Application.Context);
            ICursor Motivos = db.ConsultaMotivos();
            if (Motivos.MoveToFirst())
            {
                do
                {
                    lstMotivo.Add(new ListBD() { ID = Motivos.GetInt(0), Nombre = Motivos.GetString(1) });
                } while (Motivos.MoveToNext());
            }

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstMotivo);
            spinnerMotivo.Adapter = adapter;
            spinnerMotivo.ItemSelected += ItemMotivoSelected;
        }

        private void CargarSpinnerDestino()
        {

            lstDestino.Clear();
            //SqlDataReader Destinos;           
            //ConexionServidor con = new ConexionServidor();
            //Destinos = con.consultaDestino();

            //if (Destinos.HasRows)
            //{
            //    while (Destinos.Read())
            //    {
            //        lstDestino.Add(new ListBD() { ID = Destinos.GetInt32(0), Nombre = Destinos.GetString(1) });
            //    }
            //}
            db = new ConexionLocal(Application.Context);
            ICursor Destinos = db.ConsultaDestinos();
            if (Destinos.MoveToFirst())
            {
                do
                {
                    lstDestino.Add(new ListBD() { ID = Destinos.GetInt(0), Nombre = Destinos.GetString(1) });
                } while (Destinos.MoveToNext());
            }
            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstDestino);

            spinnerDestino.Adapter = adapter;
            spinnerDestino.ItemSelected += ItemDestinoSelected;
        }

        private void CargarSpinnerCodLote()
        {
            lstCodLote.Clear();

            //SqlDataReader CodLote;            
            //ConexionServidor con = new ConexionServidor();
            //CodLote = con.ConsultaCodLote();

            //if (CodLote.HasRows)
            //{
            //    while (CodLote.Read())
            //    {
            //        lstCodLote.Add(new ListBD() { ID = CodLote.GetInt32(0), Nombre = CodLote.GetString(1) });
            //    }
            //}

            db = new ConexionLocal(Application.Context);
            ICursor CodLote = db.ConsultaCodLote();
            if (CodLote.MoveToFirst())
            {
                do
                {
                    lstCodLote.Add(new ListBD() { ID = CodLote.GetInt(0), Nombre = CodLote.GetString(1) });
                } while (CodLote.MoveToNext());
            }

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstCodLote);

            spinnerCodLote.Adapter = adapter;
            spinnerCodLote.ItemSelected += ItemCodLoteSelected; ;
        }


        private void CargarSpinnerBodegaDest()
        {
            lstBodegaDest.Clear();
            //SqlDataReader BodegaDest;            

            //ConexionServidor con = new ConexionServidor();
            //BodegaDest = con.consultaBodegaDestino();

            //if (BodegaDest.HasRows)
            //{
            //    while (BodegaDest.Read())
            //    {
            //        lstBodegaDest.Add(new ListBD() { ID = BodegaDest.GetInt32(0), Nombre = BodegaDest.GetString(2) });
            //    }
            //}

            db = new ConexionLocal(Application.Context);
            ICursor BodegaDest = db.ConsultaBodegaDest();
            if (BodegaDest.MoveToFirst())
            {
                do
                {
                    lstBodegaDest.Add(new ListBD() { ID = BodegaDest.GetInt(0), Nombre = BodegaDest.GetString(1) });
                } while (BodegaDest.MoveToNext());
            }

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstBodegaDest);

            spinnerBodegaDest.Adapter = adapter;
            spinnerBodegaDest.ItemSelected += ItemBodegaDestSelected;
        }

        private void CargarSpinnerRemision(int IDBodega)
        {
            SqlDataReader Remision;
            lstRemision.Clear();
            //int IDBodega = 303;
            ConexionServidor con = new ConexionServidor();
            Remision = con.consultaRemision(IDBodega);
            //Remision
            if (Remision.HasRows)
            {
                while (Remision.Read())
                {
                    lstRemision.Add(new ListBD() { ID = Remision.GetInt32(0), Nombre = Remision.GetString(4) });
                }
            }

            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstRemision);

            spinnerRemision.Adapter = adapter;
            spinnerRemision.ItemSelected += ItemRemisionSelected;
        }

        //*********************************************************************************************************************//
        //                                               EVENTO SPINNER
        //**********************************************************************************************************************/
        private void ItemCompaniaSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstCompania[e.Position];
            VariablesGlobales.vgIDCompanias = l.ID;

            CargarSpinnerUnidades();
            CargarSpinnerCentroC();
        }

        private void ItemUnidadesSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstUnidades[e.Position];

            VariablesGlobales.vgIDUnidades = l.ID;
            CargarSpinnerBodegas();
        }

        private void ItemBodegasSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstBodega[e.Position];
            VariablesGlobales.vgIDBodega = l.ID;
        }

        private void ItemCodLoteSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstCodLote[e.Position];
            VariablesGlobales.vgIDCodLote = l.ID;
            VariablesGlobales.vgNombreCodLote = l.Nombre;
        }


        private void ItemDocumentpsSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstDocumentos[e.Position];
            VariablesGlobales.vgIDDocumento = l.ID;

            if (VariablesGlobales.vgIDDocumento == 1) //bajas
            {
                lyCentros.Visibility = ViewStates.Visible;
                lyCamion.Visibility = ViewStates.Gone;
                lyConductor.Visibility = ViewStates.Gone;
                lyMotivo.Visibility = ViewStates.Visible;
                lyDestino.Visibility = ViewStates.Visible;
                lyBodegaDest.Visibility = ViewStates.Gone;
                lyCentros1.Visibility = ViewStates.Visible;
                lyCamion1.Visibility = ViewStates.Gone;
                lyConductor1.Visibility = ViewStates.Gone;
                lyMotivo1.Visibility = ViewStates.Visible;
                lyDestino1.Visibility = ViewStates.Visible;
                lyBodegaDest1.Visibility = ViewStates.Gone;
                lyCodLote.Visibility = ViewStates.Gone;
                lyCodLote1.Visibility = ViewStates.Gone;
                lyPrecinto.Visibility = ViewStates.Gone;
                lyPrecinto1.Visibility = ViewStates.Gone;
                lyRemision.Visibility = ViewStates.Gone;
                lyRemision1.Visibility = ViewStates.Gone;
            }

            if (VariablesGlobales.vgIDDocumento == 3) //conteo total
            {
                lyCentros.Visibility = ViewStates.Gone;
                lyCamion.Visibility = ViewStates.Gone;
                lyConductor.Visibility = ViewStates.Gone;
                lyMotivo.Visibility = ViewStates.Gone;
                lyDestino.Visibility = ViewStates.Gone;
                lyBodegaDest.Visibility = ViewStates.Gone;
                lyCentros1.Visibility = ViewStates.Gone;
                lyCamion1.Visibility = ViewStates.Gone;
                lyConductor1.Visibility = ViewStates.Gone;
                lyMotivo1.Visibility = ViewStates.Gone;
                lyDestino1.Visibility = ViewStates.Gone;
                lyBodegaDest1.Visibility = ViewStates.Gone;
                lyCodLote.Visibility = ViewStates.Gone;
                lyCodLote1.Visibility = ViewStates.Gone;
                lyPrecinto.Visibility = ViewStates.Gone;
                lyPrecinto1.Visibility = ViewStates.Gone;
                lyRemision.Visibility = ViewStates.Gone;
                lyRemision1.Visibility = ViewStates.Gone;
            }
            if (VariablesGlobales.vgIDDocumento == 8) //conteo parcial
            {
                lyCentros.Visibility = ViewStates.Gone;
                lyCamion.Visibility = ViewStates.Gone;
                lyConductor.Visibility = ViewStates.Gone;
                lyMotivo.Visibility = ViewStates.Gone;
                lyDestino.Visibility = ViewStates.Gone;
                lyBodegaDest.Visibility = ViewStates.Gone;
                lyCentros1.Visibility = ViewStates.Gone;
                lyCamion1.Visibility = ViewStates.Gone;
                lyConductor1.Visibility = ViewStates.Gone;
                lyMotivo1.Visibility = ViewStates.Gone;
                lyDestino1.Visibility = ViewStates.Gone;
                lyBodegaDest1.Visibility = ViewStates.Gone;
                lyCodLote.Visibility = ViewStates.Gone;
                lyCodLote1.Visibility = ViewStates.Gone;
                lyPrecinto.Visibility = ViewStates.Gone;
                lyPrecinto1.Visibility = ViewStates.Gone;
                lyRemision.Visibility = ViewStates.Gone;
                lyRemision1.Visibility = ViewStates.Gone;
            }

            if (VariablesGlobales.vgIDDocumento == 4) //reingreso
            {
                lyCentros.Visibility = ViewStates.Gone;
                lyCamion.Visibility = ViewStates.Gone;
                lyConductor.Visibility = ViewStates.Gone;
                lyMotivo.Visibility = ViewStates.Gone;
                lyDestino.Visibility = ViewStates.Gone;
                lyBodegaDest.Visibility = ViewStates.Gone;
                lyCentros1.Visibility = ViewStates.Gone;
                lyCamion1.Visibility = ViewStates.Gone;
                lyConductor1.Visibility = ViewStates.Gone;
                lyMotivo1.Visibility = ViewStates.Gone;
                lyDestino1.Visibility = ViewStates.Gone;
                lyBodegaDest1.Visibility = ViewStates.Gone;
                lyCodLote.Visibility = ViewStates.Gone;
                lyCodLote1.Visibility = ViewStates.Gone;
                lyPrecinto.Visibility = ViewStates.Gone;
                lyPrecinto1.Visibility = ViewStates.Gone;
                lyRemision.Visibility = ViewStates.Gone;
                lyRemision1.Visibility = ViewStates.Gone;
            }

            if (VariablesGlobales.vgIDDocumento == 2) //Consumos
            {
                lyBodegaDest.Visibility = ViewStates.Gone;
                lyCentros.Visibility = ViewStates.Visible;
                lyCamion.Visibility = ViewStates.Gone;
                lyConductor.Visibility = ViewStates.Gone;
                lyMotivo.Visibility = ViewStates.Gone;
                lyDestino.Visibility = ViewStates.Gone;
                lyBodegaDest1.Visibility = ViewStates.Gone;
                lyCentros1.Visibility = ViewStates.Visible;
                lyCamion1.Visibility = ViewStates.Gone;
                lyConductor1.Visibility = ViewStates.Gone;
                lyMotivo1.Visibility = ViewStates.Gone;
                lyDestino1.Visibility = ViewStates.Gone;
                lyCodLote.Visibility = ViewStates.Visible;
                lyCodLote1.Visibility = ViewStates.Visible;
                lyPrecinto.Visibility = ViewStates.Gone;
                lyPrecinto1.Visibility = ViewStates.Gone;
                lyRemision.Visibility = ViewStates.Gone;
                lyRemision1.Visibility = ViewStates.Gone;
            }

            if (VariablesGlobales.vgIDDocumento == 5) //Traslados
            {
                lyCentros.Visibility = ViewStates.Gone;
                lyCamion.Visibility = ViewStates.Visible;
                lyConductor.Visibility = ViewStates.Visible;
                lyMotivo.Visibility = ViewStates.Gone;
                lyDestino.Visibility = ViewStates.Gone;
                lyBodegaDest.Visibility = ViewStates.Visible;
                lyCentros1.Visibility = ViewStates.Gone;
                lyCamion1.Visibility = ViewStates.Visible;
                lyConductor1.Visibility = ViewStates.Visible;
                lyMotivo1.Visibility = ViewStates.Gone;
                lyDestino1.Visibility = ViewStates.Gone;
                lyBodegaDest1.Visibility = ViewStates.Visible;
                lyCodLote.Visibility = ViewStates.Gone;
                lyCodLote1.Visibility = ViewStates.Gone;
                lyPrecinto.Visibility = ViewStates.Visible;
                lyPrecinto1.Visibility = ViewStates.Visible;
                lyRemision.Visibility = ViewStates.Gone;
                lyRemision1.Visibility = ViewStates.Gone;
            }

            if (VariablesGlobales.vgIDDocumento == 6) //Ingresos x producción
            {
                lyCentros.Visibility = ViewStates.Gone;
                lyCamion.Visibility = ViewStates.Gone;
                lyConductor.Visibility = ViewStates.Gone;
                lyMotivo.Visibility = ViewStates.Gone;
                lyDestino.Visibility = ViewStates.Gone;
                lyBodegaDest.Visibility = ViewStates.Gone;
                lyCentros1.Visibility = ViewStates.Gone;
                lyCamion1.Visibility = ViewStates.Gone;
                lyConductor1.Visibility = ViewStates.Gone;
                lyMotivo1.Visibility = ViewStates.Gone;
                lyDestino1.Visibility = ViewStates.Gone;
                lyBodegaDest1.Visibility = ViewStates.Gone;
                lyCodLote.Visibility = ViewStates.Gone;
                lyCodLote1.Visibility = ViewStates.Gone;
                lyPrecinto.Visibility = ViewStates.Gone;
                lyPrecinto1.Visibility = ViewStates.Gone;
                lyRemision.Visibility = ViewStates.Gone;
                lyRemision1.Visibility = ViewStates.Gone;
            }
            if (VariablesGlobales.vgIDDocumento == 7) //Traslados
            {
                lyCentros.Visibility = ViewStates.Gone;
                lyCamion.Visibility = ViewStates.Gone;
                lyConductor.Visibility = ViewStates.Gone;
                lyMotivo.Visibility = ViewStates.Gone;
                lyDestino.Visibility = ViewStates.Gone;
                lyBodegaDest.Visibility = ViewStates.Gone;
                lyCentros1.Visibility = ViewStates.Gone;
                lyCamion1.Visibility = ViewStates.Gone;
                lyConductor1.Visibility = ViewStates.Gone;
                lyMotivo1.Visibility = ViewStates.Gone;
                lyDestino1.Visibility = ViewStates.Gone;
                lyBodegaDest1.Visibility = ViewStates.Gone;
                lyCodLote.Visibility = ViewStates.Gone;
                lyCodLote1.Visibility = ViewStates.Gone;
                lyPrecinto.Visibility = ViewStates.Gone;
                lyPrecinto1.Visibility = ViewStates.Gone;
                lyRemision.Visibility = ViewStates.Visible;
                lyRemision1.Visibility = ViewStates.Visible;

                CargarSpinnerRemision(VariablesGlobales.vgIDBodega);
                
            }
            if (VariablesGlobales.vgIDDocumento == 9) //Pruebas
            {
                lyBodegaDest.Visibility = ViewStates.Gone;
                lyCentros.Visibility = ViewStates.Visible;
                lyCamion.Visibility = ViewStates.Gone;
                lyConductor.Visibility = ViewStates.Gone;
                lyMotivo.Visibility = ViewStates.Gone;
                lyDestino.Visibility = ViewStates.Gone;
                lyBodegaDest1.Visibility = ViewStates.Gone;
                lyCentros1.Visibility = ViewStates.Visible;
                lyCamion1.Visibility = ViewStates.Gone;
                lyConductor1.Visibility = ViewStates.Gone;
                lyMotivo1.Visibility = ViewStates.Gone;
                lyDestino1.Visibility = ViewStates.Gone;
                lyCodLote.Visibility = ViewStates.Visible;
                lyCodLote1.Visibility = ViewStates.Visible;
                lyPrecinto.Visibility = ViewStates.Gone;
                lyPrecinto1.Visibility = ViewStates.Gone;
                lyRemision.Visibility = ViewStates.Gone;
                lyRemision1.Visibility = ViewStates.Gone;

            }
        }
        private void ItemCentroCSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstCentroC[e.Position];
            VariablesGlobales.vgIDCentroC = l.ID;
        }
        private void ItemBodegaDestSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstBodegaDest[e.Position];
            VariablesGlobales.vgIDBodegaDest = l.ID;
            if (VariablesGlobales.vgIDBodega == VariablesGlobales.vgIDBodegaDest) //Ingresos x producción
            {
                Toast.MakeText(Application.Context, "La bodega de Destino no puede ser igual a la de Origen", ToastLength.Long).Show();
                return;
            }

        }
        private void ItemRemisionSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstRemision[e.Position];
            VariablesGlobales.vgIDRemision = l.ID;
        }
        private void ItemCamionSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstCamiones[e.Position];
            VariablesGlobales.vgIDCamion = l.ID;
        }
        private void ItemMotivoSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstMotivo[e.Position];
            VariablesGlobales.vgIDMotivo = l.ID;
        }
        private void ItemDestinoSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstDestino[e.Position];
            VariablesGlobales.vgIDDestino = l.ID;

        }
        private void ItemConductorSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstConductores[e.Position];
            VariablesGlobales.vgIDConductor = l.ID;
        }
        private void ItemConsumoSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstRemision[e.Position];
            VariablesGlobales.vgIDConsumo = l.ID;
        }

        //*********************************************************************************************************************//
        //                                               EVENTO BOTON
        //**********************************************************************************************************************/

        private void btnIngresarConsumoClick(object sender, EventArgs e)
        {
            switch (VariablesGlobales.vgIDDocumento)
            {
                case 1:
                    Console.WriteLine("Case 1");
                    break;
                case 2:
                    ReplaceFragment(lectorFragment);
                    //Console.WriteLine("Case 2");
                    break;
                case 3:
                   ReplaceFragment(LectorConteoTotalFragment);                    
                    break;
                case 4:
                    Console.WriteLine("Case 2");
                    break;
                case 5:
                    if (VariablesGlobales.vgIDBodega == VariablesGlobales.vgIDBodegaDest) //Ingresos x producción
                    {
                        Toast.MakeText(Application.Context, "La bodega de Destino no puede ser igual a la de Origen", ToastLength.Long).Show();
                        return;
                    }

                    if (EdPrecinto.Text.Length == 0)
                    {
                        VariablesGlobales.vgPrecinto = "0";
                    }
                    else
                    {
                        VariablesGlobales.vgPrecinto = EdPrecinto.Text;
                    }
                    ReplaceFragment(lectorTrasladosFragment);
                    break;
                case 6:
                    LayoutInflater layoutInflater = LayoutInflater.From(this.Activity);
                    var promptView = layoutInflater.Inflate(Resource.Layout.DialogParametros, null);


                    tallos = (EditText)promptView.FindViewById<EditText>(Resource.Id.CanTallos);


                    var builder = new AlertDialog.Builder(this.Activity);
                    builder.SetTitle("Parametrización");
                    builder.SetView(promptView);

                    builder.SetPositiveButton("Continuar", ContinuarAction);
                    //builder.SetNegativeButton("No", (IntentSender, args) => {
                    //    builder.Dispose();
                    //});
                    builder.Show();
                    break;
                case 7:
                    if (VariablesGlobales.vgIDRemision == 0) //Ingresos x producción
                    {
                        Toast.MakeText(Application.Context, "No selecciono ninguna remisión", ToastLength.Long).Show();
                        return;
                    }
                    ReplaceFragment(lectorTrasladosConfirmacionFragment);
                    break;
                case 8:
                    ReplaceFragment(LectorConteoTotalFragment);
                    break;
                case 9:
                    ReplaceFragment(lectorConsumoParcialFragment);
                    //ReplaceFragment(PruebasFragment);
                    break;
                default:
                    ReplaceFragment(lectorFragment);
                    break;
            }
        }

        private void ContinuarAction(object sender, DialogClickEventArgs e)
        {
            VariablesGlobales.CantTallosDef = Convert.ToInt32(tallos.Text);
            ReplaceFragment(lectorIngresosProduccionFragment);
        }

        public void ReplaceFragment(Fragment fragment)
        {
            if (fragment.IsVisible)
            {
                return;
            }
            var trans = FragmentManager.BeginTransaction();
            trans.Replace(Resource.Id.fragmentContainer, fragment);
            trans.AddToBackStack(null);
            trans.Commit();

            mCurrentFragment = fragment;
        }





    }
}