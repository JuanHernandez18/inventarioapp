﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.Media;
using Android.Net.Wifi;
using Android.OS;
using Android.Views;
using Android.Widget;
using InventariosApp.Clases;
using ZXing.Mobile;
using static Android.Provider.Settings;
using Environment = Android.OS.Environment;


namespace InventariosApp.Fragmentos
{
    public class Lector : Fragment
    {
        AlertDialog.Builder builder;
        private View alertLayout;
        Button btnCapturar;
        EditText txtCodigo;
        TextView txtPresentacion;
        TextView txtProducto;
        Switch btnState;
        Button btnOk;
        Button btnIngresar;
        Button btnManual;
        EditText cantTallos;
        ListView lstLectura;
        TextView txtValTallos;
        ConexionServidor con = new ConexionServidor();
        List<LstLectura> lstlectura = new List<LstLectura>();
        List<ListBD> lstBodegas = new List<ListBD>();

        bool isConnected = false;
        ArrayAdapter adapter;
        ConexionLocal db;
        GeneraTXT GeneraTXT;
        ActualizaEtiquetasDisp ActualizaEtiquetas;
        Multimedia Multimedia;

        ProgressDialog progress;

        String direccionIPLocal;
        String direccionIPPublica;
        String baseDatos;
        String usuario;
        String contrasena;
        String cadenaConexionLocal;
        String cadenaConexionPublica;
        SqlConnection conexion;
        private MediaPlayer player;


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            Multimedia = new Multimedia();

            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.FgLector, container, false);
            alertLayout = inflater.Inflate(Resource.Layout.LstBodegas, container, false);
            
            txtCodigo = view.FindViewById<EditText>(Resource.Id.txtCodigo);
            lstLectura = view.FindViewById<ListView>(Resource.Id.lstCodigos);
            txtValTallos = view.FindViewById<TextView>(Resource.Id.valTallos);
            txtPresentacion = view.FindViewById<TextView>(Resource.Id.txtPresentacion);
            txtProducto = view.FindViewById<TextView>(Resource.Id.txtProducto);
            btnIngresar = view.FindViewById<Button>(Resource.Id.btnIngresar);
            btnManual = view.FindViewById<Button>(Resource.Id.btnManual);

            try
            {
                var builder = new AlertDialog.Builder(this.Activity);
                builder.SetIcon(Resource.Drawable.warning);
                builder.SetTitle("Advertencia");
                builder.SetMessage("¿Desea Actualizar las Etiquetas Disponibles?");
                builder.SetPositiveButton("Si", OkActionActualiza);
                builder.SetNegativeButton("No", (IntentSender, args) => {
                    builder.Dispose();
                });
                builder.Show();
            }
            catch (SqlException e)
            {
                foreach (SqlError error in e.Errors)
                {
                    //var FechaRegistro = DateTime.Now.ToString();
                    //var errors = FechaRegistro + error.Number.ToString() + error.Message.ToString();

                    //GenerarArchivoTxt(errors, "errores.txt");
                    Toast.MakeText(Application.Context, "No se pudo descargar la información de etiquetas disponibles", ToastLength.Long).Show();

                    Multimedia.StartPlayer("E");

                }
                throw;
            }           

            MobileBarcodeScanner.Initialize(Activity.Application);

            txtCodigo.RequestFocus();
            btnIngresar.Click += btnIngresarInfo;
            btnManual.Click += bthManualLectura;

            lstlectura.Clear();
            lstLectura.SetAdapter(null);
            txtCodigo.Text = "";
            txtPresentacion.Text = "";
            txtProducto.Text = "";

            progress = new ProgressDialog(this.Context);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetCancelable(false);

            CargarEtiquetas();

            string NombreArchivo = "Con_" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");


            if (VariablesGlobales.FlagCamaraLector == 0)
            {
                txtCodigo.TextChanged += (object sender, Android.Text.TextChangedEventArgs e) =>
                {
                    if (txtCodigo.Text.Trim().Length == 15)
                    {

                            txtCodigo.Enabled = false;
                            //Validación digit de verificación
                            int cont = 1;
                            int par = 0;
                            int impar = 0;

                            while (cont <= 14)
                            {
                                if (cont % 2 == 0)
                                {
                                    par += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                                }
                                else
                                {
                                    impar += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                                }
                                cont++;
                            }

                            int digVerif = ((impar * 3) + par) % 10;

                            if (digVerif == Convert.ToInt32(txtCodigo.Text.Substring(14, 1)))
                            {
                            
                            txtCodigo.Enabled = false;
                            ObtenerInformacionEtiqueta();
                            txtCodigo.Enabled = true;
                            }
                            else
                            {
                                txtCodigo.Text = "";
                                txtPresentacion.Text = "";
                                txtProducto.Text = "";                                
                                Multimedia.StartPlayer("E");                                
                                Toast.MakeText(Application.Context, "Etiqueta Invalida en digito de Verificación", ToastLength.Short).Show();
                            }

                        txtCodigo.Enabled = true;
                    }
                    else
                    {
                        if(txtCodigo.Text.Length > 15)
                        {
                            string resultado = txtCodigo.Text.Substring(0, 15);
                            txtCodigo.Text = "";
                            txtCodigo.Text = resultado;
                        }
                    }
                };
            }
            else {
                btnManual.Click += async delegate
                {
                    var scanner = new MobileBarcodeScanner()
                    {
                        TopText = "Acerca la camara al elemento",
                        BottomText = "Toca la pantalla para enfocar",
                        CancelButtonText = "Cancelar"
                    };
                    try
                    {
                        var result = await scanner.Scan();
                        if (result != null)
                            txtCodigo.Text = result.ToString();

                        if (txtCodigo.Text.Length == 15)
                        {
                            //Validación digit de verificación
                            int cont = 1;
                            int par = 0;
                            int impar = 0;

                            while (cont <= 14)
                            {
                                if (cont % 2 == 0)
                                {
                                    par += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                                }
                                else
                                {
                                    impar += Convert.ToInt32(txtCodigo.Text.Substring(cont - 1, 1));
                                }
                                cont++;
                            }

                            int digVerif = ((impar * 3) + par) % 10;

                            if (digVerif == Convert.ToInt32(txtCodigo.Text.Substring(14, 1)))
                            {
                                txtCodigo.Enabled = false;
                                ObtenerInformacionEtiqueta();                                
                                txtCodigo.Enabled = true;
                            }
                            else
                            {
                                txtCodigo.Text = "";
                                txtPresentacion.Text = "";
                                txtProducto.Text = "";                                
                                Multimedia.StartPlayer("E");                               
                                Toast.MakeText(Application.Context, "Etiqueta Invalida", ToastLength.Short).Show();
                            }
                            
                        }
                        else
                        {
                            txtCodigo.Text = "";
                            txtPresentacion.Text = "";
                            txtProducto.Text = "";
                            Multimedia.StartPlayer("E");
                            Toast.MakeText(Application.Context, "Código inválido", ToastLength.Short).Show(); ;
                        }
                    }
                    catch
                    {
                        txtCodigo.Text = "";
                        txtPresentacion.Text = "";
                        txtProducto.Text = "";
                        Multimedia.StartPlayer("E");
                        Toast.MakeText(Application.Context, "Error de cámara", ToastLength.Long).Show();
                    }
                };
            }
         
            return view;
        }

        private void CargarEtiquetas()
        {
            lstlectura.Clear();

            db = new ConexionLocal(Application.Context);
            ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubir(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            int SumTallos = 0;

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    SumTallos += Convert.ToInt32(cursorEtiquetas.GetString(1));
                    lstlectura.Add(new LstLectura(cursorEtiquetas.GetString(0), cursorEtiquetas.GetString(1)));
                } while (cursorEtiquetas.MoveToNext());
            }
            lstLectura.Adapter = new AdLstLectura(Activity, lstlectura);

            var CantEtiquetas = lstlectura.Count;

            btnIngresar.Text = "INGRESAR - " + CantEtiquetas + " - " + SumTallos + " TALLOS";
        }

        private void bthManualLectura(object sender, EventArgs e)
        {
            String var = txtCodigo.Text;
            int tam_var = var.Length;

            if (tam_var == 15)
            {
                txtCodigo.Enabled = false;
                ObtenerInformacionEtiqueta();
                txtCodigo.Enabled = true;
            }
            else
            {
                if (txtCodigo.Text.Length != 0)
                {
                    txtCodigo.Text = "";
                    txtPresentacion.Text = "";
                    txtProducto.Text = "";
                    Multimedia.StartPlayer("E");
                    Toast.MakeText(Application.Context, "Codigo Inválido", ToastLength.Long).Show();                    
                }           
            }
        }

        private void btnIngresarInfo(object sender, EventArgs e)
        {
            var builder = new AlertDialog.Builder(this.Activity);
            builder.SetIcon(Resource.Drawable.warning);
            builder.SetTitle("Advertencia");
            builder.SetMessage("¿Esta seguro de registar el documento?");
            builder.SetPositiveButton("Si", OkAction);
            builder.SetNegativeButton("No", (IntentSender, args) => {
                builder.Dispose();
            });
            builder.Show();
        }

        //private void GenerarArchivoTxt(string codigo, string NombreArchivo)
        //{

        //    string pathTxt = "/sdcard/Download/" + NombreArchivo;

        //    StringBuilder constructor = new StringBuilder();         
        //    String CodigoTxt = codigo + "\r\n";

        //    if (!File.Exists(pathTxt))
        //    {
        //        StreamWriter streamWriter = File.CreateText(pathTxt);
        //        constructor.AppendLine(CodigoTxt);
        //        streamWriter.Dispose();
        //    }
        //    else
        //    {
        //        constructor.AppendLine(CodigoTxt);
        //    }
        //    //WriteAllText
        //    File.AppendAllText(pathTxt, constructor.ToString());
        //}

        private void GenerarArchivoTxtNoDisponible(string codigo)


        {
            db = new ConexionLocal(Application.Context);
            db.ConsultaLotesEtiquetanodisponible(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            string NombreArchivo = "NoDis_" + VariablesGlobales.vgNombreCodLote + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");
            //GenerarArchivoTxt(txtCodigo.Text, NombreArchivo);



            string pathTxt = "/sdcard/Download/" + NombreArchivo;

            StringBuilder constructor = new StringBuilder();

            ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubir(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
           

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    String CodigoTxt = codigo + "\r\n";

                    if (!File.Exists(pathTxt))
                    {
                        StreamWriter streamWriter = File.CreateText(pathTxt);
                        constructor.AppendLine(CodigoTxt);
                        streamWriter.Dispose();
                    }
                    else
                    {
                        constructor.AppendLine(CodigoTxt);
                    }
                    //WriteAllText
                    File.AppendAllText(pathTxt, constructor.ToString());
                } while (cursorEtiquetas.MoveToNext());
            }






            //String CodigoTxt = codigo + "\r\n";

            //if (!File.Exists(pathTxt))
            //{
            //    StreamWriter streamWriter = File.CreateText(pathTxt);
            //    constructor.AppendLine(CodigoTxt);
            //    streamWriter.Dispose();
            //}
            //else
            //{
            //    constructor.AppendLine(CodigoTxt);
            //}
            ////WriteAllText
            //File.AppendAllText(pathTxt, constructor.ToString());
        }

        private void GenerarArchivo()
        {
            ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirNoDisp(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            string NombreArchivo = VariablesGlobales.vgIDBodega + "_con_" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    GeneraTXT  = new GeneraTXT();
                    GeneraTXT.GenerarArchivoTxt(cursorEtiquetas.GetString(0), NombreArchivo);
                } while (cursorEtiquetas.MoveToNext());
            }
        }

        private void OkAction(object sender, DialogClickEventArgs e)
        {
            GenerarArchivo();

            if (VariablesGlobales.vgIDDocumento == 2)
            {
                if (lstlectura.Count == 0)
                {
                    Toast.MakeText(Application.Context, "No hay datos para generar consumos", ToastLength.Long).Show();
                }
                else
                {
                    db = new ConexionLocal(Application.Context);
                    ICursor cursorDatosConexion = db.ConsultaDatosConexion();
                    
                    if (cursorDatosConexion.MoveToFirst())
                    {
                        do
                        {
                            direccionIPLocal = cursorDatosConexion.GetString(1);
                            direccionIPPublica = cursorDatosConexion.GetString(2);
                            baseDatos = cursorDatosConexion.GetString(3);
                            usuario = cursorDatosConexion.GetString(4);
                            contrasena = cursorDatosConexion.GetString(5);

                        } while (cursorDatosConexion.MoveToNext());
                    }

                    cadenaConexionLocal = @"data source= " + direccionIPLocal + ";initial catalog=" + baseDatos + ";user id=" + usuario + ";password= " + contrasena + ";Connect Timeout=0";
               
                    conexion = new SqlConnection(cadenaConexionLocal);

                    int valConex = 0;
                    try
                    {
                        conexion.Open();
                        valConex = 1;
                    }
                    catch (SqlException Exp)
                    {
                        Toast.MakeText(Application.Context, "Error de conexion", ToastLength.Long).Show();                        
                        throw;
                    }

                    if (valConex == 1)
                    {
                        MyTaskSync tarea = new MyTaskSync(progress, VariablesGlobales.vgIDDocumento, conexion);
                        tarea.Execute();
                    }
                }
                CargarEtiquetas();
            }
        }

        private void OkActionActualiza(object sender, DialogClickEventArgs e)
        {
            ActualizaEtiquetas = new ActualizaEtiquetasDisp();
            ActualizaEtiquetas.CargarEtiquetasDisponibles(1);
        }

        private void LimpiarTablaEtiquetas()
        {
            db = new ConexionLocal(Application.Context);
            db.EliminarDatosEtiquetas(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            txtCodigo.Text = "";
            txtPresentacion.Text = "";
            txtProducto.Text = "";
            txtValTallos.Text = "";
        }

        private void ItemBodegaSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var l = lstBodegas[e.Position];
            VariablesGlobales.vgIDBodega = l.ID;
        }
        
        private void btnOKClick(object sender, EventArgs e)
        {
            if (txtCodigo.Text == "" || cantTallos.Text == "")
            {
                Toast.MakeText(Application.Context, "Falta valores por ingresar", ToastLength.Long).Show();
            }
            else {
                int boolVal = 0;
                foreach (LstLectura item in this.lstlectura)
                {
                    if (item.codigo == txtCodigo.Text)
                    {
                        boolVal = 1;
                    }
                }

                if (boolVal == 0)
                {
                    if (Convert.ToInt32(cantTallos.Text) > Convert.ToInt32(txtValTallos.Text))
                    {
                        Toast.MakeText(Application.Context, "No puede ingresar esa cantidad de tallos", ToastLength.Long).Show();
                    }
                    else
                    {
                        lstlectura.Add(new LstLectura(txtCodigo.Text, txtPresentacion.Text));
                        adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstlectura);

                        lstLectura.Adapter = new AdLstLectura(Activity, lstlectura);

                        txtCodigo.Text = "";
                        cantTallos.Text = "";
                        txtPresentacion.Text = "";
                        txtProducto.Text = "";
                    }
                }
                else
                {
                    txtCodigo.Text = "";
                    cantTallos.Text = "";
                    txtPresentacion.Text = "";
                    txtProducto.Text = "";
                    Multimedia.StartPlayer("E");
                    Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Long).Show();                   
                }
            }
        }
                
        private void LimpiaCamposForm()
        {
            lstlectura.Clear();
            lstLectura.SetAdapter(null);
            txtPresentacion.Text = "";
            txtProducto.Text = "";
        }
        private void ObtenerInformacionEtiqueta()
        {
            String PresentacionEt = "";
            String ProductoEt = "";
            String ValTallosEt = "";
            String var = txtCodigo.Text;
            int varIDBodega = 0;

            int boolVal = 0;
            foreach (LstLectura item in this.lstlectura)
            {
                if (item.codigo == txtCodigo.Text)
                {
                    boolVal = 1;
                }
            }

            if (var != "")
            {
                if (boolVal == 0)
                {

                    int tam_var = var.Length;
                    String CodigoEtiqueta = var.Substring(0, tam_var - 1);

                    //SqlDataReader EtiquetasC;

                    //ConexionServidor con = new ConexionServidor();

                    //EtiquetasC = con.ConsultaEtiqueta(CodigoEtiqueta, VariablesGlobales.vgIDBodega);
                    db = new ConexionLocal(Application.Context);
                    //ConexionServidor con = new ConexionServidor();

                    //LoginOk = con.Login(Usuario.Text, Contrasena.Text);
                    //LoginOk = db.ConsultaLoginUsuarios(Usuario.Text, Contrasena.Text);
                    ICursor EtiquetasC = db.ConsultaEtiquetasDisponibles(CodigoEtiqueta, VariablesGlobales.vgIDBodega);
                    
                    //if (EtiquetasC != null && EtiquetasC.HasRows)
                    if (EtiquetasC.MoveToFirst())
                    {

                        do
                        {
                            //var PrincipalActivity = new Intent(this, typeof(MainActivity));
                            //PrincipalActivity.PutExtra("ID", Convert.ToString(LoginOk.GetInt(0)));
                            //PrincipalActivity.PutExtra("Usuario", LoginOk.GetString(1));
                            //this.StartActivity(PrincipalActivity);
                            PresentacionEt = EtiquetasC.GetString(0);
                            ProductoEt = EtiquetasC.GetString(2);
                            ValTallosEt = Convert.ToString(EtiquetasC.GetDouble(1));
                            varIDBodega = EtiquetasC.GetInt(3);
                            
                        } while (EtiquetasC.MoveToNext());
                        //while (EtiquetasC.Read())
                        //{

                        //    PresentacionEt = EtiquetasC.GetString(0);
                        //    ProductoEt = EtiquetasC.GetString(2);
                        //    ValTallosEt = Convert.ToString(EtiquetasC.GetDouble(1));
                        //    varIDBodega = EtiquetasC.GetInt32(3);
                        //}

                        //validacion que no exista etiqueta

                        db = new ConexionLocal(Application.Context);
                        ICursor cursorEtiquetasExist = db.ConsultaEtiquetasExist(txtCodigo.Text);
                        int eti = 0;
                        if (cursorEtiquetasExist.MoveToFirst())
                        {
                            do
                            {
                                eti = cursorEtiquetasExist.GetInt(0);
                            } while (cursorEtiquetasExist.MoveToNext());
                        }

                        if (eti == 0)
                        {
                            GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 1,1);
                            AgregarEtiqueta(txtCodigo.Text, ValTallosEt, varIDBodega);
                            Multimedia.StartPlayer("B");                            
                        }
                        else
                        {
                            txtCodigo.Text = "";
                            txtPresentacion.Text = "";
                            txtProducto.Text = "";
                            //player = new MediaPlayer();
                            Multimedia.StartPlayer("E");
                            //String FilePath = "/sdcard/Music/TonoError.mp3";
                            //player.SetDataSource(FilePath);
                            //player.Prepare();
                            //player.Start();
                            Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
                            
                        }
                    }
                    else
                    {
                        GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0,0);
                        //player = new MediaPlayer();
                        Multimedia.StartPlayer("E");
                        //String FilePath = "/sdcard/Music/TonoError.mp3";                        
                        //Multimedia = new Multimedia();
                        //Multimedia.StartPlayer(FilePath);
                        //player.SetDataSource(FilePath);
                        //player.Prepare();
                        //player.Start();
                        Toast.MakeText(Application.Context, "Etiqueta no disponible", ToastLength.Short).Show();
                    }

                    if (ValTallosEt != "")
                    {
                        if (Convert.ToInt32(ValTallosEt) == 0)
                        {
                            txtCodigo.Text = "";
                            Toast.MakeText(Application.Context, "No tiene tallos disponibles para consumo", ToastLength.Short).Show();
                        }
                        else
                        {
                            txtPresentacion.Text = PresentacionEt;
                            txtProducto.Text = ProductoEt;
                            txtValTallos.Text = ValTallosEt;
                        }
                    }
                    else
                    {
                        txtCodigo.Text = "";
                    }
                }
                else
                {
                    txtCodigo.Text = "";
                    txtPresentacion.Text = "";
                    txtProducto.Text = "";
                    Multimedia.StartPlayer("E");
                    //player = new MediaPlayer();
                    //String FilePath = "/sdcard/Music/TonoError.mp3";
                    //player.SetDataSource(FilePath);
                    //player.Prepare();
                    //player.Start();
                    Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
                }
            }
        }

        public class MyTaskSync : AsyncTask
        {
            private ConexionLocal db;
            SqlDataReader CabezaDocumento;
            ConexionServidor con = new ConexionServidor();
            ProgressDialog progress;
            int IDTipoDocumento;
            SqlConnection conexion;
            String tablaTemporal;
            String NombreEquipo = "";
            public MyTaskSync(ProgressDialog progress, int IDDocumento, SqlConnection conexion)
            {
                this.progress = progress;
                this.IDTipoDocumento = IDDocumento;
                this.conexion = conexion;
            }

            protected override void OnPreExecute()
            {
                progress.SetMessage("Generando documento ...");
                progress.Show();
            }
            protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
            {
                VerificaTablaTemporalEtiquetas();
                CargarEtiquetasTemporalConsumos();
                
                
                return "d";
            }
            

            private void VerificaTablaTemporalEtiquetas()
            {
                SqlDataReader tablaEtiquetas;

                ConexionServidor con = new ConexionServidor();
                String id = Secure.GetString(Application.Context.ContentResolver, Secure.AndroidId);
                int tam_var = id.Length;
                String IDLectora = id.Substring(tam_var - 2, 2);

                NombreEquipo = "Lec" + IDLectora;
                var NombreTabla = "AppMovilInventarios_Lec" + IDLectora;
               
                tablaEtiquetas = con.VerificaTablaTemporal(VariablesGlobales.vgIDUsuario, NombreTabla);

                if (tablaEtiquetas.HasRows)
                {
                    while (tablaEtiquetas.Read())
                    {
                        tablaTemporal = tablaEtiquetas.GetString(0);
                    }
                }
            }

            private void CargarEtiquetasTemporalConsumos()
            {

                db = new ConexionLocal(Application.Context);
                ICursor cursorlotes = db.ConsultaLotesSinSubir(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
                       
                if (cursorlotes.MoveToFirst())
                {
                    do
                    {
                        //dlote = cursorlotes.GetInt(0);
                        insertaEtiquetaTemporalXLote(cursorlotes.GetInt(0));
                        // con.InsertaEtiquetaTablaTemporal(cursorEtiquetas.GetString(0), VariablesGlobales.vgTablaTemp);
                    } while (cursorlotes.MoveToNext());
                }
            }
             private void insertaEtiquetaTemporalXLote(int lote)
             {
                VerificaTablaTemporalEtiquetas();
                
                ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirXLote(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento, lote);

                string sql = "INSERT INTO " + tablaTemporal + "(Etiqueta) VALUES (@Etiqueta)";

                if (cursorEtiquetas.MoveToFirst())
                {
                    do
                    {
                        using (SqlCommand comando = new SqlCommand(sql, conexion))
                        {
                            comando.Parameters.Add("@Etiqueta", SqlDbType.VarChar).Value = cursorEtiquetas.GetString(0);
                            comando.CommandType = CommandType.Text;
                            comando.ExecuteNonQuery();
                        }
                    } while (cursorEtiquetas.MoveToNext());
                }
                insertaDocumentoConsumos(lote);
             }

            private void insertaDocumentoConsumos(int lote)
            {
                //CABEZA DOCUMENTO
                CabezaDocumento = con.GeneraCabezaDocFlor(2, VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDUnidades, VariablesGlobales.vgIDCompanias, VariablesGlobales.vgIDCentroC, 0, 0, 0, 0, 0, lote, VariablesGlobales.vgIDUsuario,"");

                if (CabezaDocumento.HasRows)
                {
                    while (CabezaDocumento.Read())
                    {
                        VariablesGlobales.vgIDConsumo = CabezaDocumento.GetInt32(0);
                    }
                }

                //DETTALLES ETIQUETAS DOCUMENTO
                con.GeneraDetallesEtiquetasDocFlorConsumos(2, tablaTemporal, VariablesGlobales.vgIDConsumo, NombreEquipo, VariablesGlobales.vgNombreUsuario);
                              
                //COMPACTA DETTALLES ETIQUETAS DOCUMENTO
                con.CompactaDocConsumoFlorDetalles(VariablesGlobales.vgIDConsumo);
            }

           
            private void LimpiarTablaEtiquetas()
            {
                db = new ConexionLocal(Application.Context);
                db.EliminarDatosEtiquetas(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);                
            }
            protected override void OnPostExecute(Java.Lang.Object result)
            {
                Toast.MakeText(Application.Context, "Documento Registrado", ToastLength.Long).Show();
                progress.Dismiss();
                LimpiarTablaEtiquetas();
                db.BorrarDatosEtiquetasDisp(1);
            }
            
        }

        private void GuardarEtiLocal(string codigoEtiqueta, string valTallosEt, int vgIDBodega, int FlagDisponible,int ActualizaEliminar)
        {
            db = new ConexionLocal(Application.Context);
            

            string fecha = string.Format("{0:ddMMyyyy}", DateTime.Now);
            int CantTallos;
            if (valTallosEt.Length == 0)
            {
                CantTallos = 0;
            }
            else
            {
                CantTallos = Convert.ToInt32(valTallosEt);
            }
            db.GuardarDatosEtiquetas(codigoEtiqueta, CantTallos, vgIDBodega, VariablesGlobales.vgIDDocumento, VariablesGlobales.vgIDCodLote, Convert.ToInt32(fecha), FlagDisponible);
            if (ActualizaEliminar == 1)
            {
                //Actualizo la etiquetas a eliminar
                db.ActualizaEtiquetasEliminar(txtCodigo.Text, 1);
            }
        }

        private void AgregarEtiqueta(string codigoEtiqueta, string valTallosEt, int IDBodega)
        {
            if (VariablesGlobales.vgIDBodega == IDBodega)
            {
                lstlectura.Add(new LstLectura(codigoEtiqueta, valTallosEt));
                adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstlectura);

                lstLectura.Adapter = new AdLstLectura(Activity, lstlectura);
            }

            var CantEtiquetas = lstlectura.Count;

            btnIngresar.Text = "INGRESAR - " + CantEtiquetas;


            //Toast.MakeText(Application.Context, "OK", ToastLength.Short).Show();
            txtCodigo.Text = "";
            txtPresentacion.Text = "";
            txtProducto.Text = "";
        }

        private void btnConexion(object sender, EventArgs e)
        {
            WifiManager wifi = (WifiManager)Application.Context.GetSystemService(Context.WifiService);
            if (isConnected == false)
            {               
                wifi.SetWifiEnabled(true);
            }
            else
            {
                wifi.SetWifiEnabled(false);
            }
        }
    }
}
