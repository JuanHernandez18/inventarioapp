﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using InventariosApp.Clases;
using static Android.Provider.Settings;

namespace InventariosApp.Fragmentos
{
    public class LectorConteoTotal : Fragment
    {
        Button btnIngresar;
        ConexionLocal db;
        String direccionIPLocal;
        String direccionIPPublica;
        String baseDatos;
        String usuario;
        String contrasena;
        String cadenaConexionLocal;
        String cadenaConexionPublica;
        SqlConnection conexion;
        ProgressDialog progress;

        EditText txtCodigo;
        ListView lstLectura;
        TextView txtValTallos;
        TextView txtPresentacion;
        TextView txtProducto;
        Button btnManual;
        List<ListBD> lstCalidades = new List<ListBD>();
        List<LstLectura> lstlectura = new List<LstLectura>();

        EditText etTallosxRamo;
        EditText etCosechador;
        Spinner spCalidades;
        ArrayAdapter adapter;

        int etiExist = 0;

        EditText IDCalidad;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View view = inflater.Inflate(Resource.Layout.FgLectorConteoTotal, container, false);

            progress = new ProgressDialog(this.Context);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetCancelable(false);


            txtCodigo = view.FindViewById<EditText>(Resource.Id.txtCodigo);
            lstLectura = view.FindViewById<ListView>(Resource.Id.lstCodigos);
            btnIngresar = view.FindViewById<Button>(Resource.Id.btnIngresar);
            btnManual = view.FindViewById<Button>(Resource.Id.btnManual);

          
            txtCodigo.RequestFocus();
            btnIngresar.Click += BtnIngresar_Click;
            btnManual.Click += BtnManual_Click; ;
            //Conexion();
            // MobileBarcodeScanner.Initialize(Activity.Application);

            lstlectura.Clear();
            lstLectura.SetAdapter(null);
            txtCodigo.Text = "";
                      

            txtCodigo.TextChanged += (object sender, Android.Text.TextChangedEventArgs e) => {
                if (txtCodigo.Text.Length == 15)
                {
                    ObtenerInformacionEtiqueta();
                };
            };

            CargarEtiquetas();
            return view;
        }



        private void CargarEtiquetas()
        {
            lstlectura.Clear();

            db = new ConexionLocal(Application.Context);
            ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubir(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    lstlectura.Add(new LstLectura(cursorEtiquetas.GetString(0), ""));
                } while (cursorEtiquetas.MoveToNext());
            }
            lstLectura.Adapter = new AdLstLectura(Activity, lstlectura);

            var a = lstlectura.Count;

            btnIngresar.Text = "INGRESAR - " + a;
        }

        
        private void BtnManual_Click(object sender, EventArgs e)
        {

            if (etiExist == 0)
            {
                GuardarEtiLocal(txtCodigo.Text, "");
                CargarEtiquetas();
                LimpiarCampos();
                //AgregarEtiqueta(txtCodigo.Text, ValTallosEt, varIDBodega);
            }
            else
            {
                txtCodigo.Text = "";
                txtPresentacion.Text = "";
                txtProducto.Text = "";
                etTallosxRamo.Text = "";
                etCosechador.Text = "";
                Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
            }
        }

        private void LimpiarCampos()
        {
            txtCodigo.Text = "";
            txtPresentacion.Text = "";
            txtProducto.Text = "";
            etTallosxRamo.Text = "";
            etCosechador.Text = "";
        }

        private void GuardarEtiLocal(string text, string tallos)
        {
            db = new ConexionLocal(Application.Context);
            String hora = DateTime.Now.ToString("hh:mm:ss");
            string fecha = string.Format("{0:ddMMyyyy}", DateTime.Now);
            db.GuardarDatosEtiquetas(text, Convert.ToInt32(tallos), VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento, 0,Convert.ToInt32(fecha), 1);
        }

        private void ObtenerInformacionEtiqueta()
        {
            String PresentacionEt = "";
            String ProductoEt = "";
            String ValTallosEt = "";
            String var = txtCodigo.Text;
            int varIDBodega = 0;

            int boolVal = 0;
            foreach (LstLectura item in this.lstlectura)
            {
                if (item.codigo == txtCodigo.Text)
                {
                    boolVal = 1;
                }
            }

            if (var != "")
            {
                if (boolVal == 0)
                {

                    int tam_var = var.Length;
                    String CodigoEtiqueta = var.Substring(0, tam_var - 1);

                    SqlDataReader Etiquetas;

                    ConexionServidor con = new ConexionServidor();

                    Etiquetas = con.ConsultaEtiquetaConteoTotal(CodigoEtiqueta, VariablesGlobales.vgIDBodega);


                    if (Etiquetas.HasRows)
                    {
                        //validacion que no exista etiqueta

                        db = new ConexionLocal(Application.Context);
                        ICursor cursorEtiquetasExist = db.ConsultaEtiquetasExist(txtCodigo.Text);
                       
                        if (cursorEtiquetasExist.MoveToFirst())
                        {
                            do
                            {
                                etiExist = cursorEtiquetasExist.GetInt(0);
                            } while (cursorEtiquetasExist.MoveToNext());
                        }

                        if (etiExist == 0)
                        {
                            GuardarEtiLocal(txtCodigo.Text, "0", VariablesGlobales.vgIDBodega);
                            txtCodigo.Text = "";
                        }
                        else
                        {
                            txtCodigo.Text = "";
                            txtPresentacion.Text = "";
                            txtProducto.Text = "";
                            Toast.MakeText(Application.Context, "Etiqueta no disponible", ToastLength.Short).Show();
                        }
                    }
                    else
                    {
                        txtCodigo.Text = "";
                        Toast.MakeText(Application.Context, "Etiqueta no disponible", ToastLength.Short).Show();
                    }
                }
                else
                {
                    txtCodigo.Text = "";
                    txtPresentacion.Text = "";
                    txtProducto.Text = "";
                    Toast.MakeText(Application.Context, "Etiqueta ya leida", ToastLength.Short).Show();
                }
            }

            CargarEtiquetas();
        }

        private void GuardarEtiLocal(string codigoEtiqueta, string valTallosEt, int vgIDBodega)
        {
            db = new ConexionLocal(Application.Context);
            string fecha = string.Format("{0:ddMMyyyy}", DateTime.Now);
            db.GuardarDatosEtiquetas(codigoEtiqueta, Convert.ToInt32(valTallosEt), vgIDBodega, VariablesGlobales.vgIDDocumento, 0, Convert.ToInt32(fecha), 1);

        }
            private void BtnIngresar_Click(object sender, EventArgs e)
        {
            db = new ConexionLocal(Application.Context);
            ICursor cursorDatosConexion = db.ConsultaDatosConexion();


            if (cursorDatosConexion.MoveToFirst())
            {
                do
                {
                    direccionIPLocal = cursorDatosConexion.GetString(1);
                    direccionIPPublica = cursorDatosConexion.GetString(2);
                    baseDatos = cursorDatosConexion.GetString(3);
                    usuario = cursorDatosConexion.GetString(4);
                    contrasena = cursorDatosConexion.GetString(5);

                } while (cursorDatosConexion.MoveToNext());
            }

            cadenaConexionLocal = @"data source= " + direccionIPLocal + ";initial catalog=" + baseDatos + ";user id=" + usuario + ";password= " + contrasena + ";Connect Timeout=0";
            cadenaConexionPublica = @"data source= " + direccionIPPublica + ";initial catalog=" + baseDatos + ";user id=" + usuario + ";password= " + contrasena + ";Connect Timeout=0";

            conexion = new SqlConnection(cadenaConexionLocal);

            int valConex = 0;
            try
            {
                conexion.Open();
                valConex = 1;
            }
            catch (SqlException Exp)
            {
                conexion = new SqlConnection(cadenaConexionPublica);
                try
                {
                    conexion.Open();
                    valConex = 1;
                }
                catch (SqlException ex)
                {
                    Toast.MakeText(Application.Context, "No estas conectado a internet", ToastLength.Long).Show();
                }
            }

            if (valConex == 1)
            {
                MyTaskSync tarea = new MyTaskSync(progress, conexion);
                tarea.Execute();
            }
        }

        public class MyTaskSync : AsyncTask
        {
            private ConexionLocal db;
            SqlDataReader CabezaDocumento;
            SqlDataReader CabezaDocumentoPrevio;
            ConexionServidor con = new ConexionServidor();
            ProgressDialog progress;
            SqlConnection conexion;
            String tablaTemporal;
            String NombreEquipo = "";
            public MyTaskSync(ProgressDialog progress,  SqlConnection conexion)
            {
                this.progress = progress;
                this.conexion = conexion;
            }

            protected override void OnPreExecute()
            {
                progress.SetMessage("Generando documento ...");
                progress.Show();
            }
            protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
            {
                db = new ConexionLocal(Application.Context);
                InsertaDatosTemporalConteoTotal();
                IngresaEtiquetasTablaTemporal();
                return "d";
            }

            private void InsertaDatosTemporalConteoTotal()
            {
                //Tabla para cargar las etiquetas
                SqlDataReader tablaEtiquetas;

                ConexionServidor con = new ConexionServidor();

                String id = Secure.GetString(Application.Context.ContentResolver, Secure.AndroidId);
                int tam_var = id.Length;
                String IDLectora = id.Substring(tam_var - 2, 2);

                NombreEquipo = "Lec" + IDLectora;
                var NombreTabla = "AppMovilInventariosCTLec" + IDLectora;

                tablaEtiquetas = con.VerificaTablaTemporalConteo(1, NombreTabla);

                if (tablaEtiquetas.HasRows)
                {
                    while (tablaEtiquetas.Read())
                    {
                        tablaTemporal = tablaEtiquetas.GetString(0);
                    }
                }
            }


            private void IngresaEtiquetasTablaTemporal()
            {
                ICursor cursorEtiquetas = db.ConsultaEtiquetasSinSubirCT(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);

                string sql = "INSERT INTO " + tablaTemporal + "(Etiqueta) VALUES (@Etiqueta)";

                if (cursorEtiquetas.MoveToFirst())
                {
                    do
                    {
                        using (SqlCommand comando = new SqlCommand(sql, conexion))
                        {
                            comando.Parameters.Add("@Etiqueta", SqlDbType.VarChar).Value = cursorEtiquetas.GetString(0);                        

                            comando.CommandType = CommandType.Text;
                            comando.ExecuteNonQuery();
                        }
                    } while (cursorEtiquetas.MoveToNext());
                }
                insertaDocumento();
            }



            private void insertaDocumento()
            {
                //CABEZA DOCUMENTO
                CabezaDocumento = con.GeneraCabezaDocFlor(3, VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDUnidades, VariablesGlobales.vgIDCompanias, VariablesGlobales.vgIDCentroC, 0, 0, 0, 0, 0, 0, VariablesGlobales.vgIDUsuario, "");

                if (CabezaDocumento.HasRows)
                {
                    while (CabezaDocumento.Read())
                    {
                        VariablesGlobales.vgIDInterno = CabezaDocumento.GetInt32(0);
                    }
                }
                //DETTALLES ETIQUETAS DOCUMENTO
                con.GeneraDetallesEtiquetasDocFlor(3, tablaTemporal, VariablesGlobales.vgIDInterno);

                //COMPACTA DETTALLES ETIQUETAS DOCUMENTO
                con.CompactaDocTrasladoInternoFlorDetalles(VariablesGlobales.vgIDInterno);
            }

            private void LimpiarTablaEtiquetas()
            {
                db = new ConexionLocal(Application.Context);
                db.EliminarDatosEtiquetas(VariablesGlobales.vgIDBodega, VariablesGlobales.vgIDDocumento);
            }
            protected override void OnPostExecute(Java.Lang.Object result)
            {
                Toast.MakeText(Application.Context, "Documento Registrado", ToastLength.Long).Show();
                LimpiarTablaEtiquetas();
                progress.Dismiss();
            }
        }
    }
}