﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace InventariosApp.Clases
{
    public static class VariablesGlobales
    {
        public static int FlagCamaraLector = 0;
        public static int CantTallosDef;
        public static int vgIDUsuario;
        public static string vgNombreUsuario;
        public static int vgIDConsumo;
        public static int vgIDTraslado;
        public static int vgIDInterno;
        public static string vgTablaTemp;
        public static int vgIDCompanias;
        public static int vgIDUnidades;
        public static int vgIDBodega;        
        public static int vgIDCentroC;
        public static int vgIDDocumento;
        public static int vgIDCamion;
        public static int vgIDConductor;
        public static int vgIDMotivo;
        public static int vgIDDestino;
        public static int vgIDBodegaDest;
        public static int vgIDCodLote;
        public static string vgNombreCodLote;
        public static string vgPrecinto;
        public static string vgCodigoTraslado;
        public static int vgCodigoConsumo;
        public static int vgIDRemision;
        public static int vgFlagExterno;
        public static string vgIDUnidadesUsr;
        public static int vgDatosMaestros;
        public static int vgEtiquetasConsumos;
        public static int vgEtiquetasIXP;
        

    }
}