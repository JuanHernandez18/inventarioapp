﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.Database.Sqlite;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;

namespace InventariosApp.Clases
{
    class ConexionLocal : SQLiteOpenHelper
    {
        private static String dbName = "inventarios.bd";
        private MediaPlayer player;


        //Declaracion de variables para tablas
        //variables que contienen los datos a guardar en la tabla de Compania
        public static string IDTABLADATOSCONEXION = "_id";
        public static string DIRECCIONIPLOCAL = "DireccionLocal";
        public static string DIRECCIONIPPUBLICA = "DireccionPublica";
        public static string BASEDATOS = "BaseDatos";
        public static string USUARIO = "Usuario";
        public static string CONTRASENA = "contrasena";
        public static string EXTERNO = "Externo";
        private static string TABLADATOSCONEXION = "AppConexion";

        
        //variables para guardar etiquetas 
        public static string CODIGOETIQUETA = "CodigoEtiqueta";
        public static string CANTTALLOS = "CantTallos";
        public static string IDTIPODOCUMENTO = "IDTipoDocumento";
        public static string IDLOTEBQT = "IDLoteBQT";
        public static string IDBODEGA = "IDBodega";
        public static string FECHALECTURA = "Fecha";
        public static string FLAGDISPONIBLE = "FlagDisponible";
        private static string TABLAETIQUETAS = "AppEtiquetas";

        //variables para guardar etiquetas Ingresos Producción
        public static string CODIGOETIQUETAINP = "CodigoEtiqueta";
        public static string CANTTALLOSIP = "CantTallos";
        public static string IDTIPODOCUMENTOIP = "IDTipoDocumento";
        public static string IDBODEGAIP = "IDBodega";
        public static string COSECHADOR = "Cosechador";
        public static string CALIDAD = "Calidad";
        public static string HORA = "Hora";
        private static string TABLAETIQUETASINP = "AppEtiquetasIP";

        //variables para guardar etiquetas consumo parcial
        public static string CODIGOETIQUETACP = "CodigoEtiqueta";
        public static string CANTTALLOSCP = "CantTallos";
        public static string CANTTALLOORICP = "CanttallosActu";
        public static string IDTIPODOCUMENTOCP = "IDTipoDocumento";
        public static string IDBODEGACP = "IDBodega";        
        public static string CALIDADCP = "Calidad";
        public static string HORACP = "Hora";
        private static string TABLAETIQUETASCP = "AppEtiquetasCP";



        //variables tabla de errores 
        public static string IDTABLALOG = "_id";
        public static string EXCEPCION = "ExcepcionApp";
        public static string FECHA = "FechaLogApp";
        public static string TABLALOG = "LogApp";

        //variables para guardar USUARIO
        public static string ID = "ID";
        public static string LOGIN = "Login";
        public static string PASSWORD = "Password";
        public static string NOMBRE = "Nombre";
        public static string APELLIDOS = "Apellidos";
        public static string IDAPPPERFILESUSRS = "IDAppPerfilesUsrs";
        public static string EMAIL = "EMail";
        public static string TABLAAPPUSRS = "AppUsrs";

        //variables para guardar compañia
        public static string IDCOMPANIA = "Id";
        public static string NOMBRECOMPANIA = "Nombre";
        public static string DIRECCIONCOMPANIA = "Direccion";
        public static string TELEFONOCOMPANIA = "Telefono";
        public static string FLAGACTIVOCOMPANIA = "FlagActivo";
        public static string TABLACOMPANIAS = "Companias";

        //variables para guardar Unidades de Negocios
        public static string IDUNIDADESNEGOCIO = "Id";
        public static string NOMBREUNIDADESNEGOCIO = "Nombre";
        public static string IDCOMPANIAUN = "IDCompanias";
        public static string IDFUNCIONESUNIDADESNEGOCIO = "IDFuncionesUnidadesNegocio";
        public static string FLAGACTIVOUNIDADNEGOCIO = "FlagActivo";
        public static string TABLAUNIDADESNEGOCIOS = "UnidadesNegocios";
        

        //variables para guardar Unidades de Negocios Usuarios
        public static string IDUNIDADNEGOCIOAPPUSRS = "Id";
        public static string IDUNIDADESNEGOCIOAPPUSRS = "IDUnidadesNegocios";
        public static string IDAPPUSRS = "IDAppUsrs";
        public static string FLAGACTIVOAPPUSRS = "FlagActivo";
        public static string TABLAUNIDADESNEGOCIOSAPPUSRS = "UnidadesNegociosAppUsrs";


        //variables para guardar Centros de Costo
        public static string IDCENTROSDECOSTO = "ID";
        public static string CODIGOCENTROSDECOSTO = "Codigo";
        public static string NOMBRECENTROSDECOSTO = "Nombre";
        public static string FLAGACTIVOCENTROSDECOSTO = "FlagActivo";
        public static string NIVELCENTROSDECOSTO = "Nivel";
        public static string TABLACENTROSDECOSTO = "CentrosdeCosto";

        //variables para guardar Lotes de Bouquetera
        public static string IDLOTESBOUQUETERA = "ID";
        public static string NOMBRELOTESBOUQUETERA = "Nombre";
        public static string DESCRIPCIONLOTESBOUQUETERA = "Descripcion";
        public static string FLAGACTIVOLOTESBOUQUETERA = "FlagActivo";
        public static string TABLALOTESBOUQUETERA = "LotesBouquetera";

        //variables para guardar Motivos dado de baja
        public static string IDLOTESMOTIVOSBAJAS = "ID";
        public static string NOMBREMOTIVOSBAJAS = "Nombre";
        public static string IDBAS_TIPOMOTIVOBAJA = "IDBAS_TipoMotivoBaja";
        public static string FLAGACTIVOMOTIVOSBAJAS = "FlagActivo";
        public static string TABLALOTESMOTIVOSBAJAS = "BAS_MotivosDadosBaja";

        //variables para guardar Destino de Baja
        public static string IDDESTINOBAJAS = "ID";
        public static string NOMBREDESTINOBAJAS = "Nombre";
        public static string IDBAS_TIPODESTINOBAJAS = "IDBAS_TipoDestinoBaja";
        public static string FLAGACTIVODESTINOBAJAS = "FlagActivo";
        public static string TABLADESTINOBAJAS = "DestinoBajas";

        //variables para guardar Bodega de Destino
        public static string IDBODEGADESTINO = "ID";
        public static string NOMBREBODEGADESTINO = "Nombre";
        public static string FLAGACTIVOBODEGADESTINO = "FlagActivo";
        public static string IDUNIDADESNEGOCIOSBODEGADESTINO = "IDUnidadesNegocios";
        public static string FLAGAUTOCONFIRMARREMISIONES = "FlagAutoConfirmarRemisiones";
        public static string IDCOMPANIABODEGADESTINO = "IDCompanias";
        public static string TABLABODEGADESTINO = "BodegaDestino";


        //variables para guardar los Camiones
        public static string IDCAMIONES = "ID";
        public static string CODIGOCAMIONES = "CODIGO";
        public static string NOMBRECAMIONES = "Nombre";
        public static string CAPACIDADCAMIONES = "Capacidad";
        public static string PLACASCAMIONES = "Placas";
        public static string TIPOVEHICULOCAMIONES = "TipoVehiculos";
        public static string FLAGACTIVOCAMIONES = "FlagActivo";
        public static string TABLACAMIONES = "Camiones";

        //variables para guardar los Conductor
        public static string IDCONDUCTOR = "ID";
        public static string CODIGOCONDUCTOR = "CODIGO";
        public static string NOMBRECONDUCTOR = "Nombre";
        public static string DOCUMENTOSCONDUCTOR = "Documento";
        public static string FLAGACTIVOCONDUCTOR = "FlagActivo";
        public static string CIUDADDOCUMENTO = "CiudadDocumentos";
        public static string TABLACONDUCTOR = "Conductores";

        //variables para los Empleados
        public static string IDEMPLEADOS = "ID"; 
        public static string CODIGO = "Codigo";
        public static string NOMBRES = "Nombres";
        public static string APELLIDO1 = "Apellido1";
        public static string APELLIDO2 = "Apellido2";        
        public static string FLAGACTIVOEMPLEADOS = "FlagActivo";        
        public static string TABLAEMPLEADOS = "Empleados";

        //variables para guardar etiquetas disponibles
        public static string CODIGOETIQUETADIS = "CodigoEtiqueta";
        public static string IDUNIDADESNEGOCIOSDIS = "IDUnidadesNegocios";        
        public static string PRESENTACIONMEDIDADIS = "PresentacionMedida";
        public static string CANTTALLOSDIS = "Cantidad";
        public static string PRODUCTO = "Producto";        
        public static string IDBODEGADIS = "IDBodega";
        public static string FLAGELIMINAR = "FlagEliminar";
        private static string TABLAETIQUETASDIS = "EtiquetasDis";

        //variables para guardar etiquetas disponibles IXP
        public static string CODIGOETIQUETADISIXP = "CodigoEtiqueta";
        public static string IDUNIDADESNEGOCIOSDISIXP = "IDUnidadesNegocios";
        public static string PRESENTACIONMEDIDADISIXP = "PresentacionMedida";
        public static string CANTTALLOSDISIXP = "Cantidad";
        public static string PRODUCTOIXP = "Producto";
        public static string IDBODEGADISIXP = "IDBodega";
        public static string FLAGELIMINARIXP = "FlagEliminar";
        private static string TABLAETIQUETASDISIXP = "EtiquetasDisIXP";

        //variables para guardar etiquetas disponibles por Confirmación de Traslado
        public static string IDCONF = "ID";
        public static string IDINVENTARIODOCEXTERNOSCONF = "IDInventariosDocExternos";
        public static string CODIGOETIQUETADISCONF = "CodigoEtiqueta";        
       
        private static string TABLAINVENTARIODOCEXTERNOSDETALLESETIQUETAS = "InventariosDocExternosDetallesetiquetas";



        public ConexionLocal(Context Context) : base(Context, dbName, null, 2)
        {
        }
        public override void OnCreate(SQLiteDatabase db)
        {
            //Funcion que permite crear tabla DATOS de conexion
            db.ExecSQL("CREATE TABLE " + TABLADATOSCONEXION +
              " (" + IDTABLADATOSCONEXION + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                   + DIRECCIONIPLOCAL + " TEXT, "
                   + DIRECCIONIPPUBLICA + " TEXT, "
                   + BASEDATOS + " TEXT, "
                   + USUARIO + " TEXT, "
                   + CONTRASENA + " TEXT, "
                   + EXTERNO + " INTEGER )");

            //Funcion que permite crear tabla Etiquetas
            db.ExecSQL("CREATE TABLE " + TABLAETIQUETAS +
              " (" + CODIGOETIQUETA + " TEXT, "
                   + CANTTALLOS + " INTEGER, "
                   + IDTIPODOCUMENTO + " INTEGER, "
                   + IDLOTEBQT + " INTEGER, "
                   + IDBODEGA + " INTEGER, "
                   + FECHALECTURA + " INTEGER, "
                   + FLAGDISPONIBLE+ " INTEGER )");

            //Funcion que permite crear tabla Etiquetas ingresos Produccion
            db.ExecSQL("CREATE TABLE " + TABLAETIQUETASINP +
              " (" + CODIGOETIQUETAINP + " TEXT, "
                   + CANTTALLOSIP + " INTEGER, "
                   + IDTIPODOCUMENTOIP + " INTEGER, "
                   + IDBODEGAIP + " INTEGER, "
                   + COSECHADOR + " INTEGER, " 
                   + CALIDAD + " INTEGER, "
                   + HORA + " TEXT," 
                   + FLAGDISPONIBLE + " INTEGER )");

            //Funcion que permite crear tabla Etiquetas Consumo Parcial
            db.ExecSQL("CREATE TABLE " + TABLAETIQUETASCP +
             " (" + CODIGOETIQUETACP + " TEXT, "
                   + CANTTALLOSCP + " INTEGER, "
                   + CANTTALLOORICP + " INTEGER, "
                   + IDTIPODOCUMENTOCP + " INTEGER, "
                   + IDBODEGACP + " INTEGER, "                   
                   + CALIDADCP + " INTEGER, "
                   + HORACP + " TEXT,"
                   + FLAGDISPONIBLE + " INTEGER )");          


        //crear tabla log
        db.ExecSQL("CREATE TABLE " + TABLALOG +
              " (" + IDTABLALOG + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                   + EXCEPCION + " TEXT, "
                   + FECHA + " TEXT )");
            //crear tabla Usuarios
            db.ExecSQL("CREATE TABLE " + TABLAAPPUSRS +
              " (" + ID + " INTEGER, "
                   + LOGIN + " TEXT, "
                   + PASSWORD + " TEXT, "
                   + NOMBRE + " TEXT, "
                   + APELLIDOS + " TEXT, "
                   + IDAPPPERFILESUSRS + " INTEGER, "
                   + EMAIL + " TEXT )");

            //crear tabla Compañia
            db.ExecSQL("CREATE TABLE " + TABLACOMPANIAS +
                  " (" + IDCOMPANIA + " INTEGER, "
                       + NOMBRECOMPANIA + " TEXT, "
                       + DIRECCIONCOMPANIA + " TEXT, "
                       + TELEFONOCOMPANIA + " TEXT, "
                       + FLAGACTIVOCOMPANIA + " INTEGER )");



            //crear tabla Unidades de Negocio
            db.ExecSQL("CREATE TABLE " + TABLAUNIDADESNEGOCIOS +
                  " (" + IDUNIDADESNEGOCIO + " INTEGER, "
                       + NOMBREUNIDADESNEGOCIO + " TEXT, "
                       + IDCOMPANIAUN + " INTEGER, "
                       + IDFUNCIONESUNIDADESNEGOCIO + " INTEGER, "
                       + FLAGACTIVOUNIDADNEGOCIO + " INTEGER )");

            //crear tabla Unidades de Negocio Usuarios
            db.ExecSQL("CREATE TABLE " + TABLAUNIDADESNEGOCIOSAPPUSRS +
                  " (" + IDUNIDADNEGOCIOAPPUSRS + " INTEGER, "
                       + IDUNIDADESNEGOCIOAPPUSRS + " INTEGER, "
                       + IDAPPUSRS + " INTEGER, "
                       + FLAGACTIVOAPPUSRS + " INTEGER )");

            //crear tabla Centros de Costo
            db.ExecSQL("CREATE TABLE " + TABLACENTROSDECOSTO +
                  " (" + IDCENTROSDECOSTO + " INTEGER, "
                       + CODIGOCENTROSDECOSTO + " TEXT, "
                       + NOMBRECENTROSDECOSTO + " TEXT, "
                       + FLAGACTIVOCENTROSDECOSTO + " INTEGER, "
                       + NIVELCENTROSDECOSTO + " INTEGER )");

            //crear tabla Lotes de Bouquetera
            db.ExecSQL("CREATE TABLE " + TABLALOTESBOUQUETERA +
                  " (" + IDLOTESBOUQUETERA + " INTEGER, "
                       + NOMBRELOTESBOUQUETERA + " TEXT, "
                       + DESCRIPCIONLOTESBOUQUETERA + " TEXT, "
                       + FLAGACTIVOLOTESBOUQUETERA + " INTEGER )");

            //crear tabla Motivos dado de baja
            db.ExecSQL("CREATE TABLE " + TABLALOTESMOTIVOSBAJAS +
                  " (" + IDLOTESMOTIVOSBAJAS + " INTEGER, "
                       + NOMBREMOTIVOSBAJAS + " TEXT, "
                       + IDBAS_TIPOMOTIVOBAJA + " INTEGER, "
                       + FLAGACTIVOMOTIVOSBAJAS + " INTEGER )");

            //crear tabla Destino de Baja
            db.ExecSQL("CREATE TABLE " + TABLADESTINOBAJAS +
                  " (" + IDDESTINOBAJAS + " INTEGER, "
                       + NOMBREDESTINOBAJAS + " TEXT, "
                       + IDBAS_TIPODESTINOBAJAS + " INTEGER, "
                       + FLAGACTIVODESTINOBAJAS + " INTEGER )");

            //crear tabla Bodega de Destino
            db.ExecSQL("CREATE TABLE " + TABLABODEGADESTINO +
                  " (" + IDBODEGADESTINO + " INTEGER, "
                       + NOMBREBODEGADESTINO + " TEXT, "
                       + FLAGACTIVOBODEGADESTINO + " INTEGER, "
                       + IDUNIDADESNEGOCIOSBODEGADESTINO + " INTEGER, "
                       + FLAGAUTOCONFIRMARREMISIONES + " INTEGER, "
                       + IDCOMPANIABODEGADESTINO + " INTEGER )");

            //crear tabla Camiones
            db.ExecSQL("CREATE TABLE " + TABLACAMIONES +
                  " (" + IDCAMIONES + " INTEGER, "
                       + CODIGOCAMIONES + " TEXT, "
                       + NOMBRECAMIONES + " TEXT, "
                       + CAPACIDADCAMIONES + " INTEGER, "
                       + PLACASCAMIONES + " TEXT, "
                       + TIPOVEHICULOCAMIONES + " TEXT, "
                       + FLAGACTIVOCAMIONES + " INTEGER )");

            //crear tabla Conductor
            db.ExecSQL("CREATE TABLE " + TABLACONDUCTOR +
                  " (" + IDCONDUCTOR + " INTEGER, "
                       + CODIGOCONDUCTOR + " TEXT, "
                       + NOMBRECONDUCTOR + " TEXT, "
                       + DOCUMENTOSCONDUCTOR + " TEXT, "
                       + FLAGACTIVOCONDUCTOR + " INTEGER, "
                       + CIUDADDOCUMENTO + " TEXT )");

            //crear tabla Empleados
            db.ExecSQL("CREATE TABLE " + TABLAEMPLEADOS +
                  " (" + IDEMPLEADOS + " INTEGER, "
                       + CODIGO + " TEXT, "
                       + NOMBRES + " TEXT, "
                       + APELLIDO1 + " TEXT, "
                       + APELLIDO2 + " TEXT, "
                       + FLAGACTIVOEMPLEADOS + " INTEGER )");


            //Crear Tablas de Etiquetas Disponibles
            db.ExecSQL("CREATE TABLE " + TABLAETIQUETASDIS +
              " (" + CODIGOETIQUETADIS + " TEXT, "
                   + IDUNIDADESNEGOCIOSDIS + " TEXT, "
                   + PRESENTACIONMEDIDADIS + " INTEGER, "
                   + CANTTALLOSDIS + " INTEGER, "
                   + PRODUCTO + " TEXT, "
                   + IDBODEGADIS  + " INTEGER, "
                   + FLAGELIMINAR + " INTEGER )");

            //Crear Tablas de Etiquetas Disponibles IXP
            db.ExecSQL("CREATE TABLE " + TABLAETIQUETASDISIXP +
              " (" + CODIGOETIQUETADISIXP + " TEXT, "
                   + IDUNIDADESNEGOCIOSDISIXP + " TEXT, "
                   + PRESENTACIONMEDIDADISIXP + " INTEGER, "
                   + CANTTALLOSDISIXP + " INTEGER, "
                   + PRODUCTOIXP + " TEXT, "
                   + IDBODEGADISIXP + " INTEGER, "
                   + FLAGELIMINARIXP + " INTEGER )");

            //Crear Tablas de Etiquetas Disponibles Por confirmación
            db.ExecSQL("CREATE TABLE " + TABLAINVENTARIODOCEXTERNOSDETALLESETIQUETAS +
              " (" + IDCONF + " INTEGER, "
                   + IDINVENTARIODOCEXTERNOSCONF + " INTEGER, "
                   + CODIGOETIQUETADISCONF + " TEXT )");

    }

        public override void OnUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            if (newVersion > oldVersion)
            {
                db.ExecSQL("ALTER TABLE " + TABLAETIQUETASINP + " ADD " + FLAGDISPONIBLE + " INTEGER ");
            }
        }

        //INSERT
        public void GuardarDatosConexion(String DireccionLocal, String DireccionPublica, String BaseDatos, String Usuario, String Contrasena, int Externo)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(DIRECCIONIPLOCAL, DireccionLocal);
            valConexion.Put(DIRECCIONIPPUBLICA, DireccionPublica);
            valConexion.Put(BASEDATOS, BaseDatos);
            valConexion.Put(USUARIO, Usuario);
            valConexion.Put(CONTRASENA, Contrasena);
            valConexion.Put(EXTERNO, Externo);

            this.WritableDatabase.Insert(TABLADATOSCONEXION, null, valConexion);
        }

        public void GuardarDatosEtiquetas(String CodigoEtiqueta, int CantTallos, int IDBodega, int TipoDocumento, int IDLote, int Fecha, int FlagDisponible)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(CODIGOETIQUETA, CodigoEtiqueta);
            valConexion.Put(CANTTALLOS, CantTallos);
            valConexion.Put(IDTIPODOCUMENTO, TipoDocumento);
            valConexion.Put(IDLOTEBQT, IDLote);
            valConexion.Put(IDBODEGA, IDBodega);
            valConexion.Put(FECHALECTURA, Fecha);
            valConexion.Put(FLAGDISPONIBLE, FlagDisponible);

            this.WritableDatabase.Insert(TABLAETIQUETAS, null, valConexion);
        }
        //Ingreso de Consumo Parcial
        public void GuardarDatosEtiquetasCP(String CodigoEtiqueta, int CantTallos, int CantTallosOri, int IDBodega, int TipoDocumento,  int Fecha, int FlagDisponible)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(CODIGOETIQUETACP, CodigoEtiqueta);
            valConexion.Put(CANTTALLOSCP, CantTallos);
            valConexion.Put(CANTTALLOORICP, CantTallosOri);
            valConexion.Put(IDTIPODOCUMENTOCP, TipoDocumento);            
            valConexion.Put(IDBODEGACP, IDBodega);
            valConexion.Put(HORACP, Fecha);
            valConexion.Put(FLAGDISPONIBLE, FlagDisponible);

            this.WritableDatabase.Insert(TABLAETIQUETASCP, null, valConexion);
        }

        public void GuardarDatosEtiquetasINP(String CodigoEtiqueta, int CantTallos,  int TipoDocumento, int IDBodega, int Cosechador, int Calidad, String Hora, int FlagDisponible)
        {
            if (CodigoEtiqueta.Substring(0, 1) == "9" & (CodigoEtiqueta.Trim().Length == 15))
            {

                ContentValues valConexion = new ContentValues();
                valConexion.Put(CODIGOETIQUETAINP, CodigoEtiqueta);
                valConexion.Put(CANTTALLOSIP, CantTallos);
                valConexion.Put(IDTIPODOCUMENTOIP, TipoDocumento);
                valConexion.Put(IDBODEGAIP, IDBodega);
                valConexion.Put(COSECHADOR, Cosechador);
                valConexion.Put(CALIDAD, Calidad);
                valConexion.Put(HORA, Hora);
                valConexion.Put(FLAGDISPONIBLE, FlagDisponible);

                this.WritableDatabase.Insert(TABLAETIQUETASINP, null, valConexion);
            }
            else
            {
                player = new MediaPlayer();

                String FilePath = "/sdcard/Music/TonoError.mp3";
                player.SetDataSource(FilePath);
                player.Prepare();
                player.Start();
                Toast.MakeText(Application.Context, "El Código de la Etiqueta no cumple con el estandar, vuelva a leer. Cod. " + CodigoEtiqueta, ToastLength.Short).Show();
            }
        }

        public void GuardarLog(string excepcion, string fecha)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(EXCEPCION, excepcion);
            valConexion.Put(FECHA, fecha);
            this.WritableDatabase.Insert(TABLALOG, null, valConexion);
        }

        //Guardar los usuario JH29112018
        public void GuardarUsuarios(int Id, String Login, String Password, String Nombre, String Apellidos, int IdApperfilesUsrs, String Email)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(ID, Id);
            valConexion.Put(LOGIN, Login);
            valConexion.Put(PASSWORD, Password);
            valConexion.Put(NOMBRE, Nombre);
            valConexion.Put(APELLIDOS, Apellidos);
            valConexion.Put(IDAPPPERFILESUSRS, IdApperfilesUsrs);
            valConexion.Put(EMAIL, Email);

            this.WritableDatabase.Insert(TABLAAPPUSRS, null, valConexion);
        }

        //Guardar las compañias JH
        public void GuardarCompanias(int Id, String Nombre, String DireccionCompania, String TelefonoCompania, int FlagActivo)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDCOMPANIA, Id);
            valConexion.Put(NOMBRECOMPANIA, Nombre);
            valConexion.Put(DIRECCIONCOMPANIA, DireccionCompania);
            valConexion.Put(TELEFONOCOMPANIA, TelefonoCompania);
            valConexion.Put(FLAGACTIVOCOMPANIA, FlagActivo);

            this.WritableDatabase.Insert(TABLACOMPANIAS, null, valConexion);
        }

        //Guardar las Unidades Negocio JH
        public void GuardarUnidadesNegocios(int Id, String Nombre, int IDCompania, int IDUnidadesNegocio, int FlagActivo)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDUNIDADESNEGOCIO, Id);
            valConexion.Put(NOMBREUNIDADESNEGOCIO, Nombre);
            valConexion.Put(IDCOMPANIAUN, IDCompania);
            valConexion.Put(IDFUNCIONESUNIDADESNEGOCIO, IDUnidadesNegocio);
            valConexion.Put(FLAGACTIVOUNIDADNEGOCIO, FlagActivo);

            this.WritableDatabase.Insert(TABLAUNIDADESNEGOCIOS, null, valConexion);
        }

        //Guardar las Unidades Negocio Usuarios JH
        public void GuardarUnidadesNegociosUsuarios(int Id, int IDUnidadesNegocio, int IDAppUsrs, int FlagActivo)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDUNIDADNEGOCIOAPPUSRS, Id);
            valConexion.Put(IDUNIDADESNEGOCIOAPPUSRS, IDUnidadesNegocio);
            valConexion.Put(IDAPPUSRS, IDAppUsrs);
            valConexion.Put(FLAGACTIVOAPPUSRS, FlagActivo);

            this.WritableDatabase.Insert(TABLAUNIDADESNEGOCIOSAPPUSRS, null, valConexion);
        }

        //Guardar los centros de Costos JH
        public void GuardarCentrosdeCosto(int Id, String CodigoCentrosdeCosto, String Nombre, int FlagActivo, int NivelCentrosdeCosto)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDCENTROSDECOSTO, Id);
            valConexion.Put(CODIGOCENTROSDECOSTO, CodigoCentrosdeCosto);
            valConexion.Put(NOMBRECENTROSDECOSTO, Nombre);
            valConexion.Put(FLAGACTIVOUNIDADNEGOCIO, FlagActivo);
            valConexion.Put(NIVELCENTROSDECOSTO, NivelCentrosdeCosto);

            this.WritableDatabase.Insert(TABLACENTROSDECOSTO, null, valConexion);
        }
        //Guardar las Bouquetera JH
        public void GuardarBouquetera(int Id, String Nombre, String DescripcionLotesBouquetera, int FlagActivo)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDLOTESBOUQUETERA, Id);
            valConexion.Put(NOMBRELOTESBOUQUETERA, Nombre);
            valConexion.Put(DESCRIPCIONLOTESBOUQUETERA, DescripcionLotesBouquetera);
            valConexion.Put(FLAGACTIVOLOTESBOUQUETERA, FlagActivo);            

            this.WritableDatabase.Insert(TABLALOTESBOUQUETERA, null, valConexion);
        }
        //Guardar los Motivos dado de baja JH
        public void GuardarMotivosBajas(int Id, String Nombre,  int IdTipoMotivoBajas, int FlagActivo)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDLOTESMOTIVOSBAJAS, Id);
            valConexion.Put(NOMBREMOTIVOSBAJAS, Nombre);
            valConexion.Put(IDBAS_TIPOMOTIVOBAJA, IdTipoMotivoBajas);
            valConexion.Put(FLAGACTIVOMOTIVOSBAJAS, FlagActivo);

            this.WritableDatabase.Insert(TABLALOTESMOTIVOSBAJAS, null, valConexion);
        }
        //Guardar los Destinos dado de baja JH
        public void GuardarDestinosBajas(int Id, String Nombre,  int IdTipoDestinoBajas, int FlagActivo)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDDESTINOBAJAS, Id);
            valConexion.Put(NOMBREDESTINOBAJAS, Nombre);
            valConexion.Put(IDBAS_TIPODESTINOBAJAS, IdTipoDestinoBajas);
            valConexion.Put(FLAGACTIVODESTINOBAJAS, FlagActivo);

            this.WritableDatabase.Insert(TABLADESTINOBAJAS, null, valConexion);
        }
        //Guardar los Destinos dado de baja JH
        public void GuardarBodegaDestinos(int Id, String Nombre, int FlagActivo, int IdUnidadesNegociosBodegaDestino, int FlagAutoConfirmarRemisiones, int IdCompaniaBodegaDestino )
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDBODEGADESTINO, Id);
            valConexion.Put(NOMBREBODEGADESTINO, Nombre);           
            valConexion.Put(FLAGACTIVOBODEGADESTINO, FlagActivo);
            valConexion.Put(IDUNIDADESNEGOCIOSBODEGADESTINO, IdUnidadesNegociosBodegaDestino);
            valConexion.Put(FLAGAUTOCONFIRMARREMISIONES, FlagAutoConfirmarRemisiones);
            valConexion.Put(IDCOMPANIABODEGADESTINO, IdCompaniaBodegaDestino);

            this.WritableDatabase.Insert(TABLABODEGADESTINO, null, valConexion);
        }
        //Guardar los Camiones JH
        public void GuardarCamiones(int Id, String Codigo, String Nombre, int CapacidadCamiones, String PlacasCamiones, String TipoVehiculosCamiones,  int FlagActivo)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDCAMIONES, Id);
            valConexion.Put(CODIGOCAMIONES, Codigo);
            valConexion.Put(NOMBRECAMIONES, Nombre);
            valConexion.Put(CAPACIDADCAMIONES, CapacidadCamiones);
            valConexion.Put(PLACASCAMIONES, PlacasCamiones);
            valConexion.Put(TIPOVEHICULOCAMIONES, TipoVehiculosCamiones);
            valConexion.Put(FLAGACTIVOCAMIONES, FlagActivo);

            this.WritableDatabase.Insert(TABLACAMIONES, null, valConexion);
        }
        //Guardar los Conductores JH
        public void GuardarConductores(int Id, String Codigo, String Nombre, String DocumentosConductor, int FlagActivo, String CiudadDocumento)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDCONDUCTOR, Id);
            valConexion.Put(CODIGOCONDUCTOR, Codigo);
            valConexion.Put(NOMBRECONDUCTOR, Nombre);
            valConexion.Put(DOCUMENTOSCONDUCTOR, DocumentosConductor);
            valConexion.Put(FLAGACTIVOCONDUCTOR, FlagActivo);
            valConexion.Put(CIUDADDOCUMENTO, CiudadDocumento);

            this.WritableDatabase.Insert(TABLACONDUCTOR, null, valConexion);
        }
        //Guardar los Empleados JH
        public void GuardarEmpleados(int Id, String Codigo, String Nombres, String Apellido1, String Apellido2, int FlagActivo)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDEMPLEADOS, Id);
            valConexion.Put(CODIGO, Codigo);
            valConexion.Put(NOMBRES, Nombres);
            valConexion.Put(APELLIDO1, Apellido1);
            valConexion.Put(APELLIDO2, Apellido2);           
            valConexion.Put(FLAGACTIVOEMPLEADOS, FlagActivo);            

            this.WritableDatabase.Insert(TABLAEMPLEADOS, null, valConexion);
        }

        //Guardar las Etiquetas disponible
        public void GuardarEtiquetasDis(String CodigoEtiquetaDis, int IDUnidadesNegocios, String PresentacionMedidadDis, int CanTallosDis, String Producto, int IDBodegaDis, int FlagEliminar)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(CODIGOETIQUETADIS, CodigoEtiquetaDis);
            valConexion.Put(IDUNIDADESNEGOCIOSDIS, IDUnidadesNegocios);
            valConexion.Put(PRESENTACIONMEDIDADIS, PresentacionMedidadDis);
            valConexion.Put(CANTTALLOSDIS, CanTallosDis);
            valConexion.Put(PRODUCTO, Producto);
            valConexion.Put(IDBODEGADIS, IDBodegaDis);
            valConexion.Put(FLAGELIMINAR, FlagEliminar);


            this.WritableDatabase.Insert(TABLAETIQUETASDIS, null, valConexion);
        }

        //Guardar las Etiquetas disponible IXP
        public void GuardarEtiquetasDisIXP(String CodigoEtiquetaDisIXP, int IDUnidadesNegocios, String PresentacionMedidadDisIxp, int CanTallosDisIxp, String ProductoIxp, int IDBodegaDisIxp, int FlagEliminar)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(CODIGOETIQUETADISIXP, CodigoEtiquetaDisIXP);
            valConexion.Put(IDUNIDADESNEGOCIOSDISIXP, IDUnidadesNegocios);
            valConexion.Put(PRESENTACIONMEDIDADISIXP, PresentacionMedidadDisIxp);
            valConexion.Put(CANTTALLOSDISIXP, CanTallosDisIxp);
            valConexion.Put(PRODUCTOIXP, ProductoIxp);
            valConexion.Put(IDBODEGADISIXP, IDBodegaDisIxp);
            valConexion.Put(FLAGELIMINAR, FlagEliminar);


            this.WritableDatabase.Insert(TABLAETIQUETASDISIXP, null, valConexion);
        }
        //Guardar las Etiquetas disponible En Confirmación de traslado
        public void GuardarEtiquetasDisCONF(int IDConf, int IDInventarioDocExternosCONF, String CodigoEtiquetaDisCONF)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(IDCONF, IDConf);
            valConexion.Put(IDINVENTARIODOCEXTERNOSCONF, IDInventarioDocExternosCONF);
            valConexion.Put(CODIGOETIQUETADISCONF, CodigoEtiquetaDisCONF);

            this.WritableDatabase.Insert(TABLAINVENTARIODOCEXTERNOSDETALLESETIQUETAS, null, valConexion);
        }      

        //SELECT
        public ICursor ConsultaDatosConexion()
        {
            String query = "SELECT * FROM AppConexion";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaEtiquetasSinSubir(int IDBodega, int IDTipoDocumento)
        {
            String query = "SELECT CodigoEtiqueta, CantTallos FROM AppEtiquetas WHERE IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDTipoDocumento + " AND FlagDisponible = 1";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaEtiquetasSinSubirCP(int IDBodega, int IDTipoDocumento)
        {
            String query = "SELECT CodigoEtiqueta, CantTallos, CanttallosActu FROM AppEtiquetasCP WHERE IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDTipoDocumento + " AND FlagDisponible = 1";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }


        public ICursor ConsultaEtiquetasSinSubirNoDisp(int IDBodega, int IDTipoDocumento)
        {
            //String query = "SELECT CodigoEtiqueta, CantTallos FROM AppEtiquetas WHERE IDTipoDocumento = " + IDTipoDocumento + " AND IDBodega =  " + IDBodega + " AND FlagDisponible = 0 ";
            String query = "SELECT DISTINCT CodigoEtiqueta, CantTallos FROM AppEtiquetas WHERE IDTipoDocumento = " + IDTipoDocumento + " AND FlagDisponible = 0 ";
            //String query = "SELECT CodigoEtiqueta, CantTallos FROM AppEtiquetas WHERE IDTipoDocumento = " + IDTipoDocumento + " AND FlagDisponible = 0 AND SUBSTR(CodigoEtiqueta, 1, 1) = 8";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaEtiquetasSinSubirNoDispCP(int IDBodega, int IDTipoDocumento)
        {
            //String query = "SELECT CodigoEtiqueta, CantTallos FROM AppEtiquetas WHERE IDTipoDocumento = " + IDTipoDocumento + " AND IDBodega =  " + IDBodega + " AND FlagDisponible = 0 ";
            String query = "SELECT DISTINCT CodigoEtiqueta, CantTallos FROM AppEtiquetasCP WHERE IDTipoDocumento = " + IDTipoDocumento + " AND FlagDisponible = 0 ";
            //String query = "SELECT CodigoEtiqueta, CantTallos FROM AppEtiquetas WHERE IDTipoDocumento = " + IDTipoDocumento + " AND FlagDisponible = 0 AND SUBSTR(CodigoEtiqueta, 1, 1) = 8";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaEtiquetasGeneradodesdeMenu()
        {
            String query = "SELECT CodigoEtiqueta, CantTallos FROM AppEtiquetas";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaEtiquetasGeneradodesdeMenuIP()
        {
            String query = "SELECT CodigoEtiqueta, CantTallos FROM AppEtiquetasIP";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        


        public ICursor ConsultaEtiquetasSinSubirTXT(int IDTipoDocumento)
        {
            String query = "SELECT CodigoEtiqueta, CantTallos FROM AppEtiquetas WHERE IDTipoDocumento = " + IDTipoDocumento;
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaEtiquetasSinSubirIP(int IDBodega, int IDTipoDocumento)
        {
            String query = "SELECT CodigoEtiqueta, CantTallos, Cosechador, Calidad, Hora FROM AppEtiquetasIP WHERE IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDTipoDocumento + " AND FlagDisponible = 1" ;
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }         

        public ICursor ConsultaEtiquetasSinSubirIPNoDisp(int IDBodega, int IDTipoDocumento)
        {
            
            String query = "SELECT CodigoEtiqueta, CantTallos, Cosechador, Calidad, Hora FROM AppEtiquetasIP WHERE IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDTipoDocumento + " AND FlagDisponible = 0";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaEtiquetasSinSubirCT(int IDBodega, int IDTipoDocumento)
        {
            String query = "SELECT CodigoEtiqueta FROM AppEtiquetas WHERE IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDTipoDocumento;
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }


        public ICursor ConsultaEtiquetasSinSubirXLote(int IDBodega, int IDTipoDocumento, int IDLote)
        {
            String query = "SELECT CodigoEtiqueta, CantTallos FROM AppEtiquetas WHERE IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDTipoDocumento + " AND IDLoteBQT = " + IDLote + " AND FlagDisponible = 1";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        
        public ICursor ConsultaLotesSinSubir(int IDBodega, int IDTipoDocumento)
        {
            String query = "SELECT DISTINCT IDLoteBQT FROM AppEtiquetas WHERE IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDTipoDocumento + " AND FlagDisponible = 1";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaLotesEtiquetanodisponible(int IDBodega, int IDTipoDocumento)
        {
            String query = "SELECT DISTINCT IDLoteBQT FROM AppEtiquetas WHERE IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDTipoDocumento + " AND FlagDisponible = 0";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaEtiquetasExist(string CodigoEtiq)
        {
            String query = "SELECT COUNT(CodigoEtiqueta) FROM AppEtiquetas WHERE CodigoEtiqueta = " + CodigoEtiq;
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaEtiquetasExistCP(string CodigoEtiq)
        {
            String query = "SELECT COUNT(CodigoEtiqueta) FROM AppEtiquetasCP WHERE CodigoEtiqueta = " + CodigoEtiq;
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaEtiquetasExistIP(string CodigoEtiq)
        {
            String query = "SELECT COUNT(CodigoEtiqueta) FROM AppEtiquetasIP WHERE CodigoEtiqueta = " + CodigoEtiq;
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaLog()
        {
            String query = "SELECT * FROM LogApp";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ObtieneEtiquetasSinSubirValidacion(int IDTipoDocumento)
        {
            String query = "SELECT CodigoEtiqueta FROM AppEtiquetas WHERE IDTipoDocumento = " + IDTipoDocumento;
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaLoginUsuarios(String Usuario, String Contrasena)
        {
            String query = "SELECT ID, Login, Password, Nombre || '_' || Apellidos AS Nombre, EMail FROM AppUsrs WHERE Login = '" + Usuario + "' AND Password = '" + Contrasena + "'";
            //String query = "SELECT ID, Login, Password, CONCAT(Nombre, '_', Apellidos) AS Nombre, EMail FROM AppUsrs WHERE Login = '" + Usuario + "' AND Password = '" + Contrasena + "'";
            //String query = "SELECT ID,Login, Password, Nombre, Apellidos) AS Nombre, EMail FROM AppUsrs ";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        

        public ICursor ConsultaCompania(int IDUsuario)
        {
            //String query = "SELECT A.ID, A.Nombre FROM Companias A INNER JOIN UnidadesNegocios B ON A.ID = B.IDCompanias INNER JOIN dbo.UnidadesNegociosAppUsrs C ON B.ID = C.IDUnidadesNegocios WHERE B.IDFuncionesUnidadesNegocio IN(0,1,4,9) AND C.IDAppUsrs = " + IDUsuario + " ORDER BY Nombre";
            String query = "SELECT DISTINCT A.ID, SUBSTR(A.Nombre, 1, 25) AS Nombre FROM Companias  A INNER JOIN UnidadesNegocios B ON A.ID = B.IDCompanias INNER JOIN UnidadesNegociosAppUsrs C ON B.ID = C.IDUnidadesNegocios WHERE B.IDFuncionesUnidadesNegocio IN(0,1,4,9) AND C.IDAppUsrs = " + IDUsuario + " ORDER BY Nombre";
            //String query = "SELECT DISTINCT A.ID, substr(A.Nombre, 1, 25) AS Nombre FROM Companias  A INNER JOIN UnidadesNegocios B ON A.ID = B.IDCompanias  WHERE B.IDFuncionesUnidadesNegocio IN(0,1,4,9) ORDER BY Nombre";

            //String query = "SELECT ID, Nombre,Direccion, Telefono,FlagActivo FROM Companias ";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaUnidadesNegocio(int IDCompania,int IDUsuario)
        {            
            String query = "SELECT B.ID,SUBSTR(B.Nombre,1,27) AS Nombre, B.IDCompanias FROM UnidadesNegociosAppUsrs  A INNER JOIN UnidadesNegocios B ON B.ID = A.IDUnidadesNegocios WHERE A.IDAppUsrs = " + IDUsuario + " AND B.IDCompanias = " + IDCompania + " ORDER BY B.Nombre";         
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaCentroC(int IDCompania)
        {
            //String query = "SELECT B.ID,SUBSTR(B.Nombre,1,27) AS Nombre, B.IDCompanias FROM UnidadesNegociosAppUsrs A INNER JOIN UnidadesNegocios B ON B.ID = A.IDUnidadesNegocios WHERE A.IDAppUsrs = " + IDUsuario + " AND B.IDCompanias = " + IDCompania + " ORDER BY B.Nombre";
            String query = "SELECT ID, SUBSTR(Nombre,1,25) AS Nombre FROM CentrosDeCosto WHERE (ID = 0 OR NIVEL = 3) AND ID BETWEEN 367 AND 397";

            //String query = "SELECT ID, Nombre,Direccion, Telefono,FlagActivo FROM Companias ";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaBodegas(int IDCompania, int IDUnidadesNegocios)
        {
            //String query = "SELECT B.ID,SUBSTR(B.Nombre,1,27) AS Nombre, B.IDCompanias FROM UnidadesNegociosAppUsrs A INNER JOIN UnidadesNegocios B ON B.ID = A.IDUnidadesNegocios WHERE A.IDAppUsrs = " + IDUsuario + " AND B.IDCompanias = " + IDCompania + " ORDER BY B.Nombre";
            String query = "SELECT ID, SUBSTR(Nombre,1,25) AS Nombre FROM BodegaDestino WHERE IDCompanias = " + IDCompania + " AND IDUnidadesNegocios = " + IDUnidadesNegocios + " AND FlagActivo = 1 ORDER BY Nombre";

            //String query = "SELECT ID, Nombre,Direccion, Telefono,FlagActivo FROM Companias ";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaCamiones()
        {            
            String query = "SELECT ID,RTRIM(LTRIM(Nombre)) || ' ' || RTRIM(LTRIM(Placas)) AS Nombre FROM Camiones WHERE FlagActivo = 1 ORDER BY Nombre";
        
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaConductores()
        {
            //String query = "SELECT B.ID,SUBSTR(B.Nombre,1,27) AS Nombre, B.IDCompanias FROM UnidadesNegociosAppUsrs A INNER JOIN UnidadesNegocios B ON B.ID = A.IDUnidadesNegocios WHERE A.IDAppUsrs = " + IDUsuario + " AND B.IDCompanias = " + IDCompania + " ORDER BY B.Nombre";
            String query = "SELECT ID, SUBSTR(Nombre, 1, 25) AS Nombre FROM Conductores WHERE FlagActivo = 1 ORDER BY Nombre";

            //String query = "SELECT ID, Nombre,Direccion, Telefono,FlagActivo FROM Companias ";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaMotivos()
        {            
            String query = "SELECT ID, SUBSTR(Nombre, 1, 25) AS Nombre FROM BAS_MotivosDadosBaja ORDER BY Nombre";
         
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaDestinos()
        {
            String query = "SELECT ID, SUBSTR(Nombre, 1, 25) AS Nombre FROM DestinoBajas WHERE ID<> 36 ORDER BY Nombre";  

            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaBodegaDest()
        {
            String query = "SELECT A.* FROM BodegaDestino A INNER JOIN UnidadesNegocios B ON B.ID = A.IDUnidadesNegocios WHERE B.IDFuncionesUnidadesNegocio = 1 AND A.IDCompanias = 10 AND A.FlagActivo = 1 AND B.Nombre LIKE '%POST%' UNION ALL SELECT A.* FROM BodegaDestino A INNER JOIN UnidadesNegocios B ON B.ID = A.IDUnidadesNegocios WHERE B.IDFuncionesUnidadesNegocio IN(9,12) AND A.FlagActivo = 1 ORDER BY A.Nombre";  

            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaCodLote()
        {
            String query = "SELECT ID, Nombre FROM LotesBouquetera ORDER BY ID";

            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaEmpleados(int ID)
        {
            String query = "SELECT ID FROM Empleados Where ID = " + ID;

            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        

        public ICursor ConsultaEtiquetasDisponibles(string CodigoEtiqueta, int IDBodega)
        {
            String query = "Select PresentacionMedida,Cantidad,Producto,IDBodega from EtiquetasDis WHERE CodigoEtiqueta =  " + CodigoEtiqueta ;
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaEtiquetasDisponiblesIXP(string CodigoEtiqueta, int IDBodega)
        {
            String query = "Select PresentacionMedida,Cantidad,Producto,IDBodega from EtiquetasDisIXP WHERE CodigoEtiqueta =  " + CodigoEtiqueta;
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaEtiquetasDisponiblesCONF(string CodigoEtiqueta)
        {
            String query = "Select ID,IDInventariosDocExternos,CodigoEtiqueta from InventariosDocExternosDetallesetiquetas WHERE CodigoEtiqueta =  " + CodigoEtiqueta;
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaListaProduccionPorCodigoEtiqueta(string strCodigoEtiqueta)
        {
            String query = "SELECT Calidad, CantTallos, Cosechador FROM AppEtiquetasIP WHERE CodigoEtiqueta = '" + strCodigoEtiqueta + "'";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        public ICursor ConsultaListaProduccionPorCodigoEtiquetaCP(string strCodigoEtiqueta)
        {
            String query = "SELECT Calidad, CantTallos, Cosechador FROM AppEtiquetasIP WHERE CodigoEtiqueta = '" + strCodigoEtiqueta + "'";
            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }

        public ICursor ConsultaUnidades()
        {
            String query = "SELECT IDUnidadesNegocios FROM UnidadesNegociosAppUsrs  Where IDAppUsrs = " + VariablesGlobales.vgIDUsuario;

            ICursor Consulta = this.WritableDatabase.RawQuery(query, null);
            return Consulta;
        }
        //UPDATE
        public void ActualizarDatosConexion(String DireccionLocal, String DireccionPublica, String BaseDatos, String Usuario, String Contrasena,Int32 Externo)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(DIRECCIONIPLOCAL, DireccionLocal);
            valConexion.Put(DIRECCIONIPPUBLICA, DireccionPublica);
            valConexion.Put(BASEDATOS, BaseDatos);
            valConexion.Put(USUARIO, Usuario);
            valConexion.Put(CONTRASENA, Contrasena);
            valConexion.Put(EXTERNO, Externo);

            String where = "_id = 1";
            this.WritableDatabase.Update(TABLADATOSCONEXION, valConexion, where, null);
        }

        public void ActualizarCalidad(string codigo, int calidad,int cantallos, int cosechador)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(CALIDAD, calidad);
            valConexion.Put(CANTTALLOSIP, cantallos);
            valConexion.Put(COSECHADOR, cosechador);            


            String where = "CodigoEtiqueta = '" + codigo + "'";
            this.WritableDatabase.Update(TABLAETIQUETASINP, valConexion, where, null);
        }

        //ACTUALIZA
        public void ActualizaBodega(string codigo, int IDBodega)
        {
            ContentValues valores = new ContentValues();
            valores.Put(IDBODEGA, IDBodega);
            String where = "CodigoEtiqueta = " + codigo;
            //Actualizamos el registro en la base de datos

            this.WritableDatabase.Update(TABLAETIQUETAS, valores, where, null);
        }

        public void ActualizaEtiquetasEliminar(string codigo,int status)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(FLAGELIMINAR, status);

            if (status == 0)
            {
                String where = "";
                this.WritableDatabase.Update(TABLAETIQUETASDIS, valConexion, where, null);
            }
            else
            {
                String where = "CodigoEtiqueta = '" + codigo + "'";
                this.WritableDatabase.Update(TABLAETIQUETASDIS, valConexion, where, null);
            }            
            
        }
        public void ActualizaEtiquetasEliminarIXP(string codigo, int status)
        {
            ContentValues valConexion = new ContentValues();
            valConexion.Put(FLAGELIMINAR, status);

            if (status == 0)
            {
                String where = "";
                this.WritableDatabase.Update(TABLAETIQUETASDISIXP, valConexion, where, null);
            }
            else
            {
                String where = "CodigoEtiqueta = '" + codigo + "'";
                this.WritableDatabase.Update(TABLAETIQUETASDISIXP, valConexion, where, null);
            }
        }
        //DELETE
        public void EliminarDatosEtiquetas(int IDBodega, int IDDocumento)
        {
            ContentValues valConexion = new ContentValues();

            String where = "IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDDocumento;
            this.WritableDatabase.Delete(TABLAETIQUETAS, where, null);

            IDBodega = 0;//Se coloca para borrar las etiquetas no disponibles, no se puede generar por bodegas por que no se obtiene al momento de la busqueda.

            where = "IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDDocumento;
            this.WritableDatabase.Delete(TABLAETIQUETAS, where, null);
        }        
        public void EliminarDatosEtiquetasIP(int IDBodega, int IDDocumento)
        {
            ContentValues valConexion = new ContentValues();

            String where = "IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDDocumento;
            this.WritableDatabase.Delete(TABLAETIQUETASINP, where, null);

        }
        public void EliminarDatosEtiquetasCP(int IDBodega, int IDDocumento)
        {
            ContentValues valConexion = new ContentValues();

            String where = "IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDDocumento;
            this.WritableDatabase.Delete(TABLAETIQUETASCP, where, null);
        }
        public void EliminarEtiquetasCP(string Etiqueta)
        {
            ContentValues valConexion = new ContentValues();

            String where = "CodigoEtiqueta = " + Etiqueta;
            this.WritableDatabase.Delete(TABLAETIQUETASCP, where, null);
        }

        public void BorrarDatosEtiquetas()
        {
            ContentValues valConexion = new ContentValues();

            //String where = "IDTipoDocumento = 2";
            String where = "";
            this.WritableDatabase.Delete(TABLAETIQUETAS, where, null);
        }

        public void BorrarDatosEtiquetasINP()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLAETIQUETASINP, where, null);
        }

        public ICursor EliminarEtiquetas(int IDBodega, int IDDocumento)
        {
            String query = "DELETE AppEtiquetas WHERE IDBodega = " + IDBodega + " AND IDTipoDocumento = " + IDDocumento;
            ICursor eliminar = this.WritableDatabase.RawQuery(query, null);
            return eliminar;
        }
        //Borra las tablas Maestras
        //APPUSRS
        public void BorrarDatosAppUsrs()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLAAPPUSRS, where, null);
        }
        //COMPANIAS
        public void BorrarDatosCompania()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLACOMPANIAS, where, null);
        }
        //UNIDADNEGOCIO
        public void BorrarDatosUnidadNegocio()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLAUNIDADESNEGOCIOS, where, null);
        }
        //UNIDADNEGOCIOAPPUSRS
        public void BorrarDatosUnidadNegocioAppUsrs()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLAUNIDADESNEGOCIOSAPPUSRS, where, null);
        }
        //TABLACENTROSDECOSTO
        public void BorrarDatosCentrosdeCosto()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLACENTROSDECOSTO, where, null);
        }
        //TABLALOTESBOUQUETERA
        public void BorrarDatosBouquetera()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLALOTESBOUQUETERA, where, null);
        }
        //TABLALOTESMOTIVOSBAJAS
        public void BorrarDatosMotivosBajas()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLALOTESMOTIVOSBAJAS, where, null);
        }
        //TABLADESTINOBAJAS
        public void BorrarDatosDestinoBajas()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLADESTINOBAJAS, where, null);
        }
        //TABLABODEGADESTINO
        public void BorrarDatosBodegaDestino()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLABODEGADESTINO, where, null);
        }
        //TABLACAMIONES
        public void BorrarDatosCamiones()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLACAMIONES, where, null);
        }
        //TABLACONDUCTOR
        public void BorrarDatosConductor()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLACONDUCTOR, where, null);
        }
        public void BorrarDatosEmpleados()
        {
            ContentValues valConexion = new ContentValues();

            String where = "";
            this.WritableDatabase.Delete(TABLAEMPLEADOS, where, null);
        }
        public void BorrarDatosEtiquetasDisp(int FlagEliminar)
        {
            ContentValues valConexion = new ContentValues();

            if (FlagEliminar == 0)
            {
                String where = "";
                this.WritableDatabase.Delete(TABLAETIQUETASDIS, where, null);
            }
            else
            {
                String where = "FLAGELIMINAR = 1";
                this.WritableDatabase.Delete(TABLAETIQUETASDIS, where, null);
            }
        }
        public void BorrarDatosEtiquetasDispIXP(int FlagEliminar)
        {
            ContentValues valConexion = new ContentValues();

            if (FlagEliminar == 0)
            {
                String where = "";
                this.WritableDatabase.Delete(TABLAETIQUETASDISIXP, where, null);
            }
            else
            {
                String where = "FLAGELIMINAR = 1";
                this.WritableDatabase.Delete(TABLAETIQUETASDISIXP, where, null);
            }
        }
        public void BorrarDatosEtiquetasDispCONF()
        {           
                String where = "";
                this.WritableDatabase.Delete(TABLAINVENTARIODOCEXTERNOSDETALLESETIQUETAS, where, null);            
        }
    }
}