﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using Android.App;
using Android.Content;
using Android.Database;
using Android.Media;
using Android.Net;
using Android.Widget;
using static Android.Provider.Settings;
using Environment = Android.OS.Environment;

namespace InventariosApp.Clases
{
    class ConexionServidor
    {
        SqlConnection conexion;
        ConexionLocal db;
        String direccionIPLocal;
        String direccionIPPublica;
        String baseDatos;
        String usuario;
        String contrasena;
        String cadenaConexionLocal;
        int validarConexionFlag = 0;

        private MediaPlayer player;

        public void validarConexion()
        {

            db = new ConexionLocal(Application.Context);
            ICursor cursorDatosConexion = db.ConsultaDatosConexion();

            if (cursorDatosConexion.MoveToFirst())
            {
                do
                {
                    direccionIPLocal = cursorDatosConexion.GetString(1);
                    direccionIPPublica = cursorDatosConexion.GetString(2);
                    baseDatos = cursorDatosConexion.GetString(3);
                    usuario = cursorDatosConexion.GetString(4);
                    contrasena = cursorDatosConexion.GetString(5);
                } while (cursorDatosConexion.MoveToNext());
            }

            cadenaConexionLocal = @"data source= " + direccionIPLocal + ";initial catalog=" + baseDatos + ";user id=" + usuario + ";password= " + contrasena + ";Connect Timeout=0;"; //;Max Pool Size=50000 Max Pool Size=200;MultipleActiveResultSets=true

            try
            {
                conexion = new SqlConnection(cadenaConexionLocal);
                conexion.Open();
                
            }
            catch (SqlException e)
            {
                foreach (SqlError error in e.Errors)
                {
                    var FechaRegistro = DateTime.Now.ToString();
                    var errors = FechaRegistro + error.Number.ToString() + error.Message.ToString();

                    GenerarArchivoTxt(errors, "errores.txt");

                    player = new MediaPlayer();

                    String FilePath = "/sdcard/Music/TonoError.mp3";
                    player.SetDataSource(FilePath);
                    player.Prepare();
                    player.Start();

                }
                throw;
            }
        }
        private void GenerarArchivoTxt(string error,string NombreArchivo)
        {

            string pathTxt = "/sdcard/Download/" + NombreArchivo;

            StringBuilder constructor = new StringBuilder();

            if (!File.Exists(pathTxt))
            {
                StreamWriter streamWriter = File.CreateText(pathTxt);
                constructor.AppendLine(DateTime.Now.ToString());
                constructor.AppendLine(error);
                streamWriter.Dispose();
            }
            else
            {
                constructor.AppendLine(DateTime.Now.ToString());
                constructor.AppendLine(error);
            }
            //WriteAllText
            File.AppendAllText(pathTxt, constructor.ToString());
        }

        //Login
        public SqlDataReader Login(String Usuario, String Contrasena)
        {
            validarConexion();

            string sql = "SELECT ID, Login, Password, CONCAT(Nombre, '_', Apellidos) AS Nombre, EMail FROM AppUsrs WHERE Login = '" + Usuario + "' AND Password = '" + Contrasena + "'";
            SqlCommand Login = new SqlCommand(sql, conexion);
            SqlDataReader AppLogin;

            AppLogin = Login.ExecuteReader();            
            return AppLogin;
        }

        //Consulta Companias
        public SqlDataReader ConsultaCompanias(int IDUsuario)
        {
            validarConexion();

            string sqlcompanias = "SELECT DISTINCT A.ID, SUBSTRING(A.Nombre, 1, 25) AS Nombre FROM Companias A INNER JOIN UnidadesNegocios B ON A.ID = B.IDCompanias INNER JOIN dbo.UnidadesNegociosAppUsrs C ON B.ID = C.IDUnidadesNegocios WHERE B.IDFuncionesUnidadesNegocio IN(0,1,4,9) AND C.IDAppUsrs = " + IDUsuario + " ORDER BY Nombre";
            SqlCommand Companias = new SqlCommand(sqlcompanias, conexion);
            SqlDataReader AppCompanias;

            AppCompanias = Companias.ExecuteReader();           
            return AppCompanias;
        }

        //Consulta Unidades
        public SqlDataReader ConsultaUnidades(int IDCompania, int IDUsuario)
        {
            validarConexion();

            string sqlunidades = "SELECT B.ID,SUBSTRING(B.Nombre,1,27) AS Nombre, B.IDCompanias FROM dbo.UnidadesNegociosAppUsrs A INNER JOIN dbo.UnidadesNegocios B ON B.ID = A.IDUnidadesNegocios WHERE A.IDAppUsrs = " + IDUsuario + " AND B.IDCompanias = " + IDCompania + " ORDER BY B.Nombre";
            SqlCommand Unidades = new SqlCommand(sqlunidades, conexion);
            SqlDataReader AppUnidades;

            AppUnidades = Unidades.ExecuteReader();           
            return AppUnidades;
        }

        //Consulta Centros Costo
        public SqlDataReader ConsultaCentrosC(int IDCompania)
        {
            validarConexion();

            string sqlcentrosc = "SELECT ID,SUBSTRING(Nombre,1,25) AS Nombre FROM CentrosDeCosto WHERE (ID = 0 OR NIVEL = 3) AND ID BETWEEN 367 AND 397";
            SqlCommand CentrosC = new SqlCommand(sqlcentrosc, conexion);
            SqlDataReader AppCentrosC;

            AppCentrosC = CentrosC.ExecuteReader();           
            return AppCentrosC;
        }

        public SqlDataReader ConsultaCodLote()
        {
            validarConexion();

            string sqlcodlote = "SELECT ID, Nombre FROM LotesBouquetera ORDER BY ID";
            SqlCommand CodLote = new SqlCommand(sqlcodlote, conexion);
            SqlDataReader AppCodLote;

            AppCodLote = CodLote.ExecuteReader();           
            return AppCodLote;
        }
        
        //Consulta BODEG
        public SqlDataReader consultaBodegas(int IDCompania, int IDUnidadesNegocios)
        {
            validarConexion();

            string sqlbodegas= "SELECT ID, SUBSTRING(Nombre,1,25) AS Nombre FROM Bodegas WHERE IDCompanias = " + IDCompania +" AND IDUnidadesNegocios = "+ IDUnidadesNegocios +" AND FlagActivo = 1 ORDER BY Nombre";
            SqlCommand Bodegas = new SqlCommand(sqlbodegas, conexion);
            SqlDataReader AppBodegas;

            AppBodegas = Bodegas.ExecuteReader();
            
            return AppBodegas;
        }

        //Consulta Camion
        public SqlDataReader consultaCamiones()
        {
            validarConexion();

            string sqlcamiones = "SELECT ID,RTRIM(LTRIM(Nombre))+' '+RTRIM(LTRIM(Placas)) AS Nombre FROM Camiones WHERE FlagActivo = 1 ORDER BY Nombre";
            SqlCommand Camiones = new SqlCommand(sqlcamiones, conexion);
            SqlDataReader AppCamiones;

            AppCamiones = Camiones.ExecuteReader();
            
            return AppCamiones;
        }



        //Consulta conductor
        public SqlDataReader consultaConductor()
        {
            validarConexion();

            string sqlconductor = "SELECT ID, SUBSTRING(Nombre,1,25) AS Nombre FROM Conductores WHERE FlagActivo = 1 ORDER BY Nombre";
            SqlCommand Conductores = new SqlCommand(sqlconductor, conexion);
            SqlDataReader AppConductores;

            AppConductores = Conductores.ExecuteReader();
            
            return AppConductores;
        }


        //Consulta Motivo
        public SqlDataReader consultaMotivo()
        {
            validarConexion();

            string sqlmotivo = "SELECT ID,SUBSTRING(Nombre,1,25) AS Nombre FROM BAS_MotivosDadosBaja ORDER BY Nombre";
            SqlCommand Motivo = new SqlCommand(sqlmotivo, conexion);
            SqlDataReader AppMotivos;

            AppMotivos = Motivo.ExecuteReader();
            
            return AppMotivos;
        }


        //Consulta Destino
        public SqlDataReader consultaDestino()
        {
            validarConexion();

            string sqldestino = "SELECT ID,SUBSTRING(Nombre,1,25) AS Nombre FROM DestinoBajas WHERE ID <> 36 ORDER BY Nombre ";
            SqlCommand Destinos = new SqlCommand(sqldestino, conexion);
            SqlDataReader AppDestinos;

            AppDestinos = Destinos.ExecuteReader();
            
            return AppDestinos;
        }

        //Consulta Bodega Destino
        public SqlDataReader consultaBodegaDestino()
        {
            validarConexion();

            string sqlbdestino = "EXEC ConsultaBodegasParaTraslados ";
            SqlCommand BDestinos = new SqlCommand(sqlbdestino, conexion);
            SqlDataReader AppbDestinos;

            AppbDestinos = BDestinos.ExecuteReader();
            
            return AppbDestinos;
        }
        //Consulta Remisión

                    
        public SqlDataReader ConsultaEtiquetaTrasladoConfirmacion(int IDRemision, string CodigoEtiqueta)
        {
            validarConexion();

            string sqlbdestino = "EXEC ConsultaDetallesEtiquetasConfirmacionTrasladosMovil " + IDRemision + " ,'" + CodigoEtiqueta + "'";
            SqlCommand BDestinos = new SqlCommand(sqlbdestino, conexion);
            SqlDataReader AppbDestinos;

            AppbDestinos = BDestinos.ExecuteReader();

            return AppbDestinos;
        }


        public SqlDataReader consultaRemision(int IDBodega)
        {
            validarConexion();
            string FechaActual = DateTime.Now.ToString("yyyyMMdd");
            string HoraActual = DateTime.Now.ToString("hh:mm");

            string sqlremision = "EXEC MenuRemisionesXConfirmar " + IDBodega + ", '" + FechaActual + "','" + HoraActual + "'";
            SqlCommand Remision = new SqlCommand(sqlremision, conexion);
            SqlDataReader AppRemision;

            AppRemision = Remision.ExecuteReader();

            return AppRemision;
        }

        //Consulta Calidades
        public SqlDataReader ConsultaCalidades()
        {

            validarConexion();

            string sqlcalidades = "SELECT Calidad, SUBSTRING(CONCAT(Calidad, ' - ', Nombre),1,2) AS Nombre FROM TablaTraduccionCalidadesCosecha";
            SqlCommand Calidades = new SqlCommand(sqlcalidades, conexion);
            SqlDataReader AppCalidades;

            AppCalidades = Calidades.ExecuteReader();

           
            return AppCalidades;
        }

        public SqlDataReader ConsultaEtiqueta(string CodigoEtiqueta, int IDBodega)
        {
            SqlDataReader AppEtiqueta = null;
            validarConexion();
            var a = conexion.State;
            int flagState = 0;

            if (conexion.State == ConnectionState.Closed)
            {
                validarConexion();
                if (conexion.State == ConnectionState.Open)
                {
                    flagState = 1;
                }
            }
            else
            {
                flagState = 1;
            }
           

            try
            {
                string sqlEtiqueta;
                if (flagState == 1)
                {
                    //sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasSimple '" + CodigoEtiqueta + "'," + IDBodega;
                    sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasCompleto '" + CodigoEtiqueta + "'";
                    SqlCommand Etiqueta = new SqlCommand(sqlEtiqueta, conexion);
                    AppEtiqueta = Etiqueta.ExecuteReader();
                }           
            }
            catch (Exception x)
            {
                string FechaRegistro = DateTime.Now.ToString();
                string valError = x.ToString();
                db.GuardarLog("ERROR AL CONSULTAR ETIQUETA SQL EST. = " + a + " - " + valError, FechaRegistro);
                return AppEtiqueta;
            }

            return AppEtiqueta;
        }

        

        public SqlDataReader RetornaEtiquetasNoDisponibles(string Equipo, string Usuario)
        {
            validarConexion();
            string sqlEtiqueta;
            
            //sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasSimple '" + CodigoEtiqueta + "'," + IDBodega;
            sqlEtiqueta = "SELECT * FROM AppMovilEtiquetasNoDisponibles WHERE Equipo = '" + Equipo + "' AND Usuario = '" + Usuario + "'";


            SqlCommand Etiqueta = new SqlCommand(sqlEtiqueta, conexion);
            SqlDataReader AppEtiqueta;

            AppEtiqueta = Etiqueta.ExecuteReader();

            return AppEtiqueta;
        }       
        public SqlDataReader ConsultaEtiquetaValidacionParcial(string CodigoEtiqueta)
        {

            validarConexion();
            string sqlEtiqueta;
            //sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasSimple '" + CodigoEtiqueta + "'," + IDBodega;
            sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasCompletoParcial '" + CodigoEtiqueta + "'";


            SqlCommand Etiqueta = new SqlCommand(sqlEtiqueta, conexion);
            SqlDataReader AppEtiqueta;


            AppEtiqueta = Etiqueta.ExecuteReader();

            return AppEtiqueta;
        }


        public SqlDataReader ConsultaEtiquetaINP(string CodigoEtiqueta, int IDBodega)
        {
            validarConexion();
            string sqlEtiqueta;

            //sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasSimple '" + CodigoEtiqueta + "'," + IDBodega;
            sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasCompletoIngProd '" + CodigoEtiqueta + "'," + IDBodega;

            SqlCommand Etiqueta = new SqlCommand(sqlEtiqueta, conexion);
            SqlDataReader AppEtiqueta;

            AppEtiqueta = Etiqueta.ExecuteReader();
            
            return AppEtiqueta;
        }     


        public SqlDataReader ObtieneCodigoTraslado(int IDDocumento)
        {
            validarConexion();

            string sql = "SELECT Codigo FROM InventariosDocExternos WHERE ID = " + IDDocumento;
            SqlCommand Codigo = new SqlCommand(sql, conexion);
            SqlDataReader AppCodigo;

            AppCodigo = Codigo.ExecuteReader();
            return AppCodigo;
        }

        //
        public SqlDataReader ConsultaUsuarioApp()
        {
            validarConexion();
            string sqlUsuario;

            sqlUsuario = "SELECT ID ,LOGIN , PASSWORD , NOMBRE , APELLIDOS , IDAPPPERFILESUSRS , EMAIL FROM AppUsrs WHERE id > 0 ";

            SqlCommand Usuario = new SqlCommand(sqlUsuario, conexion);
            SqlDataReader AppUsrs;

            AppUsrs = Usuario.ExecuteReader();

            return AppUsrs;
        }
        //

        
        public SqlDataReader ConsultaEtiquetaTraslado(string codigoEtiqueta, int IDBodega)
        {
            validarConexion();
            string sqlEtiqueta;
            
            sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasSimple '" + codigoEtiqueta + "'," + IDBodega;

            SqlCommand Etiqueta = new SqlCommand(sqlEtiqueta, conexion);
            SqlDataReader AppEtiqueta;

            AppEtiqueta = Etiqueta.ExecuteReader();

            return AppEtiqueta;
        }


        public SqlDataReader ConsultaEtiquetaConteoTotal(string CodigoEtiqueta, int IDBodega)
        {
            validarConexion();
            string sqlEtiqueta;


            //sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasSimple '" + CodigoEtiqueta + "'," + IDBodega;
            sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasCompletoConteoTotal '" + CodigoEtiqueta + "'," + IDBodega;

            SqlCommand Etiqueta = new SqlCommand(sqlEtiqueta, conexion);
            SqlDataReader AppEtiqueta;

            AppEtiqueta = Etiqueta.ExecuteReader();

          
            return AppEtiqueta;
        }


        //Verifica tabla temporal para etiquetas
        public SqlDataReader VerificaTablaTemporal(int idusuario, String equipo)
        {
            validarConexion();

            string sqlverifica = "EXEC VerificaTablaTemporalUsuario " + idusuario + " , '" + equipo + "'";
            SqlCommand Verifica = new SqlCommand(sqlverifica, conexion);
            SqlDataReader AppVerifica;

            AppVerifica = Verifica.ExecuteReader();
            
            return AppVerifica;
        }

        public SqlDataReader VerificaTablaTmpIngresosXProduccion(String NombreUsuario, String equipo)
        {
            validarConexion();

            string sqlverifica = "EXEC VerificaTablaTmpIngresosXProduccion '" + NombreUsuario + "', '" + equipo + "'";
            SqlCommand Verifica = new SqlCommand(sqlverifica, conexion);
            SqlDataReader AppVerifica;

            AppVerifica = Verifica.ExecuteReader();
            
            return AppVerifica;
        }

        public SqlDataReader VerificaTablaTemporalConteo(int id, String equipo)
        {
            validarConexion();

            string sqlverifica = "EXEC VerificaTablaTemporalUsuarioAppMovil " + id + " , '" + equipo + "'";
            SqlCommand Verifica = new SqlCommand(sqlverifica, conexion);
            SqlDataReader AppVerifica;

            AppVerifica = Verifica.ExecuteReader();
            
            return AppVerifica;
        }

        public void TruncateTable(string NombreTabla)
        {
            validarConexion();

            string sql = "TRUNCATE TABLE " + NombreTabla;
            SqlCommand Truncate = new SqlCommand(sql, conexion);

            SqlDataReader AppTruncate;
            AppTruncate = Truncate.ExecuteReader();
            
        }

        //inserta etiquetas tabla temporal 
        //public void InsertaEtiquetaTablaTemporal(string codigoEtiqueta, string vgTablaTemp)
        //{
        //    validarConexion();

        //    string sql = "INSERT INTO " + vgTablaTemp + "(Etiqueta) VALUES (" + codigoEtiqueta + ")" ;
        //    SqlCommand Inserta = new SqlCommand(sql, conexion);

        //    SqlDataReader AppInserta;
        //    AppInserta = Inserta.ExecuteReader();   
        //}

        public void InsertaEtiquetaTablaTemporal(string codigoEtiqueta, string vgTablaTemp)
        {
         //   validarConexion();

            string sql = "INSERT INTO " + vgTablaTemp + "(Etiqueta) VALUES (" + codigoEtiqueta + ")";

            SqlCommand Inserta = new SqlCommand(sql, conexion);

            SqlDataReader AppInserta;
            AppInserta = Inserta.ExecuteReader();            
        }
        
        //inserta etiquetas tabla temporal 
        public SqlDataReader GeneraCabezaDocFlor(int TipoDoc, int IDBodega, int IDUnidades, int IDCompania, int IDCentroC, int IDBodegaDest, int IDCamion, int IDConductor, int IDMotivo, int IDDestino, int IDCodLote, int IDUsuario, string Precinto)
        {
            validarConexion();

            string sqlDoc = "EXEC GeneraCabezaDocFlor " + TipoDoc +", "+ IDBodega + ", "+ IDUnidades+ ", " + IDCompania + ", " + IDCentroC + ", " + IDBodegaDest + ", " + IDCamion + ", " + IDConductor + ", " + IDMotivo + ", " + IDDestino + ", " + IDCodLote + ", " + IDUsuario + ",'" + Precinto + "'";
            SqlCommand Doc = new SqlCommand(sqlDoc, conexion);
            SqlDataReader AppDoc;

            AppDoc = Doc.ExecuteReader();            
            return AppDoc;
        }

        //inserta etiquetas de consumos parcial  en tabla
        public SqlDataReader InsertaEtiquetaConsumoParcial(string CodigoEtiqueta, int Cantidad, int CantidadActual,int IDBodega)
        {
            SqlDataReader AppEtiqueta = null;
            validarConexion();
            var a = conexion.State;
            int flagState = 0;

            if (conexion.State == ConnectionState.Closed)
            {
                validarConexion();
                if (conexion.State == ConnectionState.Open)
                {
                    flagState = 1;
                }
            }
            else
            {
                flagState = 1;
            }


            try
            {
                string sqlEtiqueta;
                if (flagState == 1)
                {
                    sqlEtiqueta = "EXEC InsertaEtiquetasCP '" + CodigoEtiqueta + "'," + Cantidad+","+ CantidadActual+","+ IDBodega;
                    SqlCommand Etiqueta = new SqlCommand(sqlEtiqueta, conexion);
                    AppEtiqueta = Etiqueta.ExecuteReader();
                }
            }
            catch (Exception x)
            {
                string FechaRegistro = DateTime.Now.ToString();
                string valError = x.ToString();
                db.GuardarLog("ERROR AL CONSULTAR ETIQUETA SQL EST. = " + a + " - " + valError, FechaRegistro);
                return AppEtiqueta;
            }

            return AppEtiqueta;
        }


        //Ejecuta procedimiento detalles etiquetas x documento
        public void GeneraDetallesEtiquetasDocFlorConsumos(int tipoDoc, string vgTablaTemp, int IDCabezaDoc, string Equipo, string usuario)
        {
            validarConexion();

            string sql = "EXEC GetDetallesEtiquetasDocFlorAppMovil " + tipoDoc + ", '" + vgTablaTemp + "', " + IDCabezaDoc + ",'" + Equipo + "','" + usuario + "'";
            SqlCommand Inserta = new SqlCommand(sql, conexion);

            SqlDataReader AppInserta;
            AppInserta = Inserta.ExecuteReader();
        }



        public void GeneraConfirmacionBasadoEtiquetasValidacion(int tipoDoc)
        {
            validarConexion();

            string sql = "EXEC GeneraConfirmacionBasadoEtiquetasValidacion " + tipoDoc + "";
            SqlCommand Inserta = new SqlCommand(sql, conexion);

            SqlDataReader AppInserta;
            AppInserta = Inserta.ExecuteReader();
        }
        public void GeneraConfirmarRemisionEntreBodegas(int tipoDoc)
        {
            validarConexion();
            string FechaActual = DateTime.Now.ToString("yyyyMMdd");
            string HoraActual = DateTime.Now.ToString("hh:mm");

            string sql = "EXEC ConfirmarRemisionEntreBodegasAPP " + tipoDoc + ", '" + FechaActual + "','" + HoraActual + "'," + VariablesGlobales.vgIDUsuario + "";
            //vgIDUsuario
            SqlCommand Inserta = new SqlCommand(sql, conexion);

            SqlDataReader AppInserta;
            AppInserta = Inserta.ExecuteReader();
        }
        //strSQL = "exec ConfirmarRemisionEntreBodegas " & Me.cmbRemision.SelectedValue & ", '" & Me.dtpFecha.Value.ToShortDateString & "', '" & Format(dtpHora.Value, "t") & "', " & IDUsuarioActual
        //Ejecuta procedimiento detalles etiquetas x documento
        public void GeneraDetallesEtiquetasDocFlor(int tipoDoc, string vgTablaTemp, int IDCabezaDoc)
        {
            validarConexion();

            string sql = "EXEC GetDetallesEtiquetasDocFlor " + tipoDoc + ", '" + vgTablaTemp + "', " + IDCabezaDoc;
            SqlCommand Inserta = new SqlCommand(sql, conexion);

            SqlDataReader AppInserta;
            AppInserta = Inserta.ExecuteReader();
        }



        public SqlDataReader GeneraDetallesEtiquetasDocFlorPrevioIP(string Fecha, string Hora, int IDCompania, int IDUnidad, int IDProveedor, int IDBodega, int Usuario, String ruta, int Pistolero)
        {
            validarConexion();
            string sqlDoc = "EXEC InsertaDatosCabezaIngresosXProduccionPrevioEtiquetas '" + Fecha + "', '" +  Hora + "', " + IDCompania + ", " + IDUnidad + ", " + IDProveedor + ", " + IDBodega + ", " + Usuario + ", '" + ruta + "' , " + Pistolero;
            SqlCommand Doc = new SqlCommand(sqlDoc, conexion);
            SqlDataReader AppDoc;

            AppDoc = Doc.ExecuteReader();
            
            return AppDoc;
        }

        //Genera detalles detalles etiquetas
        public SqlDataReader GeneraDetallesEtiquetasDocFlorIP(int IDPrevio, int IDDocumento)
        {
            validarConexion();
            string sqlDoc = "EXEC GetDetallesEtiquetasDocFlorIngxProd " + IDPrevio + ", " + IDDocumento;
            SqlCommand Doc = new SqlCommand(sqlDoc, conexion);
            SqlDataReader AppDoc;

            AppDoc = Doc.ExecuteReader();            
            return AppDoc;
        }

        
        //LLenaAfanXUsuarioMaquina
        public void LLenaAfanXUsuarioMaquina(string usuario, string equipo, string tabla)
        {
            validarConexion();

            string sqlLlena = "EXEC  LLenaAfanXUsuarioMaquina '" + equipo + "', '" + usuario + "', '" + tabla + "' ";
            SqlCommand Llena= new SqlCommand(sqlLlena, conexion);
            SqlDataReader AppLlena;

            AppLlena = Llena.ExecuteReader();            
        }
        
        public void ArreglaDocumento(int idprevio,int iddocumento)
        {
            validarConexion();

            string sqlLlena = "EXEC  ArreglaDocPrevioIngresosXProduccionDetallesEtiquetas " + idprevio + ", " + iddocumento ;
            SqlCommand Llena = new SqlCommand(sqlLlena, conexion);
            SqlDataReader AppLlena;

            AppLlena = Llena.ExecuteReader();
            
        }

        //VerificaEtiquetasDocIngresoXProduccionFlorPrevio
        public void VerificaEtiquetasDocIngresoXProduccionFlorPrevio(int seccion,string archivo, string NombreEquipo, string NombreUsuario, int idPrevio)
        {
            validarConexion();

            string sqlLlena = "EXEC VerificaEtiquetasDocIngresoXProduccionFlorPrevio " + seccion + " ,'" + archivo + "','" + NombreEquipo + "','" + NombreUsuario + "', "+  idPrevio + ",0, 1";

            
           // string sqlLlena = "EXEC VerificaEtiquetasDocIngresoXProduccionFlorPrevioLectora " + Usuario + " ,'" + archivo + "','" + NombreEquipo + "','" + NombreUsuario + "', " + idPrevio;
            SqlCommand Llena = new SqlCommand(sqlLlena, conexion);
            SqlDataReader AppLlena;

            AppLlena = Llena.ExecuteReader();
            
        }

        //COMPACTA DETALLES DOCUMENTO DE CONSUMO
        public void CompactaDocConsumoFlorDetalles(int IDConsumos)
        {
            validarConexion();

            string sqlConsumoDetalles = "EXEC GeneraDocConsumosFlorDetalles " + IDConsumos;
            SqlCommand ConsumoDetalles = new SqlCommand(sqlConsumoDetalles, conexion);
            SqlDataReader AppConsumoDetalles;

            AppConsumoDetalles = ConsumoDetalles.ExecuteReader();
            
        }

        //CO9MPACTA DDETALLES DOCUMENTO ING X PRODUCCION

         public void CompactaDocIngProdFlorDetalles(int IDConsumos, int IDUnidad, string Fecha)
        {
            validarConexion();

            string sqlIngDetalles = "EXEC GenerarDocsIngresosXProduccionDetallesFROMDetallesEtiquetas " + IDConsumos + ", " + IDUnidad + ", '" + Fecha + "'";
            SqlCommand IngDetalles = new SqlCommand(sqlIngDetalles, conexion);
            SqlDataReader AppIngDetalles;

            AppIngDetalles = IngDetalles.ExecuteReader();
        }

        //COMPACTA DETALLES DOCUMENTO DE TRASLADO
        public void CompactaDocTrasladoFlorDetalles(int IDTraslado)
        {
            validarConexion();

            string sqlTrasladosDetalles = "EXEC GeneraDocTrasladoFlorDetallesFROMDetallesEtiquetas " + IDTraslado;
            SqlCommand TrasladoDetalles = new SqlCommand(sqlTrasladosDetalles, conexion);
            SqlDataReader AppTrasladosDetalles;

            AppTrasladosDetalles = TrasladoDetalles.ExecuteReader();            
        }

        //COMPACTA DETALLES DOCUMENTO DE INTERNO
        public void CompactaDocTrasladoInternoFlorDetalles(int IDTraslado)
        {
            validarConexion();

            string sqlConsumoDetalles = "EXEC GeneraDocInternosFlorDetallesFROMDetallesEtiquetas " + IDTraslado;
            SqlCommand ConsumoDetalles = new SqlCommand(sqlConsumoDetalles, conexion);
            SqlDataReader AppConsumoDetalles;

            AppConsumoDetalles = ConsumoDetalles.ExecuteReader();

            
        }

        //VerificaEtiquetasDocIngresoXProduccionFlorPrevio
        public void EliminaEtiquetasTemporalAfan(string equipo, string usuario)
        {
            validarConexion();

            string sqlLlena = "DELETE AfanSubirEtiquetasPrevioDocIngresosXProduccion WHERE Equipo = '" + equipo + "' AND Usuario = '" + usuario +"'";

            SqlCommand Llena = new SqlCommand(sqlLlena, conexion);
            SqlDataReader AppLlena;

            AppLlena = Llena.ExecuteReader();
        }

        //Genera el traslado y la confirmación automático
        public void GeneraTrspasoConfirmacion(int vgIDDocumento)
        {
            validarConexion();

            string sqlConsumoDetalles = "EXEC GeneraTrasladoConfirmacion " + vgIDDocumento;
            SqlCommand ConsumoDetalles = new SqlCommand(sqlConsumoDetalles, conexion);
            SqlDataReader AppConsumoDetalles;

            AppConsumoDetalles = ConsumoDetalles.ExecuteReader();


        }

        //public SqlDataReader GeneraDocConsumoFlorDetallesEtiquetas(String Etiqueta, int IDConsumos, int CantTallos)
        //{
        //    validarConexion();

        //    string sqlConsumoDetallesEtiquetas = "EXEC GeneraDocConsumosFlorDetallesEtiquetas " + IDConsumos + ",'" + Etiqueta + "'," + CantTallos;
        //    SqlCommand ConsumoDetallesEtiquetas = new SqlCommand(sqlConsumoDetallesEtiquetas, conexion);
        //    SqlDataReader AppConsumoDetallesEtiquetas;

        //    AppConsumoDetallesEtiquetas = ConsumoDetallesEtiquetas.ExecuteReader();

        //    return AppConsumoDetallesEtiquetas;
        //}

        //Llenado de Tablas Maestras
        //AppUsr
        public SqlDataReader AppUsrs()
        {
            validarConexion();

            string sql = "Select ID,Login,Password,Nombre,Apellidos,IDAppPerfilesUsrs,isnull(EMail,'') From dbo.AppUsrs";
            SqlCommand Usr = new SqlCommand(sql, conexion);
            SqlDataReader AppUsr;

            AppUsr = Usr.ExecuteReader();
            return AppUsr;
        }
        
        //Compania
        public SqlDataReader Compania()
        {
            validarConexion();

            string sql = "Select ID,Nombre,Direccion,Telefono,FlagActivo From dbo.Companias";
            SqlCommand Compania = new SqlCommand(sql, conexion);
            SqlDataReader AppCompania;

            AppCompania = Compania.ExecuteReader();
            return AppCompania;
        }
        //UnidadesNegocios
        public SqlDataReader UnidadesNegocios()
        {
            validarConexion();

            string sql = "Select ID,Nombre,IDCompanias,IDFuncionesUnidadesNegocio,FlagActivo From dbo.UnidadesNegocios";
            SqlCommand UnidadesNegocios = new SqlCommand(sql, conexion);
            SqlDataReader AppUnidadesNegocios;

            AppUnidadesNegocios = UnidadesNegocios.ExecuteReader();
            return AppUnidadesNegocios;
        }
        //UnidadesNegociosUsuarios
        public SqlDataReader UnidadesNegociosUsuarios()
        {
            validarConexion();

            string sql = "Select ID,IDUnidadesNegocios,IDAppUsrs,FlagActivo From dbo.UnidadesNegociosAppUsrs";
            SqlCommand UnidadesNegociosUsuarios = new SqlCommand(sql, conexion);
            SqlDataReader AppUnidadesNegociosUsuarios;

            AppUnidadesNegociosUsuarios = UnidadesNegociosUsuarios.ExecuteReader();
            return AppUnidadesNegociosUsuarios;
        }
        //CentrosdeCosto
        public SqlDataReader CentrosdeCosto()
        {
            validarConexion();

            string sql = "Select ID,Codigo,Nombre,FlagActivo,Nivel From dbo.CentrosdeCosto";
            SqlCommand CentrosdeCosto = new SqlCommand(sql, conexion);
            SqlDataReader AppCentrosdeCosto;

            AppCentrosdeCosto = CentrosdeCosto.ExecuteReader();
            return AppCentrosdeCosto;
        }

        //Bouquetera
        public SqlDataReader Bouquetera()
        {
            validarConexion();

            string sql = "Select ID,Nombre,Descripcion,FlagActivo From dbo.LotesBouquetera";
            SqlCommand Bouquetera = new SqlCommand(sql, conexion);
            SqlDataReader AppBouquetera;

            AppBouquetera = Bouquetera.ExecuteReader();
            return AppBouquetera;
        }
        //MotivosBajas
        public SqlDataReader MotivosBajas()
        {
            validarConexion();

            string sql = "Select ID,Nombre,IdTipoMotivoBajas,FlagActivo From dbo.MotivosBajas";
            SqlCommand MotivosBajas = new SqlCommand(sql, conexion);
            SqlDataReader AppMotivosBajas;

            AppMotivosBajas = MotivosBajas.ExecuteReader();
            return AppMotivosBajas;
        }
        //DestinosBajas
        public SqlDataReader DestinosBajas()
        {
            validarConexion();

            string sql = "Select ID,Nombre,IDBAS_TipoDestinoBajas,FlagActivo From dbo.DestinoBajas";
            SqlCommand DestinosBajas = new SqlCommand(sql, conexion);
            SqlDataReader AppDestinosBajas;

            AppDestinosBajas = DestinosBajas.ExecuteReader();
            return AppDestinosBajas;
        }
        //BodegaDestinos
        public SqlDataReader BodegaDestinos()
        {
            validarConexion();

            string sql = "Select ID,Nombre,isnull(FlagActivo,0),IdUnidadesNegocios,FlagAutoConfirmarRemisiones, IdCompanias From dbo.Bodegas";
            SqlCommand BodegaDestinos = new SqlCommand(sql, conexion);
            SqlDataReader AppBodegaDestinos;

            AppBodegaDestinos = BodegaDestinos.ExecuteReader();
            return AppBodegaDestinos;
        }
        //Camiones
        public SqlDataReader Camiones()
        {
            validarConexion();

            string sql = "Select ID,Codigo,Nombre,isnull(Capacidad,''),isnull(Placas,''),isnull(TipoVehiculo,''),FlagActivo From dbo.Camiones";
            SqlCommand Camiones = new SqlCommand(sql, conexion);
            SqlDataReader AppCamiones;

            AppCamiones = Camiones.ExecuteReader();
            return AppCamiones;
        }
        //Conductores
        public SqlDataReader Conductores()
        {
            validarConexion();

            string sql = "Select ID,isnull(Codigo,''),isnull(Nombre,''),isnull(Documento,''),isnull(FlagActivo,0), isnull(CiudadDocumento,'') From dbo.Conductores";
            SqlCommand Conductores = new SqlCommand(sql, conexion);
            SqlDataReader AppConductores;

            AppConductores = Conductores.ExecuteReader();
            return AppConductores;
        }
        public SqlDataReader EtiquetasDis(int Total)
        {
            validarConexion();
            string FechaActual = DateTime.Now.ToString("yyyyMMdd");
            //string HoraActual = DateTime.Now.ToString("hh:mm");

           // string sqlremision = "EXEC MenuRemisionesXConfirmar " + IDBodega + ", '" + FechaActual + "',

            if (Total == 1)
            {
                //string sql = "Select CodigoEtiqueta,IDUnidadesNegocios,PresentacionMedida,Cantidad,Producto,IDBodegas,0 from dbo.EtiquetasDis nolock where IDUnidadesNegocios = " + VariablesGlobales.vgIDUnidades + " AND Convert(char(8),FechaRegistro,112) = '" + FechaActual + "'";
                string sql = "Exec ConsultaEtiquetasDisponibles  " + VariablesGlobales.vgIDUnidadesUsr + "0" + "," + 1;
                SqlCommand Usr = new SqlCommand(sql, conexion);
                SqlDataReader AppUsr;
                Usr.CommandTimeout = 90;
                //try
                //{
                    AppUsr = Usr.ExecuteReader();
                    return AppUsr;
                //}
                //catch (SqlException e)
                //{
                //    //Console.WriteLine("Se agoto el tiempo de espera ");
                //   // Console.WriteLine(e);
                //    Toast.MakeText(Application.Context, "No se pudo descargar la información de etiquetas disponibles" + e, ToastLength.Long).Show();                    
                //}
            }
            else
            {

                //VariablesGlobales.vgIDUnidades
                
                //string sql = "Select CodigoEtiqueta,IDUnidadesNegocios,PresentacionMedida,Cantidad,Producto,IDBodegas,0 from dbo.EtiquetasDis nolock where IDUnidadesNegocios in (" +  VariablesGlobales.vgIDUnidadesUsr  + "0)";
                string sql = "Exec ConsultaEtiquetasDisponibles  " + VariablesGlobales.vgIDUnidadesUsr + "0"+"," +0;
                SqlCommand Usr = new SqlCommand(sql, conexion);
                SqlDataReader AppUsr;
                Usr.CommandTimeout = 90;
                AppUsr = Usr.ExecuteReader();
                return AppUsr;
            }            
            
        }
        public SqlDataReader EtiquetasDisIXP(int Total)
        {
            validarConexion();
            string FechaActual = DateTime.Now.ToString("yyyyMMdd");
            //string HoraActual = DateTime.Now.ToString("hh:mm");

            // string sqlremision = "EXEC MenuRemisionesXConfirmar " + IDBodega + ", '" + FechaActual + "',

            if (Total == 1)
            {
                //string sql = "Select CodigoEtiqueta,Presentacion,CantidadTallos,Producto,0 IDBodegas,0 from dbo.TablaIngresosXProduccionControl  nolock where IDUnidadesNegocios = " + VariablesGlobales.vgIDUnidades + " AND Convert(char(8),FechaRegistro,112) = '" + FechaActual + "'";
                string sql = "Exec ConsultaEtiquetasDisponiblesIngresosXP  " + VariablesGlobales.vgIDUnidadesUsr + "0" + "," + 1;
                SqlCommand Usr = new SqlCommand(sql, conexion);
                SqlDataReader AppUsr;
                Usr.CommandTimeout = 90;
                AppUsr = Usr.ExecuteReader();
                return AppUsr;
            }
            else
            {

                //VariablesGlobales.vgIDUnidades
                //string sql = "Select CodigoEtiqueta,IDUnidadesNegocios,Presentacion,CantidadTallos,Producto,0 IDBodegas,0 from dbo.TablaIngresosXProduccionControl nolock where IDUnidadesNegocios in (" + VariablesGlobales.vgIDUnidadesUsr + "0)";
                string sql = "Exec ConsultaEtiquetasDisponiblesIngresosXP  " + VariablesGlobales.vgIDUnidadesUsr + "0" + "," + 0;
                SqlCommand Usr = new SqlCommand(sql, conexion);
                SqlDataReader AppUsr;
                Usr.CommandTimeout = 90;
                AppUsr = Usr.ExecuteReader();
                return AppUsr;
            }

        }
        public SqlDataReader EtiquetasDisCONF(int id)
        {
            validarConexion();
            string FechaActual = DateTime.Now.ToString("yyyyMMdd");
            //string HoraActual = DateTime.Now.ToString("hh:mm");

            // string sqlremision = "EXEC MenuRemisionesXConfirmar " + IDBodega + ", '" + FechaActual + "',            
            string sql = "select C.ID,C.IDInventariosDocExternos,C.CodigoEtiqueta from dbo.Bodegas A Inner Join dbo.InventariosDocExternos B ON A.ID = B.IDBodegaDestino Inner Join InventariosDocExternosDetallesEtiquetas C ON B.ID = C.IDInventariosDocExternos where B.FlagActivo = 1 AND IDBAS_EstadosDocsInvExternos = 2 AND IDUnidadesNegocios in (" +  VariablesGlobales.vgIDUnidadesUsr  + "0) AND " + "C.IDInventariosDocExternos  = "+ id;
            SqlCommand Usr = new SqlCommand(sql, conexion);
            SqlDataReader AppUsr;
            AppUsr = Usr.ExecuteReader();
            return AppUsr;
        }
        //Empleados
        public SqlDataReader Empleados()
        {
            validarConexion();

            string sql = "Select ID,codigo,Nombres,Apellido1,Apellido2,FlagActivo From HORUS_CTRL_NOVEDADES.dbo.Empleados";
            SqlCommand Empleados = new SqlCommand(sql, conexion);
            SqlDataReader AppEmpleados;

            AppEmpleados = Empleados.ExecuteReader();
            return AppEmpleados;
        }
    }
}