﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace InventariosApp.Clases
{
    internal class LstLectura
    {
        String _codigo;
        String _tallos;

        public LstLectura(String codigo, String tallos)
        {
            this._codigo = codigo;
            this._tallos = tallos;
        }

        public string codigo
        {
            get
            {
                return _codigo;
            }
            set
            {
                _codigo = value;
            }
        }

        public string tallos
        {
            get
            {
                return _tallos;
            }
            set
            {
                _tallos = value;
            }
        }
      
    }
}