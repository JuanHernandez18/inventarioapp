﻿using Android.App;
using Android.OS;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using Android.Views;
using InventariosApp.Fragmentos;
using System.Data.SqlClient;
using InventariosApp.Clases;
using System.Collections.Generic;
using Android.Widget;
using System;
using Android.Content;
using Android.Database;


namespace InventariosApp.Clases
{
    class ActualizaEtiquetasDisp
    {
        String CadenaConexionLocal;
        String CadenaConexionPublica;
        SqlConnection conexion;
        private Context context;
        EditText direccionIPLocal;
        EditText direccionIPPublica;
        EditText baseDatos;
        EditText usuario;
        EditText contrasena;
        Button btnTest;
        Button btnGuardar;
        CheckBox Externo;
        int FlagExterno;

        ConexionLocal db;
        int FlagDatosConexion = 0;

        


        public void CargarEtiquetasDisponibles(int Total)
        {            
            db = new ConexionLocal(Application.Context);
            ConexionServidor con = new ConexionServidor();
            SqlDataReader DatosEtiquetas;
           // SqlDataReader DatosUnidades;

            if (Total == 0)
            {
                db.BorrarDatosEtiquetasDisp(0);
            }            
            //Buscamos las unidades de negocio del usuario para Etiquetas disponibles
            //Etiquetasdisponibles            
            ICursor cursorDatosUnidades = db.ConsultaUnidades();
            if (cursorDatosUnidades.MoveToFirst())
            {
                do
                {
                    VariablesGlobales.vgIDUnidadesUsr = VariablesGlobales.vgIDUnidadesUsr + Convert.ToString(cursorDatosUnidades.GetInt(0)) + "_";
                    //VariablesGlobales.vgIDUnidadesUsr = VariablesGlobales.vgIDUnidadesUsr + Convert.ToString(cursorDatosUnidades.GetInt(0));
                    //db.GuardarEtiquetasDis(DatosEtiquetas.GetString(0), DatosEtiquetas.GetInt32(1), DatosEtiquetas.GetString(2), DatosEtiquetas.GetInt32(3), DatosEtiquetas.GetString(4), DatosEtiquetas.GetInt32(5), DatosEtiquetas.GetInt32(6));

                } while (cursorDatosUnidades.MoveToNext());
            }            
            //Etiquetasdisponibles            
            DatosEtiquetas = con.EtiquetasDis(Total);
            if (DatosEtiquetas != null && DatosEtiquetas.HasRows)
            {
                while (DatosEtiquetas.Read())
                {
                    db.GuardarEtiquetasDis(DatosEtiquetas.GetString(0), DatosEtiquetas.GetInt32(1), DatosEtiquetas.GetString(2), DatosEtiquetas.GetInt32(3), DatosEtiquetas.GetString(4), DatosEtiquetas.GetInt32(5), DatosEtiquetas.GetInt32(6));
                    //db.GuardarEtiquetasDis(DatosEtiquetas.GetString(0), DatosEtiquetas.GetInt32(1), DatosEtiquetas.GetString(2), 0, " ", 0, 0);
                }
            }
            //else
            //{
            //    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
            //    Toast.MakeText(Application.Context, "No hay Nuevas Etiquetas Disponibles", ToastLength.Short).Show();
            //}
        }
        public void CargarEtiquetasDisponiblesIXP(int Total)
        {
            db = new ConexionLocal(Application.Context);
            ConexionServidor con = new ConexionServidor();
            SqlDataReader DatosEtiquetas;
            // SqlDataReader DatosUnidades;

            if (Total == 0)
            {
                db.BorrarDatosEtiquetasDispIXP(0);
            }
            //Buscamos las unidades de negocio del usuario para Etiquetas disponibles
            //Etiquetasdisponibles            
            ICursor cursorDatosUnidades = db.ConsultaUnidades();
            if (cursorDatosUnidades.MoveToFirst())
            {
                do
                {
                    VariablesGlobales.vgIDUnidadesUsr = VariablesGlobales.vgIDUnidadesUsr + Convert.ToString(cursorDatosUnidades.GetInt(0)) + "_";
                    //db.GuardarEtiquetasDis(DatosEtiquetas.GetString(0), DatosEtiquetas.GetInt32(1), DatosEtiquetas.GetString(2), DatosEtiquetas.GetInt32(3), DatosEtiquetas.GetString(4), DatosEtiquetas.GetInt32(5), DatosEtiquetas.GetInt32(6));

                } while (cursorDatosUnidades.MoveToNext());
            }
            //Etiquetasdisponibles            
            DatosEtiquetas = con.EtiquetasDisIXP(Total);
            if (DatosEtiquetas != null && DatosEtiquetas.HasRows)
            {
                while (DatosEtiquetas.Read())
                {
                    db.GuardarEtiquetasDisIXP(DatosEtiquetas.GetString(0), DatosEtiquetas.GetInt32(1), DatosEtiquetas.GetString(2), DatosEtiquetas.GetInt32(3), DatosEtiquetas.GetString(4), DatosEtiquetas.GetInt32(5), DatosEtiquetas.GetInt32(6));
                }
            }
            //else
            //{
            //    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
            //    Toast.MakeText(Application.Context, "No hay Nuevas Etiquetas Disponibles", ToastLength.Short).Show();
            //}
        }
        public void CargarEtiquetasDisponiblesCONF(int id)
        {
            db = new ConexionLocal(Application.Context);
            ConexionServidor con = new ConexionServidor();
            SqlDataReader DatosEtiquetas;
            // SqlDataReader DatosUnidades;

            db.BorrarDatosEtiquetasDispCONF();
            
            //Buscamos las unidades de negocio del usuario para Etiquetas disponibles
            //Etiquetasdisponibles            
            ICursor cursorDatosUnidades = db.ConsultaUnidades();
            if (cursorDatosUnidades.MoveToFirst())
            {
                do
                {
                    VariablesGlobales.vgIDUnidadesUsr = VariablesGlobales.vgIDUnidadesUsr + Convert.ToString(cursorDatosUnidades.GetInt(0)) + ",";
                    //db.GuardarEtiquetasDis(DatosEtiquetas.GetString(0), DatosEtiquetas.GetInt32(1), DatosEtiquetas.GetString(2), DatosEtiquetas.GetInt32(3), DatosEtiquetas.GetString(4), DatosEtiquetas.GetInt32(5), DatosEtiquetas.GetInt32(6));

                } while (cursorDatosUnidades.MoveToNext());
            }
            //Etiquetasdisponibles            
            DatosEtiquetas = con.EtiquetasDisCONF(id);
            if (DatosEtiquetas != null && DatosEtiquetas.HasRows)
            {
                while (DatosEtiquetas.Read())
                {
                    db.GuardarEtiquetasDisCONF(DatosEtiquetas.GetInt32(0), DatosEtiquetas.GetInt32(1), DatosEtiquetas.GetString(2));
                }
            }
            //else
            //{
            //    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
            //    Toast.MakeText(Application.Context, "No hay Nuevas Etiquetas Disponibles", ToastLength.Short).Show();
            //}
        }
        public void DatosMaestros()
        {
            db = new ConexionLocal(Application.Context);
            ConexionServidor con = new ConexionServidor();
            SqlDataReader DatosMaestros;
            // SqlDataReader DatosUnidades;

            //Borran las tablas maestras
            //db.BorrarDatosAppUsrs();
            db.BorrarDatosCompania();
            db.BorrarDatosUnidadNegocio();
            db.BorrarDatosUnidadNegocioAppUsrs();
            db.BorrarDatosCentrosdeCosto();
            db.BorrarDatosBouquetera();
            db.BorrarDatosMotivosBajas();
            db.BorrarDatosDestinoBajas();
            db.BorrarDatosBodegaDestino();
            db.BorrarDatosCamiones();
            db.BorrarDatosConductor();
            db.BorrarDatosEmpleados();
            db.BorrarDatosEtiquetasDisp(0);
            db.BorrarDatosEtiquetasDispIXP(0);





            //Usuarios            
            DatosMaestros = con.AppUsrs();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarUsuarios(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetString(4), DatosMaestros.GetInt32(5), DatosMaestros.GetString(6));
                }
            }           
            //Compañia            
            DatosMaestros = con.Compania();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarCompanias(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetInt32(4));
                }
            }            
            //UnidadesNegocios            
            DatosMaestros = con.UnidadesNegocios();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarUnidadesNegocios(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3), DatosMaestros.GetInt32(4));
                }
            }           
            //UnidadesNegociosUsuarios            
            DatosMaestros = con.UnidadesNegociosUsuarios();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarUnidadesNegociosUsuarios(DatosMaestros.GetInt32(0), DatosMaestros.GetInt32(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3));
                }
            }            
            //CentrosodeCosto           
            DatosMaestros = con.CentrosdeCosto();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarCentrosdeCosto(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetInt32(3), DatosMaestros.GetInt32(4));
                }
            }            
            //Bouquetera
            DatosMaestros = con.Bouquetera();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarBouquetera(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetInt32(3));
                }
            }
            
            ////MotivosBajas
            //DatosMaestros = con.MotivosBajas();

            //if (DatosMaestros != null && DatosMaestros.HasRows)
            //{
            //    while (DatosMaestros.Read())
            //    {
            //        db.GuardarMotivosBajas(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3));
            //    }
            //}
            //else
            //{
            //    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
            //    Toast.MakeText(Application.Context, "Tabla de MotivosBajas No disponible", ToastLength.Short).Show();
            //}
            //DestinosBajas
            DatosMaestros = con.DestinosBajas();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarDestinosBajas(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3));
                }
            }            
            //BodegaDestinos
            DatosMaestros = con.BodegaDestinos();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarBodegaDestinos(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3), DatosMaestros.GetInt32(4), DatosMaestros.GetInt32(5));
                }
            }           
            //Camiones
            DatosMaestros = con.Camiones();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarCamiones(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetInt32(3), DatosMaestros.GetString(4), DatosMaestros.GetString(5), DatosMaestros.GetInt32(6));
                }
            }            
            //Conductores
            DatosMaestros = con.Conductores();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarConductores(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetInt32(4), DatosMaestros.GetString(5));
                }
            }           
            //Empleados
            DatosMaestros = con.Empleados();

            //if (DatosMaestros != null && DatosMaestros.HasRows)
            //{
            //    while (DatosMaestros.Read())
            //    {
            //        db.GuardarEmpleados(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetString(4), DatosMaestros.GetInt32(5));
            //    }
            //}
            //else
            //{
            //    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
            //    Toast.MakeText(Application.Context, "Tabla de Empleados No disponible", ToastLength.Short).Show();
            //}
        }
    }
}