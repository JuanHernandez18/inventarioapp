﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using InventariosApp.Clases;

namespace InventariosApp.Clases
{
    class DialogListExtensive : DialogFragment
    {
        private GridView lv;
       // private String[] shows = { "hola", "chao" };
       
      
        private ArrayAdapter adapter;
        private Context context;
        private List<ListBD> lstMerchant;
        public DialogListExtensive(Context context, List<ListBD> lstMerchant) 
        {
            this.context = context;
            this.lstMerchant = lstMerchant;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View v = inflater.Inflate(Resource.Layout.DialogListExtensive, container, false);
            this.Dialog.SetTitle("Seleccione..");
            
            lv = v.FindViewById<GridView>(Resource.Id.lv);
                        
            adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, lstMerchant);
            lv.Adapter = adapter;

            lv.ItemClick += OnListItemClick;
            
           return v; 
        }

        private void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var listView = sender as ListView;
            var l = lstMerchant[e.Position];
            var idcampo = 0;
            var nombre = "";
           
                idcampo = l.ID;
                nombre = l.Nombre;

            Toast.MakeText(Application.Context, nombre + idcampo, ToastLength.Long).Show();
            VariablesGlobales.vgIDCodLote = idcampo;
            VariablesGlobales.vgNombreCodLote = nombre;
            Dialog.Cancel();
        }
    }
}