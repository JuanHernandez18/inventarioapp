﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace InventariosApp.Clases
{
    public class Compania
    {
        //[JsonProperty("ID")]
        public int ID { get; set; }

        //[JsonProperty("Codigo")]
        public string Codigo { get; set; }
        //[JsonProperty("Nombre")]
        public string Nombre { get; set; }
        //[JsonProperty("Direccion")]
        public string Direccion { get; set; }
        //[JsonProperty("Telefono")]
        public string Telefono { get; set; }
        //[JsonProperty("Telefono2")]
        public string Telefono2 { get; set; }
        //[JsonProperty("Fax")]
        public string Fax { get; set; }
        //[JsonProperty("Email")]
        public string Email { get; set; }
        //[JsonProperty("PaginaWeb")]
        public string PaginaWeb { get; set; }
        //[JsonProperty("Observaciones")]
        public string Observaciones { get; set; }
        //[JsonProperty("FlagActivo")]
        public int FlagActivo { get; set; }
        //[JsonProperty("ObservacionesOrdenCompra")]
        public string ObservacionesOrdenCompra { get; set; }
        //[JsonProperty("Orden")]
        public int Orden { get; set; }
        //[JsonProperty("UniversalID")]
        public int UniversalID { get; set; }
        //[JsonProperty("NombreSAFEX")]
        public string NombreSAFEX { get; set; }

        //public List<object> group_ids { get; set; }
    }
}