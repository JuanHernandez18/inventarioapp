﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace InventariosApp.Clases
{

    class LstLog
    {
        /*VARIABLES*/
        string _fecha;
        string _log;

        /*CONSTRUCTOR*/
        public LstLog(string fecha,
                      string log)
        {

            this._fecha = fecha;
            this._log = log;
        }

        /*  PROPIEDADES*/
        public string fecha
        {
            get
            {
                return _fecha;
            }
            set
            {

                _fecha = value;
            }
        }

        public string log
        {
            get
            {
                return _log;
            }
            set
            {

                _log = value;
            }
        }
        
    }

}