﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace InventariosApp.Clases
{
    internal class LstLecturaIP
    {
        String _codigo;
        String _tallos;
        String _cosechador;
        String _calidad;

        public LstLecturaIP(String codigo, String tallos, String cosechador, String calidad)
        {
            this._codigo = codigo;
            this._tallos = tallos;
            this._cosechador = cosechador;
            this._calidad = calidad;
        }

        public string codigo
        {
            get
            {
                return _codigo;
            }
            set
            {
                _codigo = value;
            }
        }

        public string tallos
        {
            get
            {
                return _tallos;
            }
            set
            {
                _tallos = value;
            }
        }

        public string cosechador
        {
            get
            {
                return _cosechador;
            }
            set
            {
                _cosechador = value;
            }
        }

        public string calidad
        {
            get
            {
                return _calidad;
            }
            set
            {
                _calidad = value;
            }
        }

    }
}