﻿using Android.App;
using Android.OS;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using Android.Views;
using InventariosApp.Fragmentos;
using System.Data.SqlClient;
using InventariosApp.Clases;
using System.Collections.Generic;
using Android.Widget;
using System;
using Android.Content;
using Android.Database;


namespace InventariosApp.Clases
{
    class ActualizaDatosMaestros
    {

        String CadenaConexionLocal;
        String CadenaConexionPublica;
        SqlConnection conexion;
        private Context context;
        EditText direccionIPLocal;
        EditText direccionIPPublica;
        EditText baseDatos;
        EditText usuario;
        EditText contrasena;
        Button btnTest;
        Button btnGuardar;
        CheckBox Externo;
        int FlagExterno;

        ConexionLocal db;
        int FlagDatosConexion = 0;


        ///////////////////////
        public void DatosTodos()
        {
            db = new ConexionLocal(Application.Context);
            ConexionServidor con = new ConexionServidor();
            SqlDataReader DatosMaestros;
            //Borran las tablas maestras
            //db.BorrarDatosAppUsrs();
            db.BorrarDatosCompania();
            db.BorrarDatosUnidadNegocio();
            db.BorrarDatosUnidadNegocioAppUsrs();
            db.BorrarDatosCentrosdeCosto();
            db.BorrarDatosBouquetera();
            db.BorrarDatosMotivosBajas();
            db.BorrarDatosDestinoBajas();
            db.BorrarDatosBodegaDestino();
            db.BorrarDatosCamiones();
            db.BorrarDatosConductor();
            db.BorrarDatosEmpleados();
            db.BorrarDatosEtiquetasDisp(0);
            db.BorrarDatosEtiquetasDispIXP(0);





            //Usuarios            
            DatosMaestros = con.AppUsrs();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarUsuarios(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetString(4), DatosMaestros.GetInt32(5), DatosMaestros.GetString(6));
                }
            }
            else
            {
                //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                Toast.MakeText(Application.Context, "Tabla de Usuarios No disponible", ToastLength.Short).Show();
            }
            //Compañia            
            DatosMaestros = con.Compania();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarCompanias(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetInt32(4));
                }
            }
            else
            {
                //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                Toast.MakeText(Application.Context, "Tabla de Compañias No disponible", ToastLength.Short).Show();
            }
            //UnidadesNegocios            
            DatosMaestros = con.UnidadesNegocios();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarUnidadesNegocios(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3), DatosMaestros.GetInt32(4));
                }
            }
            else
            {
                //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                Toast.MakeText(Application.Context, "Tabla de UnidadesNegocios No disponible", ToastLength.Short).Show();
            }
            //UnidadesNegociosUsuarios            
            DatosMaestros = con.UnidadesNegociosUsuarios();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarUnidadesNegociosUsuarios(DatosMaestros.GetInt32(0), DatosMaestros.GetInt32(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3));
                }
            }
            else
            {
                //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                Toast.MakeText(Application.Context, "Tabla de UnidadesNegociosUsuarios No disponible", ToastLength.Short).Show();
            }
            //CentrosodeCosto           
            DatosMaestros = con.CentrosdeCosto();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarCentrosdeCosto(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetInt32(3), DatosMaestros.GetInt32(4));
                }
            }
            else
            {
                //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                Toast.MakeText(Application.Context, "Tabla de CentrosodeCosto No disponible", ToastLength.Short).Show();
            }
            //Bouquetera
            DatosMaestros = con.Bouquetera();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarBouquetera(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetInt32(3));
                }
            }
            else
            {
                //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                Toast.MakeText(Application.Context, "Tabla de Bouquetera No disponible", ToastLength.Short).Show();
            }
            ////MotivosBajas
            //DatosMaestros = con.MotivosBajas();

            //if (DatosMaestros != null && DatosMaestros.HasRows)
            //{
            //    while (DatosMaestros.Read())
            //    {
            //        db.GuardarMotivosBajas(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3));
            //    }
            //}
            //else
            //{
            //    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
            //    Toast.MakeText(Application.Context, "Tabla de MotivosBajas No disponible", ToastLength.Short).Show();
            //}
            //DestinosBajas
            DatosMaestros = con.DestinosBajas();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarDestinosBajas(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3));
                }
            }
            else
            {
                //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                Toast.MakeText(Application.Context, "Tabla de DestinosBajas No disponible", ToastLength.Short).Show();
            }
            //BodegaDestinos
            DatosMaestros = con.BodegaDestinos();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarBodegaDestinos(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3), DatosMaestros.GetInt32(4), DatosMaestros.GetInt32(5));
                }
            }
            else
            {
                //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                Toast.MakeText(Application.Context, "Tabla de BodegaDestinos No disponible", ToastLength.Short).Show();
            }
            //Camiones
            DatosMaestros = con.Camiones();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarCamiones(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetInt32(3), DatosMaestros.GetString(4), DatosMaestros.GetString(5), DatosMaestros.GetInt32(6));
                }
            }
            else
            {
                //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                Toast.MakeText(Application.Context, "Tabla de Camiones No disponible", ToastLength.Short).Show();
            }
            //Conductores
            DatosMaestros = con.Conductores();

            if (DatosMaestros != null && DatosMaestros.HasRows)
            {
                while (DatosMaestros.Read())
                {
                    db.GuardarConductores(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetInt32(4), DatosMaestros.GetString(5));
                }
            }
            else
            {
                //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                Toast.MakeText(Application.Context, "Tabla de Conductores No disponible", ToastLength.Short).Show();
            }
            //Empleados
            DatosMaestros = con.Empleados();

            //if (DatosMaestros != null && DatosMaestros.HasRows)
            //{
            //    while (DatosMaestros.Read())
            //    {
            //        db.GuardarEmpleados(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetString(4), DatosMaestros.GetInt32(5));
            //    }
            //}
            //else
            //{
            //    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
            //    Toast.MakeText(Application.Context, "Tabla de Empleados No disponible", ToastLength.Short).Show();
            //}



            //Toast.MakeText(Application.Context, "Tablas Actualizadas", ToastLength.Long).Show();





        }



        /////////////////////




    }
}