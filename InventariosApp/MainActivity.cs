﻿using Android.App;
using Android.OS;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using Android.Views;
using InventariosApp.Fragmentos;
using System.Data.SqlClient;
using InventariosApp.Clases;
using System.Collections.Generic;
using Android.Widget;
using System;
using Android.Content;
using Android.Database;


namespace InventariosApp
{
    [Activity(Label = "Inventarios", Theme = "@style/AppTheme", MainLauncher = false, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        int FlagNavigationDrawer = 0;
        private Fragment mCurrentFragment;
        TextView TxtUsuario;
        private Lector lectorFragment;
        private Ajustes ajustesFragment;
        private Principal principalFragment;
        private Log logFragment;
        private ActualizaDatos actualizadatosFragment;
        private ProgressDialog progress;
        List<ListBD> lstCodLote = new List<ListBD>();
        private ConexionLocal db;
        private Android.App.AlertDialog.Builder dialog;
        String IDUsuario;
        String NomreUsuario;
        GeneraTXT GeneraTXT;
        Multimedia Multimedia;


        ActualizaEtiquetasDisp ActualizaEtiquetas;

        String direccionIPLocal;
        String direccionIPPublica;
        String baseDatos;
        String usuario;
        String contrasena;
        String cadenaConexionLocal;
        String cadenaConexionPublica;
        SqlConnection conexion;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            IDUsuario = this.Intent.GetStringExtra("ID");
            NomreUsuario = this.Intent.GetStringExtra("Usuario");
            VariablesGlobales.vgIDUsuario = Convert.ToInt32(IDUsuario);
            VariablesGlobales.vgNombreUsuario = NomreUsuario;

            Multimedia = new Multimedia();

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            navigationView.NavigationItemSelected += HomeNavigationView_NavigationItemSelected;
            View headerContainer = navigationView.GetHeaderView(0);
            TxtUsuario = (TextView)headerContainer.FindViewById(Resource.Id.textUsuario);
            TxtUsuario.Text = NomreUsuario; 

            lectorFragment = new Lector();
            ajustesFragment = new Ajustes();
            principalFragment = new Principal();
            logFragment = new Log();
            actualizadatosFragment = new ActualizaDatos();

            progress = new ProgressDialog(this);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetCancelable(false);

            var trans = FragmentManager.BeginTransaction();
            trans.Add(Resource.Id.fragmentContainer, principalFragment, "Principal");
            trans.Commit();

            mCurrentFragment = lectorFragment;
        }

        private void HomeNavigationView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            var menuItem = e.MenuItem;
            menuItem.SetChecked(!menuItem.IsChecked);

            switch (menuItem.ItemId)
            {
                case Resource.Id.nav_inicio:
                    drawerLayout.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);
                    SupportActionBar.SetTitle(Resource.String.Inicio);
                    ReplaceFragment(principalFragment);
                    break;
                case Resource.Id.nav_ajustes:
                    drawerLayout.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);
                    SupportActionBar.SetTitle(Resource.String.Ajustes);
                    ReplaceFragment(ajustesFragment);
                    break;
                case Resource.Id.nav_acttablasmaestras:
                    drawerLayout.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);
                    //SupportActionBar.SetTitle(Resource.String.Lector);
                    ReplaceFragment(actualizadatosFragment);
                    //ActualizaDatosMaestros();
                    break;
                case Resource.Id.nav_GenerarTXT:
                    //drawerLayout.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);
                    //SupportActionBar.SetTitle(Resource.String.Inicio);
                    //ReplaceFragment(ActTablasMaestrasFragment);
                    GenerarArchivos();
                    break;                    
                // case Resource.Id.nav_lector:
                //drawerLayout.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);
                //SupportActionBar.SetTitle(Resource.String.Lector);
                //ReplaceFragment(lectorFragment);
                //break;
                case Resource.Id.nav_borrar:
                    borrarDatos();
                    break;
                case Resource.Id.nav_log:
                    drawerLayout.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);
                    SupportActionBar.SetTitle(Resource.String.Log);
                    ReplaceFragment(logFragment);
                    break;
                case Resource.Id.nav_cerrar:
                    var PrincipalActivity = new Intent(this, typeof(Login));
                    this.StartActivity(PrincipalActivity);            
                    break;
            }
        }

        private void GenerarArchivos()
        {
            db = new ConexionLocal(Application.Context);
            ICursor cursorEtiquetas = db.ConsultaEtiquetasGeneradodesdeMenu();
            string NombreArchivo = VariablesGlobales.vgIDBodega + "DatosGenTXT" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    GeneraTXT = new GeneraTXT();
                    GeneraTXT.GenerarArchivoTxt(cursorEtiquetas.GetString(0), NombreArchivo);
                } while (cursorEtiquetas.MoveToNext());
            }
            ICursor cursorEtiquetasIP = db.ConsultaEtiquetasGeneradodesdeMenuIP();
            string NombreArchivoIP = VariablesGlobales.vgIDBodega + "DatosGenTXTIXP" + (string.Format("{0:MM-dd-yyyy}", DateTime.Now) + ".txt");

            if (cursorEtiquetas.MoveToFirst())
            {
                do
                {
                    GeneraTXT = new GeneraTXT();
                    GeneraTXT.GenerarArchivoTxt(cursorEtiquetas.GetString(0), NombreArchivo);
                } while (cursorEtiquetas.MoveToNext());
            }

            
        }

        private void borrarDatos()
        {

            var builder = new Android.App.AlertDialog.Builder(this);
            builder.SetIcon(Resource.Drawable.warning);
            builder.SetTitle("Advertencia");
            builder.SetMessage("¿Esta seguro de borrar la información ?");
            builder.SetPositiveButton("Continuar", OkActionBorrarDatos);
            builder.Show();         
            
        }

        private void OkActionBorrarDatos(object sender, DialogClickEventArgs e)
        {
            db = new ConexionLocal(Application.Context);
            db.BorrarDatosEtiquetas();
            db.BorrarDatosEtiquetasINP();
            db.ActualizaEtiquetasEliminar("1", 0);
        }

        private void ActualizaDatosMaestros()
        {

            var builder = new Android.App.AlertDialog.Builder(this);
            builder.SetIcon(Resource.Drawable.warning);
            builder.SetTitle("Advertencia");
            builder.SetMessage("¿Esta seguro de actualizar las tablas Maestras ?");
            builder.SetPositiveButton("Continuar", OkActionActualizaDatosMaestros);            
            builder.Show();
            
            

        }

        public void OkActionActualizaDatosMaestros(object sender, DialogClickEventArgs e)
        {
            //pruebas();
            //builder.Dispose;

            //progress = new ProgressDialog(this);
            progress = new ProgressDialog(this);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetCancelable(false);

            //ProgressDialog progress;
            progress.SetMessage("Actualizando Datos Maestros ...");
            progress.Show();

            db = new ConexionLocal(Application.Context);
            ConexionServidor con = new ConexionServidor();
            SqlDataReader DatosMaestros;

            try
            {
                //Borran las tablas maestras
                //db.BorrarDatosAppUsrs();
                db.BorrarDatosCompania();
                db.BorrarDatosUnidadNegocio();
                db.BorrarDatosUnidadNegocioAppUsrs();
                db.BorrarDatosCentrosdeCosto();
                db.BorrarDatosBouquetera();
                db.BorrarDatosMotivosBajas();
                db.BorrarDatosDestinoBajas();
                db.BorrarDatosBodegaDestino();
                db.BorrarDatosCamiones();
                db.BorrarDatosConductor();
                db.BorrarDatosEmpleados();
                db.BorrarDatosEtiquetasDisp(0);
                db.BorrarDatosEtiquetasDispIXP(0);





                //Usuarios            
                DatosMaestros = con.AppUsrs();

                if (DatosMaestros != null && DatosMaestros.HasRows)
                {
                    while (DatosMaestros.Read())
                    {
                        db.GuardarUsuarios(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetString(4), DatosMaestros.GetInt32(5), DatosMaestros.GetString(6));
                    }
                }
                else
                {
                    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                    Toast.MakeText(Application.Context, "Tabla de Usuarios No disponible", ToastLength.Short).Show();
                }
                //Compañia            
                DatosMaestros = con.Compania();

                if (DatosMaestros != null && DatosMaestros.HasRows)
                {
                    while (DatosMaestros.Read())
                    {
                        db.GuardarCompanias(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetInt32(4));
                    }
                }
                else
                {
                    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                    Toast.MakeText(Application.Context, "Tabla de Compañias No disponible", ToastLength.Short).Show();
                }
                //UnidadesNegocios            
                DatosMaestros = con.UnidadesNegocios();

                if (DatosMaestros != null && DatosMaestros.HasRows)
                {
                    while (DatosMaestros.Read())
                    {
                        db.GuardarUnidadesNegocios(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3), DatosMaestros.GetInt32(4));
                    }
                }
                else
                {
                    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                    Toast.MakeText(Application.Context, "Tabla de UnidadesNegocios No disponible", ToastLength.Short).Show();
                }
                //UnidadesNegociosUsuarios            
                DatosMaestros = con.UnidadesNegociosUsuarios();

                if (DatosMaestros != null && DatosMaestros.HasRows)
                {
                    while (DatosMaestros.Read())
                    {
                        db.GuardarUnidadesNegociosUsuarios(DatosMaestros.GetInt32(0), DatosMaestros.GetInt32(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3));
                    }
                }
                else
                {
                    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                    Toast.MakeText(Application.Context, "Tabla de UnidadesNegociosUsuarios No disponible", ToastLength.Short).Show();
                }
                //CentrosodeCosto           
                DatosMaestros = con.CentrosdeCosto();

                if (DatosMaestros != null && DatosMaestros.HasRows)
                {
                    while (DatosMaestros.Read())
                    {
                        db.GuardarCentrosdeCosto(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetInt32(3), DatosMaestros.GetInt32(4));
                    }
                }
                else
                {
                    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                    Toast.MakeText(Application.Context, "Tabla de CentrosodeCosto No disponible", ToastLength.Short).Show();
                }
                //Bouquetera
                DatosMaestros = con.Bouquetera();

                if (DatosMaestros != null && DatosMaestros.HasRows)
                {
                    while (DatosMaestros.Read())
                    {
                        db.GuardarBouquetera(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetInt32(3));
                    }
                }
                else
                {
                    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                    Toast.MakeText(Application.Context, "Tabla de Bouquetera No disponible", ToastLength.Short).Show();
                }
                ////MotivosBajas
                //DatosMaestros = con.MotivosBajas();

                //if (DatosMaestros != null && DatosMaestros.HasRows)
                //{
                //    while (DatosMaestros.Read())
                //    {
                //        db.GuardarMotivosBajas(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3));
                //    }
                //}
                //else
                //{
                //    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                //    Toast.MakeText(Application.Context, "Tabla de MotivosBajas No disponible", ToastLength.Short).Show();
                //}
                //DestinosBajas
                DatosMaestros = con.DestinosBajas();

                if (DatosMaestros != null && DatosMaestros.HasRows)
                {
                    while (DatosMaestros.Read())
                    {
                        db.GuardarDestinosBajas(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3));
                    }
                }
                else
                {
                    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                    Toast.MakeText(Application.Context, "Tabla de DestinosBajas No disponible", ToastLength.Short).Show();
                }
                //BodegaDestinos
                DatosMaestros = con.BodegaDestinos();

                if (DatosMaestros != null && DatosMaestros.HasRows)
                {
                    while (DatosMaestros.Read())
                    {
                        db.GuardarBodegaDestinos(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetInt32(2), DatosMaestros.GetInt32(3), DatosMaestros.GetInt32(4), DatosMaestros.GetInt32(5));
                    }
                }
                else
                {
                    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                    Toast.MakeText(Application.Context, "Tabla de BodegaDestinos No disponible", ToastLength.Short).Show();
                }
                //Camiones
                DatosMaestros = con.Camiones();

                if (DatosMaestros != null && DatosMaestros.HasRows)
                {
                    while (DatosMaestros.Read())
                    {
                        db.GuardarCamiones(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetInt32(3), DatosMaestros.GetString(4), DatosMaestros.GetString(5), DatosMaestros.GetInt32(6));
                    }
                }
                else
                {
                    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                    Toast.MakeText(Application.Context, "Tabla de Camiones No disponible", ToastLength.Short).Show();
                }
                //Conductores
                DatosMaestros = con.Conductores();

                if (DatosMaestros != null && DatosMaestros.HasRows)
                {
                    while (DatosMaestros.Read())
                    {
                        db.GuardarConductores(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetInt32(4), DatosMaestros.GetString(5));
                    }
                }
                else
                {
                    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                    Toast.MakeText(Application.Context, "Tabla de Conductores No disponible", ToastLength.Short).Show();
                }
                //Empleados
                DatosMaestros = con.Empleados();

                //if (DatosMaestros != null && DatosMaestros.HasRows)
                //{
                //    while (DatosMaestros.Read())
                //    {
                //        db.GuardarEmpleados(DatosMaestros.GetInt32(0), DatosMaestros.GetString(1), DatosMaestros.GetString(2), DatosMaestros.GetString(3), DatosMaestros.GetString(4), DatosMaestros.GetInt32(5));
                //    }
                //}
                //else
                //{
                //    //GuardarEtiLocal(txtCodigo.Text, ValTallosEt, varIDBodega, 0);
                //    Toast.MakeText(Application.Context, "Tabla de Empleados No disponible", ToastLength.Short).Show();
                //}

                ActualizaEtiquetas = new ActualizaEtiquetasDisp();

                ActualizaEtiquetas.CargarEtiquetasDisponiblesIXP(0);
                ActualizaEtiquetas.CargarEtiquetasDisponibles(0);
                //ActualizaEtiquetas.CargarEtiquetasDisponiblesIXP(0);
                //ActualizaEtiquetas.CargarEtiquetasDisponiblesCONF();

                Toast.MakeText(Application.Context, "Tablas Actualizadas", ToastLength.Long).Show();
                progress.Dismiss();
            }
            catch (SqlException )
            {
                //foreach (SqlError error in e.Errors)
                //{
                    //var FechaRegistro = DateTime.Now.ToString();
                    //var errors = FechaRegistro + error.Number.ToString() + error.Message.ToString();

                    //GenerarArchivoTxt(errors, "errores.txt");
                    Toast.MakeText(Application.Context, "No se pudieron Actualizar las tablas Maestras disponibles", ToastLength.Long).Show();

                    Multimedia.StartPlayer("E");

                //}
                throw;
            }

            
        }
        public void pruebas()
        {
            
        }
       

        private void ReplaceFragment(Fragment fragment)
        {
            if (fragment.IsVisible)
            {
                return;
            }
            var trans = FragmentManager.BeginTransaction();           
            trans.Replace(Resource.Id.fragmentContainer, fragment);
            trans.AddToBackStack(null);
           
            trans.Commit();

            mCurrentFragment = fragment;

        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            var inflater = MenuInflater;
            inflater.Inflate(Resource.Menu.menu_Principal, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    if (FlagNavigationDrawer == 0)
                    {
                        drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                        FlagNavigationDrawer = 1;
                    }
                    else
                    {
                        drawerLayout.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);
                        FlagNavigationDrawer = 0;
                    }
                return true;
                case Resource.Id.Ajustes:
                    SeleccionLotes();     
                 return true;
                case Resource.Id.Validar:
                    ValidarEtiquetas();
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void SeleccionLotes()
        {
            SqlDataReader CodLote;
            lstCodLote.Clear();

            ConexionServidor con = new ConexionServidor();
            CodLote = con.ConsultaCodLote();

            if (CodLote.HasRows)
            {
                while (CodLote.Read())
                {
                    lstCodLote.Add(new ListBD() { ID = CodLote.GetInt32(0), Nombre = CodLote.GetString(1) });
                }
            }
            
            FragmentTransaction fragmentTransactions = FragmentManager.BeginTransaction();
            DialogListExtensive Dialog = new DialogListExtensive(Application.Context, lstCodLote);
            Dialog.Show(fragmentTransactions, "Dialogo");
        }

        //Validacion de etiquetassss
        private void ValidarEtiquetas()
        {

            db = new ConexionLocal(Application.Context);
            ICursor cursorDatosConexion = db.ConsultaDatosConexion();


            if (cursorDatosConexion.MoveToFirst())
            {
                do
                {
                    direccionIPLocal = cursorDatosConexion.GetString(1);
                    direccionIPPublica = cursorDatosConexion.GetString(2);
                    baseDatos = cursorDatosConexion.GetString(3);
                    usuario = cursorDatosConexion.GetString(4);
                    contrasena = cursorDatosConexion.GetString(5);

                } while (cursorDatosConexion.MoveToNext());
            }

            cadenaConexionLocal = @"data source= " + direccionIPLocal + ";initial catalog=" + baseDatos + ";user id=" + usuario + ";password= " + contrasena + ";Connect Timeout=0";
            cadenaConexionPublica = @"data source= " + direccionIPPublica + ";initial catalog=" + baseDatos + ";user id=" + usuario + ";password= " + contrasena + ";Connect Timeout=0";

            conexion = new SqlConnection(cadenaConexionLocal);

            int valConex = 0;
            try
            {
                conexion.Open();
                valConex = 1;
            }
            catch (SqlException Exp)
            {
                conexion = new SqlConnection(cadenaConexionPublica);
                try
                {
                    conexion.Open();
                    valConex = 1;
                }
                catch (SqlException ex)
                {
                    Toast.MakeText(Application.Context, "No estas conectado a internet", ToastLength.Long).Show();
                }
            }

            if (valConex == 1)
            {
                MyTaskSync tarea = new MyTaskSync(progress,VariablesGlobales.vgIDDocumento, conexion);
                tarea.Execute();
            }
        }


        public class MyTaskSync : AsyncTask
        {
            private ConexionLocal db;
            SqlDataReader CabezaDocumento;
            ConexionServidor con = new ConexionServidor();
            ProgressDialog progress;
            int IDTipoDocumento;
            SqlConnection conexion;
            String tablaTemporal;
            String NombreEquipo = "";
            public MyTaskSync(ProgressDialog progress,int IDDocumento, SqlConnection conexion)
            {
                this.progress = progress;
                this.IDTipoDocumento = IDDocumento;
                this.conexion = conexion;
            }

            protected override void OnPreExecute()
            {
                progress.SetMessage("Validando ...");
                progress.Show();
            }
            protected override Java.Lang.Object DoInBackground(params Java.Lang.Object[] @params)
            {
                ValidaEtiquetas();
                return "d";
            }

            private void ValidaEtiquetas()
            {

                db = new ConexionLocal(Application.Context);
                ICursor validaEtiqueta = db.ObtieneEtiquetasSinSubirValidacion(VariablesGlobales.vgIDDocumento);
                ConexionServidor con = new ConexionServidor();
                string sqlEtiqueta;
                SqlDataReader Etiquetas;

                if (validaEtiqueta.MoveToFirst())
                {
                    do
                    {

                        //sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasSimple '" + CodigoEtiqueta + "'," + IDBodega;
                        sqlEtiqueta = "EXEC EstadoInventarioXBodegaFlorEtiquetasCompleto '" + validaEtiqueta.GetString(0) + "'";


                        SqlCommand Etiqueta = new SqlCommand(sqlEtiqueta, conexion);                      

                        using (SqlDataReader AppEtiqueta = Etiqueta.ExecuteReader())
                        {
                            while (AppEtiqueta.Read())
                            {
                                db.ActualizaBodega(validaEtiqueta.GetString(0), AppEtiqueta.GetInt32(3));                               
                            }
                            
                        }
                        
                        //  Etiquetas = con.ConsultaEtiquetaValidacion(validaEtiqueta.GetString(0), conexion);


                    } while (validaEtiqueta.MoveToNext());
                }
            }

            protected override void OnPostExecute(Java.Lang.Object result)
            {
                Toast.MakeText(Application.Context, "Etiquetas validadas", ToastLength.Long).Show();
                progress.Dismiss();
            }
        }


    }
}

