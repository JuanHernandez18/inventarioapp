﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using InventariosApp.Clases;

namespace InventariosApp.Fragmentos
{
    internal class AdLstLecturaIP : BaseAdapter<LstLecturaIP>
    {
        private Activity context;
        private List<LstLecturaIP> lstlectura;
        private LstLecturaIP item;

        public AdLstLecturaIP(Activity context, List<LstLecturaIP> lstlectura)
        {
            this.context = context;
            this.lstlectura = lstlectura;
        }

        public override LstLecturaIP this[int position]
        {
            get
            {
                return lstlectura[position];
            }
        }

        public override int Count
        {
            get { return lstlectura.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            item = lstlectura[position];

            View view = convertView;
            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.LstLecturaIP, null);

            view.FindViewById<TextView>(Resource.Id.txtCodigo).Text = item.codigo;
            view.FindViewById<TextView>(Resource.Id.txtTallos).Text = item.tallos;
            view.FindViewById<TextView>(Resource.Id.txtCalidad).Text = item.calidad;

            return view;
        }
    }
}