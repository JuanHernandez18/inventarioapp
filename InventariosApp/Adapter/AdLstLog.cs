﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using InventariosApp.Clases;

namespace InventariosApp.Adapter
{
  
    internal class AdLstLog: BaseAdapter<LstLog>
    {
          private Activity context;
        private List<LstLog> lstLog;
        private LstLog item;

        public AdLstLog(Activity context, List<LstLog> lstLog)
        {
            this.context = context;
            this.lstLog = lstLog;
        }

        public override LstLog this[int position]
        {
            get { return lstLog[position]; }
        }

        public override int Count
        {
            get { return lstLog.Count; }
        }


        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            item = lstLog[position];
            View view = convertView;
            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.LstLog, null);

            //view.FindViewById<TextView>(Resource.Id.IDPlaga).Text = Convert.ToString(item.id_plaga);
            view.FindViewById<TextView>(Resource.Id.txtFecha).Text = Convert.ToString(item.fecha);
            view.FindViewById<TextView>(Resource.Id.txtError).Text = Convert.ToString(item.log);
                       
            return view;
        }

    }
}