﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using InventariosApp.Clases;

namespace InventariosApp.Fragmentos
{
    internal class AdLstLectura : BaseAdapter<LstLectura>
    {
        private Activity context;
        private List<LstLectura> lstlectura;
        private LstLectura item;

        public AdLstLectura(Activity context, List<LstLectura> lstlectura)
        {
            this.context = context;
            this.lstlectura = lstlectura;
        }

        public override LstLectura this[int position]
        {
            get
            {
                return lstlectura[position];
            }
        }

        public override int Count
        {
            get { return lstlectura.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            item = lstlectura[position];

            View view = convertView;
            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.LstLectura, null);

            view.FindViewById<TextView>(Resource.Id.txtCodigo).Text = item.codigo;
            view.FindViewById<TextView>(Resource.Id.txtTallos).Text = item.tallos;

            return view;
        }
    }
}